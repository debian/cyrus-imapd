Cyrus IMAP 3.10.0 Release Notes
*******************************

Download from GitHub:

* https://github.com/cyrusimap/cyrus-imapd/releases/download/cyrus-
  imapd-3.10.0/cyrus-imapd-3.10.0.tar.gz

* https://github.com/cyrusimap/cyrus-imapd/releases/download/cyrus-
  imapd-3.10.0/cyrus-imapd-3.10.0.tar.gz.sig


Major changes since the 3.8 series
==================================

* URLs found in HTML "<a>", "<area>" and "<img>" tags, as well as
  "alt" text in "<img>" tags, are now indexed for search and snippets

* cyr_expire(8) now supports non-day durations in the
  archive/delete/expire annotations

* cyr_expire(8) no longer supports fractional durations in command
  line arguments.  Installations that passed fractional durations such
  as "1.5d" to any of the "-E", "-X", "-D", or "-A" arguments must
  adapt these to only use integer durations such as "1d12h"

* cyr_expire(8) now supports the 'noexpire_until' annotation to
  disable cyr_expire per user

* JMAP calendar default alarms are now stored in a non-DAV mailbox
  annotation. See Default alarms for upgrading instructions if you are
  already using the experimental JMAP Calendars API

* Removes support for parsing and generating bytecode for the
  deprecated denotify action and notify actions using the legacy
  (pre-**RFC 5435**) syntax. Existing bytecode containing these
  actions will still be executed.  Scripts that contain the deprecated
  denotify action should be rewritten to remove them.  Scripts that
  contain notify actions using the legacy syntax should be rewritten
  to use the syntax in **RFC 5435**

* Adds support for the "exp" and "nbf" JSON Web Token claims. Thanks
  Bruno Thomas

* Adds support for IMAP Version 4rev2 (**RFC 9051**)

* Adds support for IMAP NOTIFY (**RFC 5465**). Only available if
  "idled" is running

* Refresh interval for APNS subscriptions to DAV resources is now
  configurable. See the "aps_expiry" imapd.conf(5) option

* Upgrade IMAP Quota support to **RFC 9208**.  Sites running a Murder
  will be unable to set ANNOTATION-STORAGE or MAILBOX quotas (formerly
  known as X-ANNOTATION-STORAGE and X-NUM_FOLDERS) in a mixed-version
  environment until frontends are upgraded.  Upgraded frontends know
  how to negotiate with older backends.

* Adds support for IMAP REPLACE (**RFC 8508**)

* Adds support for IMAP UIDONLY extension (draft-ietf-extra-imap-
  uidonly)

* Adds experimental support for JMAP Contacts per upcoming IETF
  standards. Requires the not-yet-released libicalvcard

* squatter(8) now supports the "wait=y" cyrus.conf(5) option when
  started in rolling mode from the "DAEMON" section

* master(8) now touches a ready file to indicate it is "ready for
  work".  See master.pid and master.ready files

* master(8) now gets its pidfile name from the "master_pid_file"
  imapd.conf(5) option.  See master.pid and master.ready files

* Adds pcre2 support.  Prefers pcre2 over pcre if both are available.
  See PCRE2 support

* The "proc" cyr_info(8) subcommand now also reports DAEMON and EVENTS
  processes

* JMAP CalendarEventNotification objects are now automatically pruned.
  The "jmap_max_calendareventnotifs" imapd.conf(5) option can be used
  to tune this behaviour

* Cyrus now requires libical >= 3.0.10 for HTTP support

* Sieve [current]date ":zone" parameter now accepts either a UTC
  offset or an IANA time zone ID

* Adds an "implicit_keep_target" Sieve action to change the target
  mailbox for an implicit keep

* squatter(8) no longer holds a mailbox lock while extracting text
  from attachments

* IMAP "RENAME" command no longer emits non-standard per-folder
  updates.  Use the new "XRENAME" command if you need this behaviour

* Outgoing SMTP connections now EHLO as *client_bind_name* or
  *servername* rather than *localhost*

* Deprecates the experimental Cyrus Backups feature


Storage changes
===============

* None in 3.10.  But if your upgrade is skipping over 3.6 and 3.8,
  please do not miss 3.6.0 Storage changes and 3.8.0 Storage changes


Updates to default configuration
================================

The cyr_info(8) *conf*, *conf-all* and *conf-default* subcommands
accept an *-s <version>* argument to highlight imapd.conf(5) options
that are new or whose behaviour has changed since the specified
version.  We recommend using this when evaluating a new Cyrus version
to check which configuration options you will need to examine and
maybe set or change during the process.

* The master pidfile name is now read from imapd.conf, and defaults to
  "{configdirectory}/master.pid".  If you have something that looks
  for this file, you should either update it to look in the new
  default location, or set "master_pid_file" in imapd.conf(5) to
  override the default.  The "-p" option to master(8) can still be
  used to override it


Security fixes
==============

* Fixed CVE-2024-34055: Cyrus-IMAP through 3.8.2 and 3.10.0-beta2
  allow authenticated attackers to cause unbounded memory allocation
  by sending many LITERALs in a single command.

  The IMAP protocol allows for command arguments to be LITERALs of
  negotiated length, and for these the server allocates memory to
  receive the content before instructing the client to proceed. The
  allocated memory is released when the whole command has been
  received and processed.

  The IMAP protocol has a number commands that specify an unlimited
  number of arguments, for example SEARCH. Each of these arguments can
  be a LITERAL, for which memory will be allocated and not released
  until the entire command has been received and processed. This can
  run a server out of memory, with varying consequences depending on
  the server's OOM policy.

  Discovered by Damian Poddebniak.

  Two limits, with corresponding imapd.conf(5) options, have been
  added to address this:

  * "maxargssize" (default: unlimited): limits the overall length of a
    single IMAP command.  Deployments should configure this to a size
    that suits their system resources and client usage patterns

  * "maxliteral" (default: 128K): limits the length of individual IMAP
    LITERALs

  Connections sending commands that would exceed these limits will see
  the command fail, or the connection closed, depending on the
  specific context. The error message will contain the "[TOOBIG]"
  response code.

  These limits may be set small without affecting message uploads, as
  the APPEND command's message literal is limited by "maxmessagesize",
  not by these new options.


Significant bugfixes
====================

* Fixed: squat db reindexes are no longer always incremental

* Fixed: squat db corruption from unintentional indexing of fields
  intended to be skipped

* Fixed: squat db out of bounds access in incremental reindex docID
  map

* Fixed Issue #4692: squat db searches now handle unindexed messages
  correctly again (thanks Gabriele Bulfon)

* Restored functionality of the sync_client "-o"/"--connect-once"
  option

* Fixed Issue #4654: copying/moving messages from split conversations
  is now correct

* Fixed Issue #4758: fix renaming mailbox between users

* Fixed Issue #4804: mailbox_maxmessages limits now applied correctly

* Fixed Issue #4932: LITERAL+ broken in mupdate
