**ptexpire**
************

Program to expire pts cache DB entries on command.


Synopsis
========

   **ptexpire** [**-C** *filename*] [**-E** *seconds*]


Description
===========

The **ptexpire** program sweeps the "ptscache_db" database, deleting
entries older than the expiry duration, which defaults to 5400 seconds
(3 hours).  The expiry duration can be changed with the **-E** option.

**ptexpire** reads its configuration options out of the imapd.conf(5)
file unless specified otherwise by **-C**.


Options
=======

-C config-file

   Use the specified configuration file *config-file* rather than the
   default imapd.conf(5).

-E seconds, --expire-duration=seconds

   Set the expiry duration to *seconds*.


Files
=====

/etc/imapd.conf


See Also
========

imapd.conf(5)
