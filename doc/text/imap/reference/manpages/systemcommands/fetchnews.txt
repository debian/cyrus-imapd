**fetchnews**
*************

Retrieve new articles from peer and feed to Cyrus


Synopsis
========

   **fetchnews** [ **-C** *config-file* ] [ **-s** *servername*[:*port*]]
       [ **-n** ] [ **-y** ] [ **-w** *wildmat* ] [ **-f** *tstampfile* ]
       [ **-a** *authname* [ **-p** *password* ]] *peername*


Description
===========

**fetchnews** retrieves news articles from a peer news server and
feeds them to a Cyrus server. **fetchnews** connects to the peer
specified by *peername*, requests new articles since the time stored
in *tstampfile* and feeds them to *servername*.

**fetchnews** reads its configuration options out of the imapd.conf(5)
file unless specified otherwise by **-C**.


Options
=======

-C config-file

   Use the specified configuration file *config-file* rather than the
   default imapd.conf(5).

-s servername, --server=servername

   Hostname of the Cyrus server (with optional port) to which articles
   should be fed.  Defaults to "localhost:nntp".

-n, --no-newnews

   Don't use the NEWNEWS command. **fetchnews** will keep track of the
   high and low water marks for each group and use them to fetch new
   articles.

-y, --yyyy

   Use 4 instead of 2 digits for year. 2-digits are **RFC 977** - but
   not y2k-compliant.

-w wildmat, --groups=wildmat

   Wildmat pattern specifying which newsgroups to search for new
   articles.  Defaults to "*".

-f tstampfile, --newsstamp-file=tstampfile

   File in which to read/write the timestamp of when articles were
   last retrieved.  Defaults to "<configdirectory>/newsstamp" as
   specified by the configuration options.

-a authname, --auth-id=authname

   Userid to use for authentication.

-p password, --password=password

   Password to use for authentication.


Files
=====

/etc/imapd.conf


See Also
========

manpage:*imapd.conf(5)*
