/* A Bison parser, made by GNU Bison 3.8.2.  */

/* Bison implementation for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015, 2018-2021 Free Software Foundation,
   Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* DO NOT RELY ON FEATURES THAT ARE NOT DOCUMENTED in the manual,
   especially those whose name start with YY_ or yy_.  They are
   private implementation details that can be changed or removed.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output, and Bison version.  */
#define YYBISON 30802

/* Bison version string.  */
#define YYBISON_VERSION "3.8.2"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 1

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1


/* Substitute the variable and function names.  */
#define yyparse         sieveparse
#define yylex           sievelex
#define yyerror         sieveerror
#define yydebug         sievedebug
#define yynerrs         sievenerrs

/* First part of user prologue.  */
#line 1 "sieve/sieve.y"

/* sieve.y -- sieve parser
 * Larry Greenfield
 * Ken Murchison
 *
 * Copyright (c) 1994-2017 Carnegie Mellon University.  All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * 3. The name "Carnegie Mellon University" must not be used to
 *    endorse or promote products derived from this software without
 *    prior written permission. For permission or any legal
 *    details, please contact
 *      Carnegie Mellon University
 *      Center for Technology Transfer and Enterprise Creation
 *      4615 Forbes Avenue
 *      Suite 302
 *      Pittsburgh, PA  15213
 *      (412) 268-7393, fax: (412) 268-7395
 *      innovation@andrew.cmu.edu
 *
 * 4. Redistributions of any form whatsoever must retain the following
 *    acknowledgment:
 *    "This product includes software developed by Computing Services
 *     at Carnegie Mellon University (http://www.cmu.edu/computing/)."
 *
 * CARNEGIE MELLON UNIVERSITY DISCLAIMS ALL WARRANTIES WITH REGARD TO
 * THIS SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS, IN NO EVENT SHALL CARNEGIE MELLON UNIVERSITY BE LIABLE
 * FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN
 * AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING
 * OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

/*
 * Yacc definitions
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include "xmalloc.h"
#include "sieve/bytecode.h"
#include "sieve/comparator.h"
#include "sieve/interp.h"
#include "sieve/script.h"
#include "sieve/tree.h"
#include "sieve/flags.h"
#include "sieve/grammar.h"
#include "sieve/sieve_err.h"

#include "util.h"
#include "imparse.h"
#include "libconfig.h"
#include "times.h"
#include "tok.h"

#define ERR_BUF_SIZE 1024

int encoded_char = 0;    /* used to send encoded-character feedback to lexer */
static comp_t *ctags;    /* used for accessing comp_t* in a test/command union */
static int *copy;        /* used for accessing copy flag in a command union */
static int *create;      /* used for accessing create flag in a command union */
static strarray_t **flags; /* used for accessing imap flags in a command union */
static char **specialuse; /* used for accessing special-use flag in a command */
static char **mailboxid; /* used for accessing mailboxid in a command */
static char **fccfolder; /* used for accessing fcc.folder in a command */
static unsigned bctype;  /* used to set the bytecode command type in mtags */

extern int addrparse(sieve_script_t*);
typedef struct yy_buffer_state *YY_BUFFER_STATE;
extern YY_BUFFER_STATE addr_scan_string(const char*);
extern void addr_delete_buffer(YY_BUFFER_STATE);

extern int sievelineno;

void sieveerror_c(sieve_script_t*, int code, ...);

static int check_reqs(sieve_script_t*, strarray_t *sl);
static int chk_match_vars(sieve_script_t*, char *s);

static unsigned bc_precompile(cmdarg_t args[], const char *fmt, ...);

/* construct/canonicalize action commands */
static commandlist_t *build_keep(sieve_script_t*, commandlist_t *c);
static commandlist_t *build_fileinto(sieve_script_t*,
                                     commandlist_t *c, char *folder);
static commandlist_t *build_redirect(sieve_script_t*,
                                     commandlist_t *c, char *addr);
static commandlist_t *build_rej_err(sieve_script_t*, int t, char *message);
static commandlist_t *build_vacation(sieve_script_t*, commandlist_t *t, char *s);
static commandlist_t *build_flag(sieve_script_t*,
                                 commandlist_t *c, strarray_t *flags);
static commandlist_t *build_notify(sieve_script_t*,
                                   commandlist_t *c, char *method);
static commandlist_t *build_include(sieve_script_t*, commandlist_t *c, char*);
static commandlist_t *build_set(sieve_script_t*, commandlist_t *c,
                                char *variable, char *value);
static commandlist_t *build_addheader(sieve_script_t*, commandlist_t *c,
                                      char *name, char *value);
static commandlist_t *build_deleteheader(sieve_script_t*, commandlist_t *c,
                                         char *name, strarray_t *values);
static commandlist_t *build_log(sieve_script_t*, char *text);
static commandlist_t *build_snooze(sieve_script_t *sscript,
                                   commandlist_t *c, arrayu64_t *times);
static commandlist_t *build_imip(sieve_script_t *sscript, commandlist_t *t);
static commandlist_t *build_ikeep_target(sieve_script_t*,
                                         commandlist_t *c, char *folder);

/* construct/canonicalize test commands */
static test_t *build_anyof(sieve_script_t*, testlist_t *tl);
static test_t *build_allof(sieve_script_t*, testlist_t *tl);
static test_t *build_not(sieve_script_t*, test_t *t);
static test_t *build_address(sieve_script_t*, test_t *t,
                             strarray_t *sl, strarray_t *pl);
static test_t *build_envelope(sieve_script_t*, test_t *t,
                              strarray_t *sl, strarray_t *pl);
static test_t *build_header(sieve_script_t*, test_t *t,
                            strarray_t *sl, strarray_t *pl);
static test_t *build_body(sieve_script_t*, test_t *t, strarray_t *pl);
static test_t *build_stringt(sieve_script_t*, test_t *t,
                             strarray_t *sl, strarray_t *pl);
static test_t *build_hasflag(sieve_script_t*, test_t *t,
                             strarray_t *sl, strarray_t *pl);
static test_t *build_date(sieve_script_t*, test_t *t,
                          char *hn, char *part, strarray_t *kl);
static test_t *build_ihave(sieve_script_t*, strarray_t *sa);
static test_t *build_mbox_meta(sieve_script_t*, test_t *t, char *extname,
                               char *keyname, strarray_t *keylist);
static test_t *build_duplicate(sieve_script_t*, test_t *t);
static test_t *build_jmapquery(sieve_script_t*, test_t *t, char *json);

static int verify_weekday(sieve_script_t *sscript, char *day);
static int verify_time(sieve_script_t *sscript, char *time);

void yyerror(sieve_script_t*, const char *msg);
extern int yylex(void*, sieve_script_t*);
extern void sieverestart(FILE *f);

#define supported(capa) (sscript->support & capa)

#define _verify_flaglist(flags) \
  (supported(SIEVE_CAPA_VARIABLES) || verify_flaglist(flags))

#define YYERROR_VERBOSE /* I want better error messages! */

/* byacc default is 500, bison default is 10000 - go with the
   larger to support big sieve scripts (see Bug #3461) */
#define YYSTACKSIZE 10000

#line 242 "sieve/sieve.c"

# ifndef YY_CAST
#  ifdef __cplusplus
#   define YY_CAST(Type, Val) static_cast<Type> (Val)
#   define YY_REINTERPRET_CAST(Type, Val) reinterpret_cast<Type> (Val)
#  else
#   define YY_CAST(Type, Val) ((Type) (Val))
#   define YY_REINTERPRET_CAST(Type, Val) ((Type) (Val))
#  endif
# endif
# ifndef YY_NULLPTR
#  if defined __cplusplus
#   if 201103L <= __cplusplus
#    define YY_NULLPTR nullptr
#   else
#    define YY_NULLPTR 0
#   endif
#  else
#   define YY_NULLPTR ((void*)0)
#  endif
# endif

/* Use api.header.include to #include this header
   instead of duplicating it here.  */
#ifndef YY_SIEVE_SIEVE_SIEVE_H_INCLUDED
# define YY_SIEVE_SIEVE_SIEVE_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int sievedebug;
#endif

/* Token kinds.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    YYEMPTY = -2,
    YYEOF = 0,                     /* "end of file"  */
    YYerror = 256,                 /* error  */
    YYUNDEF = 257,                 /* "invalid token"  */
    NUMBER = 258,                  /* NUMBER  */
    STRING = 259,                  /* STRING  */
    IF = 260,                      /* IF  */
    ELSIF = 261,                   /* ELSIF  */
    ELSE = 262,                    /* ELSE  */
    REQUIRE = 263,                 /* REQUIRE  */
    STOP = 264,                    /* STOP  */
    DISCARD = 265,                 /* DISCARD  */
    KEEP = 266,                    /* KEEP  */
    FILEINTO = 267,                /* FILEINTO  */
    REDIRECT = 268,                /* REDIRECT  */
    ANYOF = 269,                   /* ANYOF  */
    ALLOF = 270,                   /* ALLOF  */
    EXISTS = 271,                  /* EXISTS  */
    NOT = 272,                     /* NOT  */
    SFALSE = 273,                  /* SFALSE  */
    STRUE = 274,                   /* STRUE  */
    SIZE = 275,                    /* SIZE  */
    HEADERT = 276,                 /* HEADERT  */
    ADDRESS = 277,                 /* ADDRESS  */
    ENVELOPE = 278,                /* ENVELOPE  */
    COMPARATOR = 279,              /* COMPARATOR  */
    OVER = 280,                    /* OVER  */
    UNDER = 281,                   /* UNDER  */
    ALL = 282,                     /* ALL  */
    LOCALPART = 283,               /* LOCALPART  */
    DOMAIN = 284,                  /* DOMAIN  */
    IS = 285,                      /* IS  */
    CONTAINS = 286,                /* CONTAINS  */
    MATCHES = 287,                 /* MATCHES  */
    OCTET = 288,                   /* OCTET  */
    ASCIICASEMAP = 289,            /* ASCIICASEMAP  */
    ASCIINUMERIC = 290,            /* ASCIINUMERIC  */
    REGEX = 291,                   /* REGEX  */
    QUOTEREGEX = 292,              /* QUOTEREGEX  */
    COPY = 293,                    /* COPY  */
    BODY = 294,                    /* BODY  */
    RAW = 295,                     /* RAW  */
    TEXT = 296,                    /* TEXT  */
    CONTENT = 297,                 /* CONTENT  */
    ENVIRONMENT = 298,             /* ENVIRONMENT  */
    STRINGT = 299,                 /* STRINGT  */
    SET = 300,                     /* SET  */
    LOWER = 301,                   /* LOWER  */
    UPPER = 302,                   /* UPPER  */
    LOWERFIRST = 303,              /* LOWERFIRST  */
    UPPERFIRST = 304,              /* UPPERFIRST  */
    QUOTEWILDCARD = 305,           /* QUOTEWILDCARD  */
    LENGTH = 306,                  /* LENGTH  */
    VACATION = 307,                /* VACATION  */
    DAYS = 308,                    /* DAYS  */
    SUBJECT = 309,                 /* SUBJECT  */
    FROM = 310,                    /* FROM  */
    ADDRESSES = 311,               /* ADDRESSES  */
    MIME = 312,                    /* MIME  */
    HANDLE = 313,                  /* HANDLE  */
    SECONDS = 314,                 /* SECONDS  */
    COUNT = 315,                   /* COUNT  */
    VALUE = 316,                   /* VALUE  */
    GT = 317,                      /* GT  */
    GE = 318,                      /* GE  */
    LT = 319,                      /* LT  */
    LE = 320,                      /* LE  */
    EQ = 321,                      /* EQ  */
    NE = 322,                      /* NE  */
    FLAGS = 323,                   /* FLAGS  */
    HASFLAG = 324,                 /* HASFLAG  */
    SETFLAG = 325,                 /* SETFLAG  */
    ADDFLAG = 326,                 /* ADDFLAG  */
    REMOVEFLAG = 327,              /* REMOVEFLAG  */
    USER = 328,                    /* USER  */
    DETAIL = 329,                  /* DETAIL  */
    DATE = 330,                    /* DATE  */
    CURRENTDATE = 331,             /* CURRENTDATE  */
    ORIGINALZONE = 332,            /* ORIGINALZONE  */
    ZONE = 333,                    /* ZONE  */
    INDEX = 334,                   /* INDEX  */
    LAST = 335,                    /* LAST  */
    ADDHEADER = 336,               /* ADDHEADER  */
    DELETEHEADER = 337,            /* DELETEHEADER  */
    REJCT = 338,                   /* REJCT  */
    EREJECT = 339,                 /* EREJECT  */
    OPTIONS = 340,                 /* OPTIONS  */
    MESSAGE = 341,                 /* MESSAGE  */
    IMPORTANCE = 342,              /* IMPORTANCE  */
    VALIDNOTIFYMETHOD = 343,       /* VALIDNOTIFYMETHOD  */
    NOTIFYMETHODCAPABILITY = 344,  /* NOTIFYMETHODCAPABILITY  */
    NOTIFY = 345,                  /* NOTIFY  */
    ENCODEURL = 346,               /* ENCODEURL  */
    LOW = 347,                     /* LOW  */
    NORMAL = 348,                  /* NORMAL  */
    HIGH = 349,                    /* HIGH  */
    IHAVE = 350,                   /* IHAVE  */
    ERROR = 351,                   /* ERROR  */
    MAILBOXEXISTS = 352,           /* MAILBOXEXISTS  */
    CREATE = 353,                  /* CREATE  */
    METADATA = 354,                /* METADATA  */
    METADATAEXISTS = 355,          /* METADATAEXISTS  */
    SERVERMETADATA = 356,          /* SERVERMETADATA  */
    SERVERMETADATAEXISTS = 357,    /* SERVERMETADATAEXISTS  */
    BYTIMEREL = 358,               /* BYTIMEREL  */
    BYTIMEABS = 359,               /* BYTIMEABS  */
    BYMODE = 360,                  /* BYMODE  */
    BYTRACE = 361,                 /* BYTRACE  */
    DSNNOTIFY = 362,               /* DSNNOTIFY  */
    DSNRET = 363,                  /* DSNRET  */
    VALIDEXTLIST = 364,            /* VALIDEXTLIST  */
    LIST = 365,                    /* LIST  */
    INCLUDE = 366,                 /* INCLUDE  */
    OPTIONAL = 367,                /* OPTIONAL  */
    ONCE = 368,                    /* ONCE  */
    RETURN = 369,                  /* RETURN  */
    PERSONAL = 370,                /* PERSONAL  */
    GLOBAL = 371,                  /* GLOBAL  */
    DUPLICATE = 372,               /* DUPLICATE  */
    HEADER = 373,                  /* HEADER  */
    UNIQUEID = 374,                /* UNIQUEID  */
    SPECIALUSEEXISTS = 375,        /* SPECIALUSEEXISTS  */
    SPECIALUSE = 376,              /* SPECIALUSE  */
    FCC = 377,                     /* FCC  */
    MAILBOXID = 378,               /* MAILBOXID  */
    MAILBOXIDEXISTS = 379,         /* MAILBOXIDEXISTS  */
    SNOOZE = 380,                  /* SNOOZE  */
    MAILBOX = 381,                 /* MAILBOX  */
    ADDFLAGS = 382,                /* ADDFLAGS  */
    REMOVEFLAGS = 383,             /* REMOVEFLAGS  */
    WEEKDAYS = 384,                /* WEEKDAYS  */
    TZID = 385,                    /* TZID  */
    LOG = 386,                     /* LOG  */
    JMAPQUERY = 387,               /* JMAPQUERY  */
    PROCESSIMIP = 388,             /* PROCESSIMIP  */
    INVITESONLY = 389,             /* INVITESONLY  */
    UPDATESONLY = 390,             /* UPDATESONLY  */
    DELETECANCELED = 391,          /* DELETECANCELED  */
    CALENDARID = 392,              /* CALENDARID  */
    OUTCOME = 393,                 /* OUTCOME  */
    ERRSTR = 394,                  /* ERRSTR  */
    IKEEP_TARGET = 395             /* IKEEP_TARGET  */
  };
  typedef enum yytokentype yytoken_kind_t;
#endif
/* Token kinds.  */
#define YYEMPTY -2
#define YYEOF 0
#define YYerror 256
#define YYUNDEF 257
#define NUMBER 258
#define STRING 259
#define IF 260
#define ELSIF 261
#define ELSE 262
#define REQUIRE 263
#define STOP 264
#define DISCARD 265
#define KEEP 266
#define FILEINTO 267
#define REDIRECT 268
#define ANYOF 269
#define ALLOF 270
#define EXISTS 271
#define NOT 272
#define SFALSE 273
#define STRUE 274
#define SIZE 275
#define HEADERT 276
#define ADDRESS 277
#define ENVELOPE 278
#define COMPARATOR 279
#define OVER 280
#define UNDER 281
#define ALL 282
#define LOCALPART 283
#define DOMAIN 284
#define IS 285
#define CONTAINS 286
#define MATCHES 287
#define OCTET 288
#define ASCIICASEMAP 289
#define ASCIINUMERIC 290
#define REGEX 291
#define QUOTEREGEX 292
#define COPY 293
#define BODY 294
#define RAW 295
#define TEXT 296
#define CONTENT 297
#define ENVIRONMENT 298
#define STRINGT 299
#define SET 300
#define LOWER 301
#define UPPER 302
#define LOWERFIRST 303
#define UPPERFIRST 304
#define QUOTEWILDCARD 305
#define LENGTH 306
#define VACATION 307
#define DAYS 308
#define SUBJECT 309
#define FROM 310
#define ADDRESSES 311
#define MIME 312
#define HANDLE 313
#define SECONDS 314
#define COUNT 315
#define VALUE 316
#define GT 317
#define GE 318
#define LT 319
#define LE 320
#define EQ 321
#define NE 322
#define FLAGS 323
#define HASFLAG 324
#define SETFLAG 325
#define ADDFLAG 326
#define REMOVEFLAG 327
#define USER 328
#define DETAIL 329
#define DATE 330
#define CURRENTDATE 331
#define ORIGINALZONE 332
#define ZONE 333
#define INDEX 334
#define LAST 335
#define ADDHEADER 336
#define DELETEHEADER 337
#define REJCT 338
#define EREJECT 339
#define OPTIONS 340
#define MESSAGE 341
#define IMPORTANCE 342
#define VALIDNOTIFYMETHOD 343
#define NOTIFYMETHODCAPABILITY 344
#define NOTIFY 345
#define ENCODEURL 346
#define LOW 347
#define NORMAL 348
#define HIGH 349
#define IHAVE 350
#define ERROR 351
#define MAILBOXEXISTS 352
#define CREATE 353
#define METADATA 354
#define METADATAEXISTS 355
#define SERVERMETADATA 356
#define SERVERMETADATAEXISTS 357
#define BYTIMEREL 358
#define BYTIMEABS 359
#define BYMODE 360
#define BYTRACE 361
#define DSNNOTIFY 362
#define DSNRET 363
#define VALIDEXTLIST 364
#define LIST 365
#define INCLUDE 366
#define OPTIONAL 367
#define ONCE 368
#define RETURN 369
#define PERSONAL 370
#define GLOBAL 371
#define DUPLICATE 372
#define HEADER 373
#define UNIQUEID 374
#define SPECIALUSEEXISTS 375
#define SPECIALUSE 376
#define FCC 377
#define MAILBOXID 378
#define MAILBOXIDEXISTS 379
#define SNOOZE 380
#define MAILBOX 381
#define ADDFLAGS 382
#define REMOVEFLAGS 383
#define WEEKDAYS 384
#define TZID 385
#define LOG 386
#define JMAPQUERY 387
#define PROCESSIMIP 388
#define INVITESONLY 389
#define UPDATESONLY 390
#define DELETECANCELED 391
#define CALENDARID 392
#define OUTCOME 393
#define ERRSTR 394
#define IKEEP_TARGET 395

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
union YYSTYPE
{
#line 180 "sieve/sieve.y"

    int nval;
    char *sval;
    arrayu64_t *nl;
    strarray_t *sl;
    comp_t *ctag;
    test_t *test;
    testlist_t *testl;
    commandlist_t *cl;

#line 586 "sieve/sieve.c"

};
typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif




int sieveparse (sieve_script_t *sscript);


#endif /* !YY_SIEVE_SIEVE_SIEVE_H_INCLUDED  */
/* Symbol kind.  */
enum yysymbol_kind_t
{
  YYSYMBOL_YYEMPTY = -2,
  YYSYMBOL_YYEOF = 0,                      /* "end of file"  */
  YYSYMBOL_YYerror = 1,                    /* error  */
  YYSYMBOL_YYUNDEF = 2,                    /* "invalid token"  */
  YYSYMBOL_NUMBER = 3,                     /* NUMBER  */
  YYSYMBOL_STRING = 4,                     /* STRING  */
  YYSYMBOL_IF = 5,                         /* IF  */
  YYSYMBOL_ELSIF = 6,                      /* ELSIF  */
  YYSYMBOL_ELSE = 7,                       /* ELSE  */
  YYSYMBOL_REQUIRE = 8,                    /* REQUIRE  */
  YYSYMBOL_STOP = 9,                       /* STOP  */
  YYSYMBOL_DISCARD = 10,                   /* DISCARD  */
  YYSYMBOL_KEEP = 11,                      /* KEEP  */
  YYSYMBOL_FILEINTO = 12,                  /* FILEINTO  */
  YYSYMBOL_REDIRECT = 13,                  /* REDIRECT  */
  YYSYMBOL_ANYOF = 14,                     /* ANYOF  */
  YYSYMBOL_ALLOF = 15,                     /* ALLOF  */
  YYSYMBOL_EXISTS = 16,                    /* EXISTS  */
  YYSYMBOL_NOT = 17,                       /* NOT  */
  YYSYMBOL_SFALSE = 18,                    /* SFALSE  */
  YYSYMBOL_STRUE = 19,                     /* STRUE  */
  YYSYMBOL_SIZE = 20,                      /* SIZE  */
  YYSYMBOL_HEADERT = 21,                   /* HEADERT  */
  YYSYMBOL_ADDRESS = 22,                   /* ADDRESS  */
  YYSYMBOL_ENVELOPE = 23,                  /* ENVELOPE  */
  YYSYMBOL_COMPARATOR = 24,                /* COMPARATOR  */
  YYSYMBOL_OVER = 25,                      /* OVER  */
  YYSYMBOL_UNDER = 26,                     /* UNDER  */
  YYSYMBOL_ALL = 27,                       /* ALL  */
  YYSYMBOL_LOCALPART = 28,                 /* LOCALPART  */
  YYSYMBOL_DOMAIN = 29,                    /* DOMAIN  */
  YYSYMBOL_IS = 30,                        /* IS  */
  YYSYMBOL_CONTAINS = 31,                  /* CONTAINS  */
  YYSYMBOL_MATCHES = 32,                   /* MATCHES  */
  YYSYMBOL_OCTET = 33,                     /* OCTET  */
  YYSYMBOL_ASCIICASEMAP = 34,              /* ASCIICASEMAP  */
  YYSYMBOL_ASCIINUMERIC = 35,              /* ASCIINUMERIC  */
  YYSYMBOL_REGEX = 36,                     /* REGEX  */
  YYSYMBOL_QUOTEREGEX = 37,                /* QUOTEREGEX  */
  YYSYMBOL_COPY = 38,                      /* COPY  */
  YYSYMBOL_BODY = 39,                      /* BODY  */
  YYSYMBOL_RAW = 40,                       /* RAW  */
  YYSYMBOL_TEXT = 41,                      /* TEXT  */
  YYSYMBOL_CONTENT = 42,                   /* CONTENT  */
  YYSYMBOL_ENVIRONMENT = 43,               /* ENVIRONMENT  */
  YYSYMBOL_STRINGT = 44,                   /* STRINGT  */
  YYSYMBOL_SET = 45,                       /* SET  */
  YYSYMBOL_LOWER = 46,                     /* LOWER  */
  YYSYMBOL_UPPER = 47,                     /* UPPER  */
  YYSYMBOL_LOWERFIRST = 48,                /* LOWERFIRST  */
  YYSYMBOL_UPPERFIRST = 49,                /* UPPERFIRST  */
  YYSYMBOL_QUOTEWILDCARD = 50,             /* QUOTEWILDCARD  */
  YYSYMBOL_LENGTH = 51,                    /* LENGTH  */
  YYSYMBOL_VACATION = 52,                  /* VACATION  */
  YYSYMBOL_DAYS = 53,                      /* DAYS  */
  YYSYMBOL_SUBJECT = 54,                   /* SUBJECT  */
  YYSYMBOL_FROM = 55,                      /* FROM  */
  YYSYMBOL_ADDRESSES = 56,                 /* ADDRESSES  */
  YYSYMBOL_MIME = 57,                      /* MIME  */
  YYSYMBOL_HANDLE = 58,                    /* HANDLE  */
  YYSYMBOL_SECONDS = 59,                   /* SECONDS  */
  YYSYMBOL_COUNT = 60,                     /* COUNT  */
  YYSYMBOL_VALUE = 61,                     /* VALUE  */
  YYSYMBOL_GT = 62,                        /* GT  */
  YYSYMBOL_GE = 63,                        /* GE  */
  YYSYMBOL_LT = 64,                        /* LT  */
  YYSYMBOL_LE = 65,                        /* LE  */
  YYSYMBOL_EQ = 66,                        /* EQ  */
  YYSYMBOL_NE = 67,                        /* NE  */
  YYSYMBOL_FLAGS = 68,                     /* FLAGS  */
  YYSYMBOL_HASFLAG = 69,                   /* HASFLAG  */
  YYSYMBOL_SETFLAG = 70,                   /* SETFLAG  */
  YYSYMBOL_ADDFLAG = 71,                   /* ADDFLAG  */
  YYSYMBOL_REMOVEFLAG = 72,                /* REMOVEFLAG  */
  YYSYMBOL_USER = 73,                      /* USER  */
  YYSYMBOL_DETAIL = 74,                    /* DETAIL  */
  YYSYMBOL_DATE = 75,                      /* DATE  */
  YYSYMBOL_CURRENTDATE = 76,               /* CURRENTDATE  */
  YYSYMBOL_ORIGINALZONE = 77,              /* ORIGINALZONE  */
  YYSYMBOL_ZONE = 78,                      /* ZONE  */
  YYSYMBOL_INDEX = 79,                     /* INDEX  */
  YYSYMBOL_LAST = 80,                      /* LAST  */
  YYSYMBOL_ADDHEADER = 81,                 /* ADDHEADER  */
  YYSYMBOL_DELETEHEADER = 82,              /* DELETEHEADER  */
  YYSYMBOL_REJCT = 83,                     /* REJCT  */
  YYSYMBOL_EREJECT = 84,                   /* EREJECT  */
  YYSYMBOL_OPTIONS = 85,                   /* OPTIONS  */
  YYSYMBOL_MESSAGE = 86,                   /* MESSAGE  */
  YYSYMBOL_IMPORTANCE = 87,                /* IMPORTANCE  */
  YYSYMBOL_VALIDNOTIFYMETHOD = 88,         /* VALIDNOTIFYMETHOD  */
  YYSYMBOL_NOTIFYMETHODCAPABILITY = 89,    /* NOTIFYMETHODCAPABILITY  */
  YYSYMBOL_NOTIFY = 90,                    /* NOTIFY  */
  YYSYMBOL_ENCODEURL = 91,                 /* ENCODEURL  */
  YYSYMBOL_LOW = 92,                       /* LOW  */
  YYSYMBOL_NORMAL = 93,                    /* NORMAL  */
  YYSYMBOL_HIGH = 94,                      /* HIGH  */
  YYSYMBOL_IHAVE = 95,                     /* IHAVE  */
  YYSYMBOL_ERROR = 96,                     /* ERROR  */
  YYSYMBOL_MAILBOXEXISTS = 97,             /* MAILBOXEXISTS  */
  YYSYMBOL_CREATE = 98,                    /* CREATE  */
  YYSYMBOL_METADATA = 99,                  /* METADATA  */
  YYSYMBOL_METADATAEXISTS = 100,           /* METADATAEXISTS  */
  YYSYMBOL_SERVERMETADATA = 101,           /* SERVERMETADATA  */
  YYSYMBOL_SERVERMETADATAEXISTS = 102,     /* SERVERMETADATAEXISTS  */
  YYSYMBOL_BYTIMEREL = 103,                /* BYTIMEREL  */
  YYSYMBOL_BYTIMEABS = 104,                /* BYTIMEABS  */
  YYSYMBOL_BYMODE = 105,                   /* BYMODE  */
  YYSYMBOL_BYTRACE = 106,                  /* BYTRACE  */
  YYSYMBOL_DSNNOTIFY = 107,                /* DSNNOTIFY  */
  YYSYMBOL_DSNRET = 108,                   /* DSNRET  */
  YYSYMBOL_VALIDEXTLIST = 109,             /* VALIDEXTLIST  */
  YYSYMBOL_LIST = 110,                     /* LIST  */
  YYSYMBOL_INCLUDE = 111,                  /* INCLUDE  */
  YYSYMBOL_OPTIONAL = 112,                 /* OPTIONAL  */
  YYSYMBOL_ONCE = 113,                     /* ONCE  */
  YYSYMBOL_RETURN = 114,                   /* RETURN  */
  YYSYMBOL_PERSONAL = 115,                 /* PERSONAL  */
  YYSYMBOL_GLOBAL = 116,                   /* GLOBAL  */
  YYSYMBOL_DUPLICATE = 117,                /* DUPLICATE  */
  YYSYMBOL_HEADER = 118,                   /* HEADER  */
  YYSYMBOL_UNIQUEID = 119,                 /* UNIQUEID  */
  YYSYMBOL_SPECIALUSEEXISTS = 120,         /* SPECIALUSEEXISTS  */
  YYSYMBOL_SPECIALUSE = 121,               /* SPECIALUSE  */
  YYSYMBOL_FCC = 122,                      /* FCC  */
  YYSYMBOL_MAILBOXID = 123,                /* MAILBOXID  */
  YYSYMBOL_MAILBOXIDEXISTS = 124,          /* MAILBOXIDEXISTS  */
  YYSYMBOL_SNOOZE = 125,                   /* SNOOZE  */
  YYSYMBOL_MAILBOX = 126,                  /* MAILBOX  */
  YYSYMBOL_ADDFLAGS = 127,                 /* ADDFLAGS  */
  YYSYMBOL_REMOVEFLAGS = 128,              /* REMOVEFLAGS  */
  YYSYMBOL_WEEKDAYS = 129,                 /* WEEKDAYS  */
  YYSYMBOL_TZID = 130,                     /* TZID  */
  YYSYMBOL_LOG = 131,                      /* LOG  */
  YYSYMBOL_JMAPQUERY = 132,                /* JMAPQUERY  */
  YYSYMBOL_PROCESSIMIP = 133,              /* PROCESSIMIP  */
  YYSYMBOL_INVITESONLY = 134,              /* INVITESONLY  */
  YYSYMBOL_UPDATESONLY = 135,              /* UPDATESONLY  */
  YYSYMBOL_DELETECANCELED = 136,           /* DELETECANCELED  */
  YYSYMBOL_CALENDARID = 137,               /* CALENDARID  */
  YYSYMBOL_OUTCOME = 138,                  /* OUTCOME  */
  YYSYMBOL_ERRSTR = 139,                   /* ERRSTR  */
  YYSYMBOL_IKEEP_TARGET = 140,             /* IKEEP_TARGET  */
  YYSYMBOL_141_ = 141,                     /* ';'  */
  YYSYMBOL_142_ = 142,                     /* '['  */
  YYSYMBOL_143_ = 143,                     /* ']'  */
  YYSYMBOL_144_ = 144,                     /* ','  */
  YYSYMBOL_145_ = 145,                     /* '{'  */
  YYSYMBOL_146_ = 146,                     /* '}'  */
  YYSYMBOL_147_ = 147,                     /* '('  */
  YYSYMBOL_148_ = 148,                     /* ')'  */
  YYSYMBOL_YYACCEPT = 149,                 /* $accept  */
  YYSYMBOL_start = 150,                    /* start  */
  YYSYMBOL_reqs = 151,                     /* reqs  */
  YYSYMBOL_commands = 152,                 /* commands  */
  YYSYMBOL_command = 153,                  /* command  */
  YYSYMBOL_optstringlist = 154,            /* optstringlist  */
  YYSYMBOL_stringlist = 155,               /* stringlist  */
  YYSYMBOL_strings = 156,                  /* strings  */
  YYSYMBOL_string1 = 157,                  /* string1  */
  YYSYMBOL_string = 158,                   /* string  */
  YYSYMBOL_require = 159,                  /* require  */
  YYSYMBOL_control = 160,                  /* control  */
  YYSYMBOL_thenelse = 161,                 /* thenelse  */
  YYSYMBOL_elsif = 162,                    /* elsif  */
  YYSYMBOL_block = 163,                    /* block  */
  YYSYMBOL_itags = 164,                    /* itags  */
  YYSYMBOL_location = 165,                 /* location  */
  YYSYMBOL_action = 166,                   /* action  */
  YYSYMBOL_ktags = 167,                    /* ktags  */
  YYSYMBOL_flags = 168,                    /* flags  */
  YYSYMBOL_ftags = 169,                    /* ftags  */
  YYSYMBOL_copy = 170,                     /* copy  */
  YYSYMBOL_create = 171,                   /* create  */
  YYSYMBOL_specialuse = 172,               /* specialuse  */
  YYSYMBOL_mailboxid = 173,                /* mailboxid  */
  YYSYMBOL_rtags = 174,                    /* rtags  */
  YYSYMBOL_delbytags = 175,                /* delbytags  */
  YYSYMBOL_dsntags = 176,                  /* dsntags  */
  YYSYMBOL_stags = 177,                    /* stags  */
  YYSYMBOL_mod40 = 178,                    /* mod40  */
  YYSYMBOL_mod30 = 179,                    /* mod30  */
  YYSYMBOL_mod20 = 180,                    /* mod20  */
  YYSYMBOL_mod15 = 181,                    /* mod15  */
  YYSYMBOL_mod10 = 182,                    /* mod10  */
  YYSYMBOL_vtags = 183,                    /* vtags  */
  YYSYMBOL_fcctags = 184,                  /* fcctags  */
  YYSYMBOL_flagaction = 185,               /* flagaction  */
  YYSYMBOL_flagtags = 186,                 /* flagtags  */
  YYSYMBOL_ahtags = 187,                   /* ahtags  */
  YYSYMBOL_dhtags = 188,                   /* dhtags  */
  YYSYMBOL_reject = 189,                   /* reject  */
  YYSYMBOL_ntags = 190,                    /* ntags  */
  YYSYMBOL_priority = 191,                 /* priority  */
  YYSYMBOL_sntags = 192,                   /* sntags  */
  YYSYMBOL_weekdaylist = 193,              /* weekdaylist  */
  YYSYMBOL_weekdays = 194,                 /* weekdays  */
  YYSYMBOL_weekday = 195,                  /* weekday  */
  YYSYMBOL_timelist = 196,                 /* timelist  */
  YYSYMBOL_times = 197,                    /* times  */
  YYSYMBOL_time1 = 198,                    /* time1  */
  YYSYMBOL_time = 199,                     /* time  */
  YYSYMBOL_imiptags = 200,                 /* imiptags  */
  YYSYMBOL_ikttags = 201,                  /* ikttags  */
  YYSYMBOL_testlist = 202,                 /* testlist  */
  YYSYMBOL_tests = 203,                    /* tests  */
  YYSYMBOL_test = 204,                     /* test  */
  YYSYMBOL_205_1 = 205,                    /* $@1  */
  YYSYMBOL_206_2 = 206,                    /* $@2  */
  YYSYMBOL_sizetag = 207,                  /* sizetag  */
  YYSYMBOL_htags = 208,                    /* htags  */
  YYSYMBOL_matchtype = 209,                /* matchtype  */
  YYSYMBOL_matchtag = 210,                 /* matchtag  */
  YYSYMBOL_relmatch = 211,                 /* relmatch  */
  YYSYMBOL_relation = 212,                 /* relation  */
  YYSYMBOL_listmatch = 213,                /* listmatch  */
  YYSYMBOL_comparator = 214,               /* comparator  */
  YYSYMBOL_collation = 215,                /* collation  */
  YYSYMBOL_indexext = 216,                 /* indexext  */
  YYSYMBOL_index = 217,                    /* index  */
  YYSYMBOL_atags = 218,                    /* atags  */
  YYSYMBOL_addrpart = 219,                 /* addrpart  */
  YYSYMBOL_addrparttag = 220,              /* addrparttag  */
  YYSYMBOL_subaddress = 221,               /* subaddress  */
  YYSYMBOL_etags = 222,                    /* etags  */
  YYSYMBOL_envtags = 223,                  /* envtags  */
  YYSYMBOL_btags = 224,                    /* btags  */
  YYSYMBOL_transform = 225,                /* transform  */
  YYSYMBOL_strtags = 226,                  /* strtags  */
  YYSYMBOL_hftags = 227,                   /* hftags  */
  YYSYMBOL_dttags = 228,                   /* dttags  */
  YYSYMBOL_zone = 229,                     /* zone  */
  YYSYMBOL_cdtags = 230,                   /* cdtags  */
  YYSYMBOL_methtags = 231,                 /* methtags  */
  YYSYMBOL_mtags = 232,                    /* mtags  */
  YYSYMBOL_duptags = 233,                  /* duptags  */
  YYSYMBOL_idtype = 234                    /* idtype  */
};
typedef enum yysymbol_kind_t yysymbol_kind_t;




#ifdef short
# undef short
#endif

/* On compilers that do not define __PTRDIFF_MAX__ etc., make sure
   <limits.h> and (if available) <stdint.h> are included
   so that the code can choose integer types of a good width.  */

#ifndef __PTRDIFF_MAX__
# include <limits.h> /* INFRINGES ON USER NAME SPACE */
# if defined __STDC_VERSION__ && 199901 <= __STDC_VERSION__
#  include <stdint.h> /* INFRINGES ON USER NAME SPACE */
#  define YY_STDINT_H
# endif
#endif

/* Narrow types that promote to a signed type and that can represent a
   signed or unsigned integer of at least N bits.  In tables they can
   save space and decrease cache pressure.  Promoting to a signed type
   helps avoid bugs in integer arithmetic.  */

#ifdef __INT_LEAST8_MAX__
typedef __INT_LEAST8_TYPE__ yytype_int8;
#elif defined YY_STDINT_H
typedef int_least8_t yytype_int8;
#else
typedef signed char yytype_int8;
#endif

#ifdef __INT_LEAST16_MAX__
typedef __INT_LEAST16_TYPE__ yytype_int16;
#elif defined YY_STDINT_H
typedef int_least16_t yytype_int16;
#else
typedef short yytype_int16;
#endif

/* Work around bug in HP-UX 11.23, which defines these macros
   incorrectly for preprocessor constants.  This workaround can likely
   be removed in 2023, as HPE has promised support for HP-UX 11.23
   (aka HP-UX 11i v2) only through the end of 2022; see Table 2 of
   <https://h20195.www2.hpe.com/V2/getpdf.aspx/4AA4-7673ENW.pdf>.  */
#ifdef __hpux
# undef UINT_LEAST8_MAX
# undef UINT_LEAST16_MAX
# define UINT_LEAST8_MAX 255
# define UINT_LEAST16_MAX 65535
#endif

#if defined __UINT_LEAST8_MAX__ && __UINT_LEAST8_MAX__ <= __INT_MAX__
typedef __UINT_LEAST8_TYPE__ yytype_uint8;
#elif (!defined __UINT_LEAST8_MAX__ && defined YY_STDINT_H \
       && UINT_LEAST8_MAX <= INT_MAX)
typedef uint_least8_t yytype_uint8;
#elif !defined __UINT_LEAST8_MAX__ && UCHAR_MAX <= INT_MAX
typedef unsigned char yytype_uint8;
#else
typedef short yytype_uint8;
#endif

#if defined __UINT_LEAST16_MAX__ && __UINT_LEAST16_MAX__ <= __INT_MAX__
typedef __UINT_LEAST16_TYPE__ yytype_uint16;
#elif (!defined __UINT_LEAST16_MAX__ && defined YY_STDINT_H \
       && UINT_LEAST16_MAX <= INT_MAX)
typedef uint_least16_t yytype_uint16;
#elif !defined __UINT_LEAST16_MAX__ && USHRT_MAX <= INT_MAX
typedef unsigned short yytype_uint16;
#else
typedef int yytype_uint16;
#endif

#ifndef YYPTRDIFF_T
# if defined __PTRDIFF_TYPE__ && defined __PTRDIFF_MAX__
#  define YYPTRDIFF_T __PTRDIFF_TYPE__
#  define YYPTRDIFF_MAXIMUM __PTRDIFF_MAX__
# elif defined PTRDIFF_MAX
#  ifndef ptrdiff_t
#   include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  endif
#  define YYPTRDIFF_T ptrdiff_t
#  define YYPTRDIFF_MAXIMUM PTRDIFF_MAX
# else
#  define YYPTRDIFF_T long
#  define YYPTRDIFF_MAXIMUM LONG_MAX
# endif
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif defined __STDC_VERSION__ && 199901 <= __STDC_VERSION__
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned
# endif
#endif

#define YYSIZE_MAXIMUM                                  \
  YY_CAST (YYPTRDIFF_T,                                 \
           (YYPTRDIFF_MAXIMUM < YY_CAST (YYSIZE_T, -1)  \
            ? YYPTRDIFF_MAXIMUM                         \
            : YY_CAST (YYSIZE_T, -1)))

#define YYSIZEOF(X) YY_CAST (YYPTRDIFF_T, sizeof (X))


/* Stored state numbers (used for stacks). */
typedef yytype_int16 yy_state_t;

/* State numbers in computations.  */
typedef int yy_state_fast_t;

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(Msgid) Msgid
# endif
#endif


#ifndef YY_ATTRIBUTE_PURE
# if defined __GNUC__ && 2 < __GNUC__ + (96 <= __GNUC_MINOR__)
#  define YY_ATTRIBUTE_PURE __attribute__ ((__pure__))
# else
#  define YY_ATTRIBUTE_PURE
# endif
#endif

#ifndef YY_ATTRIBUTE_UNUSED
# if defined __GNUC__ && 2 < __GNUC__ + (7 <= __GNUC_MINOR__)
#  define YY_ATTRIBUTE_UNUSED __attribute__ ((__unused__))
# else
#  define YY_ATTRIBUTE_UNUSED
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YY_USE(E) ((void) (E))
#else
# define YY_USE(E) /* empty */
#endif

/* Suppress an incorrect diagnostic about yylval being uninitialized.  */
#if defined __GNUC__ && ! defined __ICC && 406 <= __GNUC__ * 100 + __GNUC_MINOR__
# if __GNUC__ * 100 + __GNUC_MINOR__ < 407
#  define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN                           \
    _Pragma ("GCC diagnostic push")                                     \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")
# else
#  define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN                           \
    _Pragma ("GCC diagnostic push")                                     \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")              \
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# endif
# define YY_IGNORE_MAYBE_UNINITIALIZED_END      \
    _Pragma ("GCC diagnostic pop")
#else
# define YY_INITIAL_VALUE(Value) Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif

#if defined __cplusplus && defined __GNUC__ && ! defined __ICC && 6 <= __GNUC__
# define YY_IGNORE_USELESS_CAST_BEGIN                          \
    _Pragma ("GCC diagnostic push")                            \
    _Pragma ("GCC diagnostic ignored \"-Wuseless-cast\"")
# define YY_IGNORE_USELESS_CAST_END            \
    _Pragma ("GCC diagnostic pop")
#endif
#ifndef YY_IGNORE_USELESS_CAST_BEGIN
# define YY_IGNORE_USELESS_CAST_BEGIN
# define YY_IGNORE_USELESS_CAST_END
#endif


#define YY_ASSERT(E) ((void) (0 && (E)))

#if !defined yyoverflow

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
      /* Use EXIT_SUCCESS as a witness for stdlib.h.  */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's 'empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
             && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* !defined yyoverflow */

#if (! defined yyoverflow \
     && (! defined __cplusplus \
         || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yy_state_t yyss_alloc;
  YYSTYPE yyvs_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (YYSIZEOF (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (YYSIZEOF (yy_state_t) + YYSIZEOF (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)                           \
    do                                                                  \
      {                                                                 \
        YYPTRDIFF_T yynewbytes;                                         \
        YYCOPY (&yyptr->Stack_alloc, Stack, yysize);                    \
        Stack = &yyptr->Stack_alloc;                                    \
        yynewbytes = yystacksize * YYSIZEOF (*Stack) + YYSTACK_GAP_MAXIMUM; \
        yyptr += yynewbytes / YYSIZEOF (*yyptr);                        \
      }                                                                 \
    while (0)

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, YY_CAST (YYSIZE_T, (Count)) * sizeof (*(Src)))
#  else
#   define YYCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          YYPTRDIFF_T yyi;                      \
          for (yyi = 0; yyi < (Count); yyi++)   \
            (Dst)[yyi] = (Src)[yyi];            \
        }                                       \
      while (0)
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  10
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   723

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  149
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  86
/* YYNRULES -- Number of rules.  */
#define YYNRULES  281
/* YYNSTATES -- Number of states.  */
#define YYNSTATES  403

/* YYMAXUTOK -- Last valid token kind.  */
#define YYMAXUTOK   395


/* YYTRANSLATE(TOKEN-NUM) -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex, with out-of-bounds checking.  */
#define YYTRANSLATE(YYX)                                \
  (0 <= (YYX) && (YYX) <= YYMAXUTOK                     \
   ? YY_CAST (yysymbol_kind_t, yytranslate[YYX])        \
   : YYSYMBOL_YYUNDEF)

/* YYTRANSLATE[TOKEN-NUM] -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex.  */
static const yytype_uint8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     147,   148,     2,     2,   144,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,   141,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,   142,     2,   143,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,   145,     2,   146,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,    58,    59,    60,    61,    62,    63,    64,
      65,    66,    67,    68,    69,    70,    71,    72,    73,    74,
      75,    76,    77,    78,    79,    80,    81,    82,    83,    84,
      85,    86,    87,    88,    89,    90,    91,    92,    93,    94,
      95,    96,    97,    98,    99,   100,   101,   102,   103,   104,
     105,   106,   107,   108,   109,   110,   111,   112,   113,   114,
     115,   116,   117,   118,   119,   120,   121,   122,   123,   124,
     125,   126,   127,   128,   129,   130,   131,   132,   133,   134,
     135,   136,   137,   138,   139,   140
};

#if YYDEBUG
/* YYRLINE[YYN] -- Source line where rule number YYN was defined.  */
static const yytype_int16 yyrline[] =
{
       0,   364,   364,   365,   369,   370,   375,   376,   380,   381,
     382,   394,   395,   399,   400,   404,   405,   409,   416,   423,
     427,   428,   429,   430,   432,   436,   447,   448,   449,   453,
     454,   459,   460,   469,   478,   491,   492,   499,   500,   501,
     502,   503,   505,   508,   511,   514,   518,   519,   520,   521,
     522,   524,   530,   534,   539,   559,   567,   568,   569,   570,
     571,   576,   593,   610,   635,   661,   665,   666,   678,   685,
     695,   717,   737,   749,   765,   777,   793,   794,   803,   812,
     821,   830,   843,   844,   847,   848,   851,   852,   864,   875,
     880,   888,   897,   911,   921,   932,   943,   952,   962,   966,
     982,   983,   984,   985,   990,   991,   992,   997,   998,  1017,
    1018,  1031,  1035,  1036,  1037,  1038,  1043,  1044,  1049,  1057,
    1068,  1078,  1089,  1100,  1105,  1106,  1107,  1112,  1118,  1128,
    1129,  1130,  1132,  1148,  1164,  1174,  1187,  1188,  1192,  1193,
    1197,  1201,  1202,  1206,  1207,  1211,  1218,  1223,  1224,  1246,
    1268,  1284,  1301,  1317,  1336,  1341,  1342,  1349,  1353,  1354,
    1358,  1359,  1360,  1361,  1362,  1363,  1369,  1378,  1381,  1383,
    1386,  1388,  1392,  1399,  1401,  1404,  1407,  1410,  1417,  1421,
    1423,  1430,  1430,  1434,  1441,  1441,  1445,  1453,  1460,  1462,
    1469,  1476,  1483,  1488,  1493,  1494,  1499,  1503,  1504,  1505,
    1506,  1511,  1521,  1543,  1544,  1545,  1546,  1559,  1560,  1565,
    1566,  1567,  1568,  1569,  1570,  1575,  1595,  1610,  1611,  1612,
    1626,  1637,  1652,  1671,  1675,  1676,  1677,  1678,  1679,  1684,
    1699,  1700,  1701,  1702,  1713,  1714,  1719,  1723,  1724,  1725,
    1726,  1731,  1735,  1736,  1741,  1745,  1755,  1768,  1769,  1774,
    1775,  1780,  1784,  1785,  1786,  1791,  1795,  1796,  1801,  1805,
    1806,  1815,  1816,  1817,  1822,  1839,  1843,  1844,  1845,  1850,
    1855,  1856,  1861,  1866,  1867,  1872,  1873,  1884,  1894,  1903,
    1916,  1917
};
#endif

/** Accessing symbol of state STATE.  */
#define YY_ACCESSING_SYMBOL(State) YY_CAST (yysymbol_kind_t, yystos[State])

#if YYDEBUG || 0
/* The user-facing name of the symbol whose (internal) number is
   YYSYMBOL.  No bounds checking.  */
static const char *yysymbol_name (yysymbol_kind_t yysymbol) YY_ATTRIBUTE_UNUSED;

/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "\"end of file\"", "error", "\"invalid token\"", "NUMBER", "STRING",
  "IF", "ELSIF", "ELSE", "REQUIRE", "STOP", "DISCARD", "KEEP", "FILEINTO",
  "REDIRECT", "ANYOF", "ALLOF", "EXISTS", "NOT", "SFALSE", "STRUE", "SIZE",
  "HEADERT", "ADDRESS", "ENVELOPE", "COMPARATOR", "OVER", "UNDER", "ALL",
  "LOCALPART", "DOMAIN", "IS", "CONTAINS", "MATCHES", "OCTET",
  "ASCIICASEMAP", "ASCIINUMERIC", "REGEX", "QUOTEREGEX", "COPY", "BODY",
  "RAW", "TEXT", "CONTENT", "ENVIRONMENT", "STRINGT", "SET", "LOWER",
  "UPPER", "LOWERFIRST", "UPPERFIRST", "QUOTEWILDCARD", "LENGTH",
  "VACATION", "DAYS", "SUBJECT", "FROM", "ADDRESSES", "MIME", "HANDLE",
  "SECONDS", "COUNT", "VALUE", "GT", "GE", "LT", "LE", "EQ", "NE", "FLAGS",
  "HASFLAG", "SETFLAG", "ADDFLAG", "REMOVEFLAG", "USER", "DETAIL", "DATE",
  "CURRENTDATE", "ORIGINALZONE", "ZONE", "INDEX", "LAST", "ADDHEADER",
  "DELETEHEADER", "REJCT", "EREJECT", "OPTIONS", "MESSAGE", "IMPORTANCE",
  "VALIDNOTIFYMETHOD", "NOTIFYMETHODCAPABILITY", "NOTIFY", "ENCODEURL",
  "LOW", "NORMAL", "HIGH", "IHAVE", "ERROR", "MAILBOXEXISTS", "CREATE",
  "METADATA", "METADATAEXISTS", "SERVERMETADATA", "SERVERMETADATAEXISTS",
  "BYTIMEREL", "BYTIMEABS", "BYMODE", "BYTRACE", "DSNNOTIFY", "DSNRET",
  "VALIDEXTLIST", "LIST", "INCLUDE", "OPTIONAL", "ONCE", "RETURN",
  "PERSONAL", "GLOBAL", "DUPLICATE", "HEADER", "UNIQUEID",
  "SPECIALUSEEXISTS", "SPECIALUSE", "FCC", "MAILBOXID", "MAILBOXIDEXISTS",
  "SNOOZE", "MAILBOX", "ADDFLAGS", "REMOVEFLAGS", "WEEKDAYS", "TZID",
  "LOG", "JMAPQUERY", "PROCESSIMIP", "INVITESONLY", "UPDATESONLY",
  "DELETECANCELED", "CALENDARID", "OUTCOME", "ERRSTR", "IKEEP_TARGET",
  "';'", "'['", "']'", "','", "'{'", "'}'", "'('", "')'", "$accept",
  "start", "reqs", "commands", "command", "optstringlist", "stringlist",
  "strings", "string1", "string", "require", "control", "thenelse",
  "elsif", "block", "itags", "location", "action", "ktags", "flags",
  "ftags", "copy", "create", "specialuse", "mailboxid", "rtags",
  "delbytags", "dsntags", "stags", "mod40", "mod30", "mod20", "mod15",
  "mod10", "vtags", "fcctags", "flagaction", "flagtags", "ahtags",
  "dhtags", "reject", "ntags", "priority", "sntags", "weekdaylist",
  "weekdays", "weekday", "timelist", "times", "time1", "time", "imiptags",
  "ikttags", "testlist", "tests", "test", "$@1", "$@2", "sizetag", "htags",
  "matchtype", "matchtag", "relmatch", "relation", "listmatch",
  "comparator", "collation", "indexext", "index", "atags", "addrpart",
  "addrparttag", "subaddress", "etags", "envtags", "btags", "transform",
  "strtags", "hftags", "dttags", "zone", "cdtags", "methtags", "mtags",
  "duptags", "idtype", YY_NULLPTR
};

static const char *
yysymbol_name (yysymbol_kind_t yysymbol)
{
  return yytname[yysymbol];
}
#endif

#define YYPACT_NINF (-341)

#define yypact_value_is_default(Yyn) \
  ((Yyn) == YYPACT_NINF)

#define YYTABLE_NINF (-18)

#define yytable_value_is_error(Yyn) \
  0

/* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
   STATE-NUM.  */
static const yytype_int16 yypact[] =
{
      10,    -2,    16,   424,    10,  -341,    21,   -97,  -341,  -341,
    -341,   -95,   553,   -93,  -341,  -341,  -341,  -341,  -341,  -341,
    -341,  -341,  -341,  -341,  -341,  -341,  -341,  -341,    21,  -341,
     -91,  -341,    21,  -341,  -341,  -341,   232,  -341,   -56,  -341,
      21,  -341,  -115,  -341,  -341,  -341,  -341,   -61,   -61,    -2,
     553,  -341,  -341,    12,  -341,  -341,  -341,  -341,  -341,  -341,
    -341,  -341,  -341,    -2,  -341,    -2,    -2,  -341,    21,  -341,
      -2,    -2,  -341,    -2,    -2,    21,  -341,   -54,  -341,    37,
      69,   120,   249,   358,     1,   558,   493,   -52,    -1,  -341,
       4,  -341,    35,    45,  -341,  -341,    -2,  -341,  -341,    21,
     553,  -341,  -341,  -341,  -341,  -341,  -341,   100,   251,     3,
      68,   344,   662,   498,   152,   603,   600,  -341,   662,  -341,
    -341,  -341,    -2,  -341,  -341,  -341,   -44,  -341,    -2,  -341,
    -341,   378,    55,    -2,  -341,  -341,  -341,    21,    21,  -341,
    -341,  -341,  -341,  -341,  -341,   103,    21,    21,  -341,    21,
      21,  -341,  -341,  -341,  -341,  -341,  -341,  -341,  -341,  -341,
    -341,  -341,  -341,  -341,    21,  -341,  -341,  -341,  -341,  -341,
     105,    21,    21,    -2,  -341,    21,   113,    21,  -341,  -341,
    -341,  -341,  -341,  -341,  -341,    21,     8,  -341,  -341,  -341,
    -341,  -341,  -341,   114,  -341,  -341,    -2,  -341,  -341,   184,
    -341,  -341,  -341,    21,    -2,    21,   -14,  -341,  -341,  -341,
    -341,  -341,  -341,  -341,   -21,  -341,  -341,    21,    -2,    -2,
       6,    21,   119,  -341,  -341,  -341,  -341,  -341,  -341,  -341,
    -341,  -341,    21,    21,    21,  -341,  -341,  -341,  -341,   -15,
    -341,   -13,    -5,  -341,    -2,  -341,  -341,  -341,  -341,  -341,
    -341,  -341,  -341,  -341,  -341,    -2,  -341,  -341,  -341,  -341,
    -341,  -341,  -341,    -2,  -341,  -341,  -341,  -341,  -341,  -341,
      -2,  -341,  -341,  -341,  -341,    -2,  -341,  -341,    -2,  -341,
    -341,  -341,    -2,  -341,  -341,  -341,    21,    21,  -341,  -341,
    -341,  -341,    -2,  -341,  -341,  -341,    21,  -341,  -341,   662,
    -341,   662,    21,   147,  -341,  -341,  -341,    21,  -341,  -341,
       5,   553,   -54,  -341,  -341,  -341,  -341,  -341,  -341,  -341,
    -341,  -341,  -341,  -341,  -341,  -341,  -341,  -341,  -341,  -341,
    -341,  -341,  -341,  -341,  -341,  -341,  -341,  -341,  -341,  -341,
    -341,  -341,  -341,  -341,  -341,  -341,  -341,  -341,  -341,  -341,
    -341,  -341,  -341,  -341,  -341,  -341,  -341,   149,  -341,  -341,
    -341,   -77,  -341,  -341,  -341,  -341,  -341,   553,  -341,  -341,
    -341,  -341,  -341,  -341,  -341,  -341,    -2,  -341,    -2,    21,
    -341,  -341,    -2,  -341,  -341,  -341,  -341,  -341,  -341,   -74,
    -341,  -341,   119,  -341,  -341,  -341,    -2,  -341,  -341,   149,
    -341,  -341,  -341
};

/* YYDEFACT[STATE-NUM] -- Default reduction number in state STATE-NUM.
   Performed when YYTABLE does not specify something else to do.  Zero
   means the default is an error.  */
static const yytype_int16 yydefact[] =
{
       4,     0,     0,     0,     4,    18,     0,     0,    13,    17,
       1,     0,     0,     0,    40,    52,    55,    65,    76,    90,
     104,   105,   106,   109,   111,   116,   117,   118,     0,    31,
       0,   127,     0,   147,   154,     3,     0,     8,     0,   107,
       0,     5,     0,    15,    19,    10,   193,     0,     0,     0,
       0,   163,   164,     0,   196,   223,   236,   244,   241,   251,
     255,   258,   265,     0,   269,     0,     0,   181,     0,   184,
       0,     0,   275,     0,     0,     0,    20,     0,    21,    37,
       0,     0,     0,     0,     0,     0,     0,     0,     0,    24,
       0,    48,    50,     0,     7,     9,     0,    46,    14,     0,
       0,   160,   161,   165,   162,   194,   195,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   177,     0,   179,
     180,   272,     0,   272,   186,   187,   188,   189,    17,   191,
     192,     0,    26,     0,    53,    61,    62,     0,     0,    38,
      57,    56,    58,    59,    60,     0,     0,     0,    73,     0,
       0,    67,    39,    66,    68,    69,    87,    82,    83,    84,
      85,    86,    89,    88,     0,    77,    78,    79,    80,    81,
       0,     0,     0,     0,    96,     0,     0,     0,    42,   101,
     100,   102,   103,    98,   110,     0,     0,   203,   204,   205,
     206,   207,   208,     0,   222,   215,    11,   112,   201,     0,
     113,   114,   115,     0,     0,     0,     0,    47,   123,    22,
      34,    33,    35,    36,     0,    32,   146,     0,     0,     0,
       0,     0,     0,   129,   130,   131,    49,   141,   145,   148,
     149,   150,     0,     0,     0,    51,   155,   156,    43,   108,
      16,     0,   158,   166,     0,   197,   198,   199,   200,   220,
     230,   231,   232,   234,   235,     0,   225,   226,   227,   228,
     224,   229,   233,     0,   238,   239,   240,   237,   249,   250,
       0,   170,   247,   248,   245,     0,   242,   243,     0,   252,
     253,   254,   174,   256,   257,   260,     0,     0,   261,   262,
     263,   259,     0,   267,   268,   266,     0,   270,   271,     0,
     183,     0,     0,     0,   279,   280,   281,     0,   190,    30,
       0,     0,     0,    25,    54,    63,    64,    70,    71,    72,
      74,    75,    41,    91,    93,    94,    95,    97,    92,    99,
      44,   217,   218,   219,   216,   221,    45,    12,   211,   212,
     213,   214,   209,   210,   202,   119,   122,   121,   124,   125,
     126,   120,    23,   128,   132,   133,   140,     0,   134,   136,
     135,     0,   143,   151,   152,   153,   157,     0,   167,   168,
     169,   246,   171,   172,   173,   264,     0,   176,     0,     0,
     273,   274,     0,   277,   278,   276,    29,    27,    28,     0,
     138,   142,     0,   159,   175,   178,     0,   185,   137,     0,
     144,   182,   139
};

/* YYPGOTO[NTERM-NUM].  */
static const yytype_int16 yypgoto[] =
{
    -341,  -341,   157,   -30,  -341,  -341,    89,  -341,   158,   -28,
    -341,  -341,  -146,  -341,  -133,  -341,  -341,  -341,  -341,    14,
    -341,    99,   -67,   -71,   -69,  -341,  -341,  -341,  -341,  -341,
    -341,  -341,  -341,  -341,  -341,    95,  -341,  -341,  -341,  -341,
    -341,  -341,  -341,  -341,  -341,  -341,  -340,  -341,  -341,   -36,
    -205,  -341,  -341,   143,  -173,   -49,  -341,  -341,  -341,  -341,
     591,  -341,  -341,  -341,   106,   602,  -341,   -89,   110,  -341,
      86,  -341,  -341,  -341,  -341,  -341,  -341,  -341,  -341,  -341,
      85,  -341,  -341,    84,  -341,  -341
};

/* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int16 yydefgoto[] =
{
       0,     2,     3,    35,    36,   336,     7,    42,     8,     9,
       4,    37,    76,   313,   132,    88,   215,    38,    79,   179,
      80,   141,   180,   181,   182,    81,   154,   155,    82,   165,
     166,   167,   168,   169,    83,   183,    39,    96,    84,    85,
      40,    86,   351,    90,   358,   389,   359,   226,   361,   227,
     228,    92,    93,   101,   241,    77,   121,   123,   107,   108,
     380,   198,   199,   344,   200,   381,   334,   248,   249,   109,
     260,   261,   262,   110,   112,   111,   274,   113,   114,   115,
     291,   116,   118,   299,   126,   307
};

/* YYTABLE[YYPACT[STATE-NUM]] -- What to do in state STATE-NUM.  If
   positive, shift that token.  If negative, reduce the rule whose
   number is the opposite.  If YYTABLE_NINF, syntax error.  */
static const yytype_int16 yytable[] =
{
      87,   104,     5,     5,    91,     5,    94,     5,   216,   143,
     356,   144,    97,   142,   302,   303,    10,   390,     1,   224,
     259,   225,   236,   223,   237,     5,   290,   186,    98,    99,
     250,   251,   252,   187,   188,   189,   304,   105,   106,   190,
     122,   331,   332,   333,    44,   128,    45,   130,    78,     5,
      89,   242,   139,   152,   164,   178,   185,   196,   207,   402,
     214,   311,   312,   191,   192,   235,   391,   392,   239,   398,
     399,   240,     5,     5,   305,   306,   253,   254,   348,   349,
     350,   184,   193,   194,   275,    95,   100,   287,   292,   209,
     296,   131,   186,   134,   140,   250,   251,   252,   187,   188,
     189,   310,   136,   243,   190,   133,   317,   135,   323,   315,
     316,   210,   211,   195,   212,   213,   328,   335,   318,   319,
     352,   320,   321,   216,     5,   137,   -17,   138,   191,   192,
     217,   218,   219,   220,   221,   366,   322,   133,   103,   367,
       6,   253,   254,   324,   325,     6,   222,   327,   357,   329,
     384,   386,   117,   356,   119,   120,     5,   330,   135,   124,
     125,    41,   127,   129,    43,   387,   137,   136,   138,   229,
     230,   231,   232,   233,   234,   345,   186,   347,   195,   388,
     153,   208,   187,   188,   189,   238,   362,   400,   190,   353,
     137,   102,   138,   360,   393,   202,   267,   244,   255,   263,
     271,   295,   278,   282,   363,   364,   365,   301,     0,     0,
       6,   300,   191,   192,   246,   257,   265,   308,     0,   280,
       0,     0,   314,   145,   146,   147,   148,   149,   150,     0,
     151,     0,    -6,    11,     0,     0,     0,    12,     0,     0,
       0,    13,    14,    15,    16,    17,   338,   339,   340,   341,
     342,   343,     0,     5,     0,     5,     0,     0,   375,   376,
       0,     0,   326,     0,     0,     0,     0,     0,   378,     0,
       0,   379,     0,   382,   383,   186,     0,    18,     0,   385,
       0,   187,   188,   189,    19,   337,   156,   190,     0,     0,
       0,     0,     0,   346,     6,   157,   158,   159,   160,   161,
     162,     0,    20,    21,    22,     0,     0,   354,   355,     0,
       0,   191,   192,    23,    24,    25,    26,     0,   242,     0,
       0,     0,    27,     0,     0,     0,     0,     0,    28,     0,
     193,   194,     0,   368,     0,     0,     0,     0,     0,     0,
     163,     0,     0,    29,   369,     0,    30,     0,     5,     0,
       0,   396,   370,     0,     0,     0,     0,    31,     0,   371,
       0,   195,     5,    32,   372,    33,     0,   373,   186,     0,
       0,   374,    34,     0,   187,   188,   189,     0,    -6,    11,
     190,   377,     0,    12,   268,   269,   270,    13,    14,    15,
      16,    17,     0,     6,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   191,   192,     0,     0,     0,     0,
       0,   170,   171,   172,   173,   174,   175,   176,     0,     0,
       0,     0,     0,    18,    -2,    11,   133,     0,     0,    12,
      19,     0,     0,    13,    14,    15,    16,    17,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,    20,    21,
      22,     0,     0,     0,     0,     0,   136,     0,     0,    23,
      24,    25,    26,     0,     0,   394,     0,   395,    27,    18,
       0,   397,     0,     0,    28,     0,    19,     0,     0,   137,
     177,   138,     0,     0,     0,   401,     6,     0,     0,    29,
       0,     0,    30,     0,    20,    21,    22,     5,     0,     0,
       0,     0,     5,    31,     0,    23,    24,    25,    26,    32,
       0,    33,     0,     0,    27,     0,     0,     0,    34,     0,
      28,     0,   186,     0,   309,     0,     0,     0,   187,   188,
     189,     0,     0,     0,   190,    29,     0,     0,    30,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   203,    31,
       0,     0,     0,     0,    46,    32,     0,    33,   191,   192,
       0,   133,     5,     0,    34,     0,     0,    47,    48,    49,
      50,    51,    52,    53,    54,    55,    56,     0,   204,   205,
     206,     0,   186,     0,     0,     0,     0,     0,   187,   188,
     189,   136,    57,     0,   190,     0,    58,    59,     0,     0,
       0,     0,     0,     0,     5,     0,     0,     5,   195,     0,
       0,     0,     0,     0,   137,   177,   138,     0,   191,   192,
       0,     0,    60,     0,   186,     0,     0,   186,    61,    62,
     187,   188,   189,   187,   188,   189,   190,   193,   194,   190,
       6,    63,    64,     0,     0,     0,     0,     0,    65,     0,
      66,     0,    67,    68,    69,    70,     0,     0,     0,     0,
     191,   192,    71,   191,   192,     0,     5,     0,   195,     0,
      72,     0,     0,    73,     0,     0,   197,    74,   286,     0,
     285,   286,   193,   194,     0,    75,   186,   201,     0,     0,
       0,     0,   187,   188,   189,     0,     0,     0,   190,   245,
     256,   264,   272,   276,   279,   283,   288,   293,     0,   297,
     247,   258,   266,   273,   277,   281,   284,   289,   294,     0,
     298,     0,   191,   192
};

static const yytype_int16 yycheck[] =
{
      28,    50,     4,     4,    32,     4,    36,     4,     4,    80,
       4,    80,    40,    80,    58,    59,     0,   357,     8,    90,
     109,    90,    93,    90,    93,     4,   115,    24,   143,   144,
      27,    28,    29,    30,    31,    32,    80,    25,    26,    36,
      68,    33,    34,    35,   141,    73,   141,    75,   141,     4,
     141,   100,    80,    81,    82,    83,    84,    85,    86,   399,
      88,     6,     7,    60,    61,    93,   143,   144,    96,   143,
     144,    99,     4,     4,   118,   119,    73,    74,    92,    93,
      94,    80,    79,    80,   112,   141,   147,   115,   116,   141,
     118,   145,    24,    79,    80,    27,    28,    29,    30,    31,
      32,   131,    98,     3,    36,    68,     3,    38,     3,   137,
     138,   112,   113,   110,   115,   116,     3,     3,   146,   147,
     141,   149,   150,     4,     4,   121,   141,   123,    60,    61,
     126,   127,   128,   129,   130,   148,   164,    68,    49,   144,
     142,    73,    74,   171,   172,   142,   142,   175,   142,   177,
       3,   146,    63,     4,    65,    66,     4,   185,    38,    70,
      71,     4,    73,    74,     6,   311,   121,    98,   123,   134,
     135,   136,   137,   138,   139,   203,    24,   205,   110,   312,
      81,    86,    30,    31,    32,    96,   222,   392,    36,   217,
     121,    48,   123,   221,   367,    85,   110,   108,   109,   110,
     111,   116,   113,   114,   232,   233,   234,   123,    -1,    -1,
     142,   122,    60,    61,   108,   109,   110,   128,    -1,   113,
      -1,    -1,   133,   103,   104,   105,   106,   107,   108,    -1,
     110,    -1,     0,     1,    -1,    -1,    -1,     5,    -1,    -1,
      -1,     9,    10,    11,    12,    13,    62,    63,    64,    65,
      66,    67,    -1,     4,    -1,     4,    -1,    -1,   286,   287,
      -1,    -1,   173,    -1,    -1,    -1,    -1,    -1,   296,    -1,
      -1,   299,    -1,   301,   302,    24,    -1,    45,    -1,   307,
      -1,    30,    31,    32,    52,   196,    37,    36,    -1,    -1,
      -1,    -1,    -1,   204,   142,    46,    47,    48,    49,    50,
      51,    -1,    70,    71,    72,    -1,    -1,   218,   219,    -1,
      -1,    60,    61,    81,    82,    83,    84,    -1,   367,    -1,
      -1,    -1,    90,    -1,    -1,    -1,    -1,    -1,    96,    -1,
      79,    80,    -1,   244,    -1,    -1,    -1,    -1,    -1,    -1,
      91,    -1,    -1,   111,   255,    -1,   114,    -1,     4,    -1,
      -1,   379,   263,    -1,    -1,    -1,    -1,   125,    -1,   270,
      -1,   110,     4,   131,   275,   133,    -1,   278,    24,    -1,
      -1,   282,   140,    -1,    30,    31,    32,    -1,   146,     1,
      36,   292,    -1,     5,    40,    41,    42,     9,    10,    11,
      12,    13,    -1,   142,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    60,    61,    -1,    -1,    -1,    -1,
      -1,    53,    54,    55,    56,    57,    58,    59,    -1,    -1,
      -1,    -1,    -1,    45,     0,     1,    68,    -1,    -1,     5,
      52,    -1,    -1,     9,    10,    11,    12,    13,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    70,    71,
      72,    -1,    -1,    -1,    -1,    -1,    98,    -1,    -1,    81,
      82,    83,    84,    -1,    -1,   376,    -1,   378,    90,    45,
      -1,   382,    -1,    -1,    96,    -1,    52,    -1,    -1,   121,
     122,   123,    -1,    -1,    -1,   396,   142,    -1,    -1,   111,
      -1,    -1,   114,    -1,    70,    71,    72,     4,    -1,    -1,
      -1,    -1,     4,   125,    -1,    81,    82,    83,    84,   131,
      -1,   133,    -1,    -1,    90,    -1,    -1,    -1,   140,    -1,
      96,    -1,    24,    -1,   146,    -1,    -1,    -1,    30,    31,
      32,    -1,    -1,    -1,    36,   111,    -1,    -1,   114,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    55,   125,
      -1,    -1,    -1,    -1,     1,   131,    -1,   133,    60,    61,
      -1,    68,     4,    -1,   140,    -1,    -1,    14,    15,    16,
      17,    18,    19,    20,    21,    22,    23,    -1,    85,    86,
      87,    -1,    24,    -1,    -1,    -1,    -1,    -1,    30,    31,
      32,    98,    39,    -1,    36,    -1,    43,    44,    -1,    -1,
      -1,    -1,    -1,    -1,     4,    -1,    -1,     4,   110,    -1,
      -1,    -1,    -1,    -1,   121,   122,   123,    -1,    60,    61,
      -1,    -1,    69,    -1,    24,    -1,    -1,    24,    75,    76,
      30,    31,    32,    30,    31,    32,    36,    79,    80,    36,
     142,    88,    89,    -1,    -1,    -1,    -1,    -1,    95,    -1,
      97,    -1,    99,   100,   101,   102,    -1,    -1,    -1,    -1,
      60,    61,   109,    60,    61,    -1,     4,    -1,   110,    -1,
     117,    -1,    -1,   120,    -1,    -1,    85,   124,    78,    -1,
      77,    78,    79,    80,    -1,   132,    24,    85,    -1,    -1,
      -1,    -1,    30,    31,    32,    -1,    -1,    -1,    36,   108,
     109,   110,   111,   112,   113,   114,   115,   116,    -1,   118,
     108,   109,   110,   111,   112,   113,   114,   115,   116,    -1,
     118,    -1,    60,    61
};

/* YYSTOS[STATE-NUM] -- The symbol kind of the accessing symbol of
   state STATE-NUM.  */
static const yytype_uint8 yystos[] =
{
       0,     8,   150,   151,   159,     4,   142,   155,   157,   158,
       0,     1,     5,     9,    10,    11,    12,    13,    45,    52,
      70,    71,    72,    81,    82,    83,    84,    90,    96,   111,
     114,   125,   131,   133,   140,   152,   153,   160,   166,   185,
     189,   151,   156,   157,   141,   141,     1,    14,    15,    16,
      17,    18,    19,    20,    21,    22,    23,    39,    43,    44,
      69,    75,    76,    88,    89,    95,    97,    99,   100,   101,
     102,   109,   117,   120,   124,   132,   161,   204,   141,   167,
     169,   174,   177,   183,   187,   188,   190,   158,   164,   141,
     192,   158,   200,   201,   152,   141,   186,   158,   143,   144,
     147,   202,   202,   155,   204,    25,    26,   207,   208,   218,
     222,   224,   223,   226,   227,   228,   230,   155,   231,   155,
     155,   205,   158,   206,   155,   155,   233,   155,   158,   155,
     158,   145,   163,    68,   168,    38,    98,   121,   123,   158,
     168,   170,   171,   172,   173,   103,   104,   105,   106,   107,
     108,   110,   158,   170,   175,   176,    37,    46,    47,    48,
      49,    50,    51,    91,   158,   178,   179,   180,   181,   182,
      53,    54,    55,    56,    57,    58,    59,   122,   158,   168,
     171,   172,   173,   184,    80,   158,    24,    30,    31,    32,
      36,    60,    61,    79,    80,   110,   158,   209,   210,   211,
     213,   214,   217,    55,    85,    86,    87,   158,   184,   141,
     112,   113,   115,   116,   158,   165,     4,   126,   127,   128,
     129,   130,   142,   171,   172,   173,   196,   198,   199,   134,
     135,   136,   137,   138,   139,   158,   172,   173,   155,   158,
     158,   203,   204,     3,   155,   209,   213,   214,   216,   217,
      27,    28,    29,    73,    74,   155,   209,   213,   214,   216,
     219,   220,   221,   155,   209,   213,   214,   219,    40,    41,
      42,   155,   209,   214,   225,   158,   209,   214,   155,   209,
     213,   214,   155,   209,   214,    77,    78,   158,   209,   214,
     216,   229,   158,   209,   214,   229,   158,   209,   214,   232,
     155,   232,    58,    59,    80,   118,   119,   234,   155,   146,
     152,     6,     7,   162,   155,   158,   158,     3,   158,   158,
     158,   158,   158,     3,   158,   158,   155,   158,     3,   158,
     158,    33,    34,    35,   215,     3,   154,   155,    62,    63,
      64,    65,    66,    67,   212,   158,   155,   158,    92,    93,
      94,   191,   141,   158,   155,   155,     4,   142,   193,   195,
     158,   197,   198,   158,   158,   158,   148,   144,   155,   155,
     155,   155,   155,   155,   155,   158,   158,   155,   158,   158,
     209,   214,   158,   158,     3,   158,   146,   161,   163,   194,
     195,   143,   144,   203,   155,   155,   158,   155,   143,   144,
     199,   155,   195
};

/* YYR1[RULE-NUM] -- Symbol kind of the left-hand side of rule RULE-NUM.  */
static const yytype_uint8 yyr1[] =
{
       0,   149,   150,   150,   151,   151,   152,   152,   153,   153,
     153,   154,   154,   155,   155,   156,   156,   157,   158,   159,
     160,   160,   160,   160,   160,   161,   162,   162,   162,   163,
     163,   164,   164,   164,   164,   165,   165,   166,   166,   166,
     166,   166,   166,   166,   166,   166,   166,   166,   166,   166,
     166,   166,   167,   167,   168,   169,   169,   169,   169,   169,
     169,   170,   171,   172,   173,   174,   174,   174,   174,   174,
     175,   175,   175,   175,   176,   176,   177,   177,   177,   177,
     177,   177,   178,   178,   179,   179,   180,   180,   181,   182,
     183,   183,   183,   183,   183,   183,   183,   183,   183,   184,
     184,   184,   184,   184,   185,   185,   185,   186,   186,   187,
     187,   188,   188,   188,   188,   188,   189,   189,   190,   190,
     190,   190,   190,   190,   191,   191,   191,   192,   192,   192,
     192,   192,   192,   192,   192,   192,   193,   193,   194,   194,
     195,   196,   196,   197,   197,   198,   199,   200,   200,   200,
     200,   200,   200,   200,   201,   201,   201,   202,   203,   203,
     204,   204,   204,   204,   204,   204,   204,   204,   204,   204,
     204,   204,   204,   204,   204,   204,   204,   204,   204,   204,
     204,   205,   204,   204,   206,   204,   204,   204,   204,   204,
     204,   204,   204,   204,   207,   207,   208,   208,   208,   208,
     208,   209,   209,   210,   210,   210,   210,   211,   211,   212,
     212,   212,   212,   212,   212,   213,   214,   215,   215,   215,
     216,   217,   217,   218,   218,   218,   218,   218,   218,   219,
     220,   220,   220,   220,   221,   221,   222,   222,   222,   222,
     222,   223,   223,   223,   224,   224,   224,   224,   224,   225,
     225,   226,   226,   226,   226,   227,   227,   227,   228,   228,
     228,   228,   228,   228,   229,   230,   230,   230,   230,   231,
     231,   231,   232,   232,   232,   233,   233,   233,   233,   233,
     234,   234
};

/* YYR2[RULE-NUM] -- Number of symbols on the right-hand side of rule RULE-NUM.  */
static const yytype_int8 yyr2[] =
{
       0,     2,     1,     2,     0,     2,     1,     2,     1,     2,
       2,     0,     1,     1,     3,     1,     3,     1,     1,     3,
       2,     2,     3,     4,     2,     3,     0,     2,     2,     3,
       2,     0,     2,     2,     2,     1,     1,     2,     3,     3,
       1,     4,     3,     3,     4,     4,     2,     3,     2,     3,
       2,     3,     0,     2,     2,     0,     2,     2,     2,     2,
       2,     1,     1,     2,     2,     0,     2,     2,     2,     2,
       2,     2,     2,     1,     2,     2,     0,     2,     2,     2,
       2,     2,     1,     1,     1,     1,     1,     1,     1,     1,
       0,     3,     3,     3,     3,     3,     2,     3,     2,     2,
       1,     1,     1,     1,     1,     1,     1,     0,     2,     0,
       2,     0,     2,     2,     2,     2,     1,     1,     0,     3,
       3,     3,     3,     2,     1,     1,     1,     0,     3,     2,
       2,     2,     3,     3,     3,     3,     1,     3,     1,     3,
       1,     1,     3,     1,     3,     1,     1,     0,     2,     2,
       2,     3,     3,     3,     0,     2,     2,     3,     1,     3,
       2,     2,     2,     1,     1,     2,     3,     4,     4,     4,
       3,     4,     4,     4,     3,     5,     4,     2,     5,     2,
       2,     0,     6,     3,     0,     5,     2,     2,     2,     2,
       3,     2,     2,     1,     1,     1,     0,     2,     2,     2,
       2,     1,     2,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     2,     1,     1,     1,
       1,     2,     1,     0,     2,     2,     2,     2,     2,     1,
       1,     1,     1,     1,     1,     1,     0,     2,     2,     2,
       2,     0,     2,     2,     0,     2,     3,     2,     2,     1,
       1,     0,     2,     2,     2,     0,     2,     2,     0,     2,
       2,     2,     2,     2,     2,     0,     2,     2,     2,     0,
       2,     2,     0,     2,     2,     0,     3,     3,     3,     2,
       1,     1
};


enum { YYENOMEM = -2 };

#define yyerrok         (yyerrstatus = 0)
#define yyclearin       (yychar = YYEMPTY)

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab
#define YYNOMEM         goto yyexhaustedlab


#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)                                    \
  do                                                              \
    if (yychar == YYEMPTY)                                        \
      {                                                           \
        yychar = (Token);                                         \
        yylval = (Value);                                         \
        YYPOPSTACK (yylen);                                       \
        yystate = *yyssp;                                         \
        goto yybackup;                                            \
      }                                                           \
    else                                                          \
      {                                                           \
        yyerror (sscript, YY_("syntax error: cannot back up")); \
        YYERROR;                                                  \
      }                                                           \
  while (0)

/* Backward compatibility with an undocumented macro.
   Use YYerror or YYUNDEF. */
#define YYERRCODE YYUNDEF


/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)                        \
do {                                            \
  if (yydebug)                                  \
    YYFPRINTF Args;                             \
} while (0)




# define YY_SYMBOL_PRINT(Title, Kind, Value, Location)                    \
do {                                                                      \
  if (yydebug)                                                            \
    {                                                                     \
      YYFPRINTF (stderr, "%s ", Title);                                   \
      yy_symbol_print (stderr,                                            \
                  Kind, Value, sscript); \
      YYFPRINTF (stderr, "\n");                                           \
    }                                                                     \
} while (0)


/*-----------------------------------.
| Print this symbol's value on YYO.  |
`-----------------------------------*/

static void
yy_symbol_value_print (FILE *yyo,
                       yysymbol_kind_t yykind, YYSTYPE const * const yyvaluep, sieve_script_t *sscript)
{
  FILE *yyoutput = yyo;
  YY_USE (yyoutput);
  YY_USE (sscript);
  if (!yyvaluep)
    return;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YY_USE (yykind);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}


/*---------------------------.
| Print this symbol on YYO.  |
`---------------------------*/

static void
yy_symbol_print (FILE *yyo,
                 yysymbol_kind_t yykind, YYSTYPE const * const yyvaluep, sieve_script_t *sscript)
{
  YYFPRINTF (yyo, "%s %s (",
             yykind < YYNTOKENS ? "token" : "nterm", yysymbol_name (yykind));

  yy_symbol_value_print (yyo, yykind, yyvaluep, sscript);
  YYFPRINTF (yyo, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

static void
yy_stack_print (yy_state_t *yybottom, yy_state_t *yytop)
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)                            \
do {                                                            \
  if (yydebug)                                                  \
    yy_stack_print ((Bottom), (Top));                           \
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

static void
yy_reduce_print (yy_state_t *yyssp, YYSTYPE *yyvsp,
                 int yyrule, sieve_script_t *sscript)
{
  int yylno = yyrline[yyrule];
  int yynrhs = yyr2[yyrule];
  int yyi;
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %d):\n",
             yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr,
                       YY_ACCESSING_SYMBOL (+yyssp[yyi + 1 - yynrhs]),
                       &yyvsp[(yyi + 1) - (yynrhs)], sscript);
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)          \
do {                                    \
  if (yydebug)                          \
    yy_reduce_print (yyssp, yyvsp, Rule, sscript); \
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args) ((void) 0)
# define YY_SYMBOL_PRINT(Title, Kind, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif






/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

static void
yydestruct (const char *yymsg,
            yysymbol_kind_t yykind, YYSTYPE *yyvaluep, sieve_script_t *sscript)
{
  YY_USE (yyvaluep);
  YY_USE (sscript);
  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yykind, yyvaluep, yylocationp);

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  switch (yykind)
    {
    case YYSYMBOL_STRING: /* STRING  */
#line 174 "sieve/sieve.y"
             { free(((*yyvaluep).sval));          }
#line 1940 "sieve/sieve.c"
        break;

    case YYSYMBOL_commands: /* commands  */
#line 170 "sieve/sieve.y"
             { free_tree(((*yyvaluep).cl));     }
#line 1946 "sieve/sieve.c"
        break;

    case YYSYMBOL_command: /* command  */
#line 170 "sieve/sieve.y"
             { free_tree(((*yyvaluep).cl));     }
#line 1952 "sieve/sieve.c"
        break;

    case YYSYMBOL_optstringlist: /* optstringlist  */
#line 173 "sieve/sieve.y"
             { strarray_free(((*yyvaluep).sl)); }
#line 1958 "sieve/sieve.c"
        break;

    case YYSYMBOL_stringlist: /* stringlist  */
#line 173 "sieve/sieve.y"
             { strarray_free(((*yyvaluep).sl)); }
#line 1964 "sieve/sieve.c"
        break;

    case YYSYMBOL_strings: /* strings  */
#line 173 "sieve/sieve.y"
             { strarray_free(((*yyvaluep).sl)); }
#line 1970 "sieve/sieve.c"
        break;

    case YYSYMBOL_string1: /* string1  */
#line 173 "sieve/sieve.y"
             { strarray_free(((*yyvaluep).sl)); }
#line 1976 "sieve/sieve.c"
        break;

    case YYSYMBOL_string: /* string  */
#line 174 "sieve/sieve.y"
             { free(((*yyvaluep).sval));          }
#line 1982 "sieve/sieve.c"
        break;

    case YYSYMBOL_control: /* control  */
#line 170 "sieve/sieve.y"
             { free_tree(((*yyvaluep).cl));     }
#line 1988 "sieve/sieve.c"
        break;

    case YYSYMBOL_thenelse: /* thenelse  */
#line 170 "sieve/sieve.y"
             { free_tree(((*yyvaluep).cl));     }
#line 1994 "sieve/sieve.c"
        break;

    case YYSYMBOL_elsif: /* elsif  */
#line 170 "sieve/sieve.y"
             { free_tree(((*yyvaluep).cl));     }
#line 2000 "sieve/sieve.c"
        break;

    case YYSYMBOL_block: /* block  */
#line 170 "sieve/sieve.y"
             { free_tree(((*yyvaluep).cl));     }
#line 2006 "sieve/sieve.c"
        break;

    case YYSYMBOL_action: /* action  */
#line 170 "sieve/sieve.y"
             { free_tree(((*yyvaluep).cl));     }
#line 2012 "sieve/sieve.c"
        break;

    case YYSYMBOL_ftags: /* ftags  */
#line 170 "sieve/sieve.y"
             { free_tree(((*yyvaluep).cl));     }
#line 2018 "sieve/sieve.c"
        break;

    case YYSYMBOL_timelist: /* timelist  */
#line 175 "sieve/sieve.y"
             { arrayu64_free(((*yyvaluep).nl)); }
#line 2024 "sieve/sieve.c"
        break;

    case YYSYMBOL_times: /* times  */
#line 175 "sieve/sieve.y"
             { arrayu64_free(((*yyvaluep).nl)); }
#line 2030 "sieve/sieve.c"
        break;

    case YYSYMBOL_time1: /* time1  */
#line 175 "sieve/sieve.y"
             { arrayu64_free(((*yyvaluep).nl)); }
#line 2036 "sieve/sieve.c"
        break;

    case YYSYMBOL_ikttags: /* ikttags  */
#line 170 "sieve/sieve.y"
             { free_tree(((*yyvaluep).cl));     }
#line 2042 "sieve/sieve.c"
        break;

    case YYSYMBOL_testlist: /* testlist  */
#line 171 "sieve/sieve.y"
             { free_testlist(((*yyvaluep).testl)); }
#line 2048 "sieve/sieve.c"
        break;

    case YYSYMBOL_tests: /* tests  */
#line 171 "sieve/sieve.y"
             { free_testlist(((*yyvaluep).testl)); }
#line 2054 "sieve/sieve.c"
        break;

    case YYSYMBOL_test: /* test  */
#line 172 "sieve/sieve.y"
             { free_test(((*yyvaluep).test));     }
#line 2060 "sieve/sieve.c"
        break;

      default:
        break;
    }
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}






/*----------.
| yyparse.  |
`----------*/

int
yyparse (sieve_script_t *sscript)
{
/* Lookahead token kind.  */
int yychar;


/* The semantic value of the lookahead symbol.  */
/* Default value used for initialization, for pacifying older GCCs
   or non-GCC compilers.  */
YY_INITIAL_VALUE (static YYSTYPE yyval_default;)
YYSTYPE yylval YY_INITIAL_VALUE (= yyval_default);

    /* Number of syntax errors so far.  */
    int yynerrs = 0;

    yy_state_fast_t yystate = 0;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus = 0;

    /* Refer to the stacks through separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* Their size.  */
    YYPTRDIFF_T yystacksize = YYINITDEPTH;

    /* The state stack: array, bottom, top.  */
    yy_state_t yyssa[YYINITDEPTH];
    yy_state_t *yyss = yyssa;
    yy_state_t *yyssp = yyss;

    /* The semantic value stack: array, bottom, top.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs = yyvsa;
    YYSTYPE *yyvsp = yyvs;

  int yyn;
  /* The return value of yyparse.  */
  int yyresult;
  /* Lookahead symbol kind.  */
  yysymbol_kind_t yytoken = YYSYMBOL_YYEMPTY;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;



#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yychar = YYEMPTY; /* Cause a token to be read.  */

  goto yysetstate;


/*------------------------------------------------------------.
| yynewstate -- push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;


/*--------------------------------------------------------------------.
| yysetstate -- set current state (the top of the stack) to yystate.  |
`--------------------------------------------------------------------*/
yysetstate:
  YYDPRINTF ((stderr, "Entering state %d\n", yystate));
  YY_ASSERT (0 <= yystate && yystate < YYNSTATES);
  YY_IGNORE_USELESS_CAST_BEGIN
  *yyssp = YY_CAST (yy_state_t, yystate);
  YY_IGNORE_USELESS_CAST_END
  YY_STACK_PRINT (yyss, yyssp);

  if (yyss + yystacksize - 1 <= yyssp)
#if !defined yyoverflow && !defined YYSTACK_RELOCATE
    YYNOMEM;
#else
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYPTRDIFF_T yysize = yyssp - yyss + 1;

# if defined yyoverflow
      {
        /* Give user a chance to reallocate the stack.  Use copies of
           these so that the &'s don't force the real ones into
           memory.  */
        yy_state_t *yyss1 = yyss;
        YYSTYPE *yyvs1 = yyvs;

        /* Each stack pointer address is followed by the size of the
           data in use in that stack, in bytes.  This used to be a
           conditional around just the two extra args, but that might
           be undefined if yyoverflow is a macro.  */
        yyoverflow (YY_("memory exhausted"),
                    &yyss1, yysize * YYSIZEOF (*yyssp),
                    &yyvs1, yysize * YYSIZEOF (*yyvsp),
                    &yystacksize);
        yyss = yyss1;
        yyvs = yyvs1;
      }
# else /* defined YYSTACK_RELOCATE */
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
        YYNOMEM;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
        yystacksize = YYMAXDEPTH;

      {
        yy_state_t *yyss1 = yyss;
        union yyalloc *yyptr =
          YY_CAST (union yyalloc *,
                   YYSTACK_ALLOC (YY_CAST (YYSIZE_T, YYSTACK_BYTES (yystacksize))));
        if (! yyptr)
          YYNOMEM;
        YYSTACK_RELOCATE (yyss_alloc, yyss);
        YYSTACK_RELOCATE (yyvs_alloc, yyvs);
#  undef YYSTACK_RELOCATE
        if (yyss1 != yyssa)
          YYSTACK_FREE (yyss1);
      }
# endif

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;

      YY_IGNORE_USELESS_CAST_BEGIN
      YYDPRINTF ((stderr, "Stack size increased to %ld\n",
                  YY_CAST (long, yystacksize)));
      YY_IGNORE_USELESS_CAST_END

      if (yyss + yystacksize - 1 <= yyssp)
        YYABORT;
    }
#endif /* !defined yyoverflow && !defined YYSTACK_RELOCATE */


  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;


/*-----------.
| yybackup.  |
`-----------*/
yybackup:
  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either empty, or end-of-input, or a valid lookahead.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token\n"));
      yychar = yylex (&yylval, sscript);
    }

  if (yychar <= YYEOF)
    {
      yychar = YYEOF;
      yytoken = YYSYMBOL_YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else if (yychar == YYerror)
    {
      /* The scanner already issued an error message, process directly
         to error recovery.  But do not keep the error token as
         lookahead, it is too special and may lead us to an endless
         loop in error recovery. */
      yychar = YYUNDEF;
      yytoken = YYSYMBOL_YYerror;
      goto yyerrlab1;
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);
  yystate = yyn;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END

  /* Discard the shifted token.  */
  yychar = YYEMPTY;
  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     '$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
  case 2: /* start: reqs  */
#line 364 "sieve/sieve.y"
                                 { sscript->cmds = NULL; }
#line 2336 "sieve/sieve.c"
    break;

  case 3: /* start: reqs commands  */
#line 365 "sieve/sieve.y"
                                 { sscript->cmds = (yyvsp[0].cl); }
#line 2342 "sieve/sieve.c"
    break;

  case 7: /* commands: command commands  */
#line 376 "sieve/sieve.y"
                                 { (yyval.cl) = (yyvsp[-1].cl); (yyval.cl)->next = (yyvsp[0].cl); }
#line 2348 "sieve/sieve.c"
    break;

  case 10: /* command: error ';'  */
#line 382 "sieve/sieve.y"
                                 {
                                     struct buf buf = BUF_INITIALIZER;
                                     buf_printf(&buf, "%s: line %d",
                                                error_message(SIEVE_UNSUPP_EXT),
                                                sievelineno);
                                     sscript->support |= SIEVE_CAPA_IHAVE;
                                     (yyval.cl) = build_rej_err(sscript, B_ERROR,
                                                        buf_release(&buf));
                                 }
#line 2362 "sieve/sieve.c"
    break;

  case 11: /* optstringlist: %empty  */
#line 394 "sieve/sieve.y"
                                 { (yyval.sl) = strarray_new(); }
#line 2368 "sieve/sieve.c"
    break;

  case 14: /* stringlist: '[' strings ']'  */
#line 400 "sieve/sieve.y"
                                 { (yyval.sl) = (yyvsp[-1].sl); }
#line 2374 "sieve/sieve.c"
    break;

  case 16: /* strings: strings ',' string  */
#line 405 "sieve/sieve.y"
                                 { (yyval.sl) = (yyvsp[-2].sl); strarray_appendm((yyval.sl), (yyvsp[0].sval)); }
#line 2380 "sieve/sieve.c"
    break;

  case 17: /* string1: string  */
#line 409 "sieve/sieve.y"
                                 {
                                     (yyval.sl) = strarray_new();
                                     strarray_appendm((yyval.sl), (yyvsp[0].sval));
                                 }
#line 2389 "sieve/sieve.c"
    break;

  case 18: /* string: STRING  */
#line 416 "sieve/sieve.y"
                                 { (yyval.sval) = (yyvsp[0].sval); chk_match_vars(sscript, (yyval.sval)); }
#line 2395 "sieve/sieve.c"
    break;

  case 19: /* require: REQUIRE stringlist ';'  */
#line 423 "sieve/sieve.y"
                                 { check_reqs(sscript, (yyvsp[-1].sl)); }
#line 2401 "sieve/sieve.c"
    break;

  case 20: /* control: IF thenelse  */
#line 427 "sieve/sieve.y"
                                 { (yyval.cl) = (yyvsp[0].cl); }
#line 2407 "sieve/sieve.c"
    break;

  case 21: /* control: STOP ';'  */
#line 428 "sieve/sieve.y"
                                 { (yyval.cl) = new_command(B_STOP, sscript); }
#line 2413 "sieve/sieve.c"
    break;

  case 22: /* control: ERROR string ';'  */
#line 429 "sieve/sieve.y"
                                 { (yyval.cl) = build_rej_err(sscript, B_ERROR, (yyvsp[-1].sval)); }
#line 2419 "sieve/sieve.c"
    break;

  case 23: /* control: INCLUDE itags string ';'  */
#line 431 "sieve/sieve.y"
                                 { (yyval.cl) = build_include(sscript, (yyvsp[-2].cl), (yyvsp[-1].sval)); }
#line 2425 "sieve/sieve.c"
    break;

  case 24: /* control: RETURN ';'  */
#line 432 "sieve/sieve.y"
                                 { (yyval.cl) = new_command(B_RETURN, sscript); }
#line 2431 "sieve/sieve.c"
    break;

  case 25: /* thenelse: test block elsif  */
#line 436 "sieve/sieve.y"
                                 { 
                                     if ((yyvsp[-2].test)->ignore_err) {
                                         /* end of block - decrement counter */
                                         sscript->ignore_err--;
                                     }

                                     (yyval.cl) = new_if((yyvsp[-2].test), (yyvsp[-1].cl), (yyvsp[0].cl));
                                 }
#line 2444 "sieve/sieve.c"
    break;

  case 26: /* elsif: %empty  */
#line 447 "sieve/sieve.y"
                                 { (yyval.cl) = NULL; }
#line 2450 "sieve/sieve.c"
    break;

  case 27: /* elsif: ELSIF thenelse  */
#line 448 "sieve/sieve.y"
                                 { (yyval.cl) = (yyvsp[0].cl); }
#line 2456 "sieve/sieve.c"
    break;

  case 28: /* elsif: ELSE block  */
#line 449 "sieve/sieve.y"
                                 { (yyval.cl) = (yyvsp[0].cl); }
#line 2462 "sieve/sieve.c"
    break;

  case 29: /* block: '{' commands '}'  */
#line 453 "sieve/sieve.y"
                                 { (yyval.cl) = (yyvsp[-1].cl); }
#line 2468 "sieve/sieve.c"
    break;

  case 30: /* block: '{' '}'  */
#line 454 "sieve/sieve.y"
                                 { (yyval.cl) = NULL; }
#line 2474 "sieve/sieve.c"
    break;

  case 31: /* itags: %empty  */
#line 459 "sieve/sieve.y"
                                 { (yyval.cl) = new_command(B_INCLUDE, sscript); }
#line 2480 "sieve/sieve.c"
    break;

  case 32: /* itags: itags location  */
#line 460 "sieve/sieve.y"
                                 {
                                     if ((yyval.cl)->u.inc.location != -1) {
                                         sieveerror_c(sscript,
                                                      SIEVE_CONFLICTING_TAGS,
                                                      ":personal", ":location");
                                     }

                                     (yyval.cl)->u.inc.location = (yyvsp[0].nval);
                                 }
#line 2494 "sieve/sieve.c"
    break;

  case 33: /* itags: itags ONCE  */
#line 469 "sieve/sieve.y"
                                 {
                                     if ((yyval.cl)->u.inc.once != -1) {
                                         sieveerror_c(sscript,
                                                      SIEVE_DUPLICATE_TAG,
                                                      ":once");
                                     }

                                     (yyval.cl)->u.inc.once = INC_ONCE_MASK;
                                 }
#line 2508 "sieve/sieve.c"
    break;

  case 34: /* itags: itags OPTIONAL  */
#line 478 "sieve/sieve.y"
                                 {
                                     if ((yyval.cl)->u.inc.optional != -1) {
                                         sieveerror_c(sscript,
                                                      SIEVE_DUPLICATE_TAG,
                                                      ":optional");
                                     }

                                     (yyval.cl)->u.inc.optional = INC_OPTIONAL_MASK;
                                 }
#line 2522 "sieve/sieve.c"
    break;

  case 35: /* location: PERSONAL  */
#line 491 "sieve/sieve.y"
                                 { (yyval.nval) = B_PERSONAL; }
#line 2528 "sieve/sieve.c"
    break;

  case 36: /* location: GLOBAL  */
#line 492 "sieve/sieve.y"
                                 { (yyval.nval) = B_GLOBAL;   }
#line 2534 "sieve/sieve.c"
    break;

  case 37: /* action: KEEP ktags  */
#line 499 "sieve/sieve.y"
                                 { (yyval.cl) = build_keep(sscript, (yyvsp[0].cl)); }
#line 2540 "sieve/sieve.c"
    break;

  case 38: /* action: FILEINTO ftags string  */
#line 500 "sieve/sieve.y"
                                 { (yyval.cl) = build_fileinto(sscript, (yyvsp[-1].cl), (yyvsp[0].sval)); }
#line 2546 "sieve/sieve.c"
    break;

  case 39: /* action: REDIRECT rtags string  */
#line 501 "sieve/sieve.y"
                                 { (yyval.cl) = build_redirect(sscript, (yyvsp[-1].cl), (yyvsp[0].sval)); }
#line 2552 "sieve/sieve.c"
    break;

  case 40: /* action: DISCARD  */
#line 502 "sieve/sieve.y"
                                 { (yyval.cl) = new_command(B_DISCARD, sscript); }
#line 2558 "sieve/sieve.c"
    break;

  case 41: /* action: SET stags string string  */
#line 504 "sieve/sieve.y"
                                 { (yyval.cl) = build_set(sscript, (yyvsp[-2].cl), (yyvsp[-1].sval), (yyvsp[0].sval)); }
#line 2564 "sieve/sieve.c"
    break;

  case 42: /* action: VACATION vtags string  */
#line 505 "sieve/sieve.y"
                                 { (yyval.cl) = build_vacation(sscript, (yyvsp[-1].cl), (yyvsp[0].sval)); }
#line 2570 "sieve/sieve.c"
    break;

  case 43: /* action: flagaction flagtags stringlist  */
#line 509 "sieve/sieve.y"
                                 { (yyval.cl) = build_flag(sscript, (yyvsp[-1].cl), (yyvsp[0].sl)); }
#line 2576 "sieve/sieve.c"
    break;

  case 44: /* action: ADDHEADER ahtags string string  */
#line 512 "sieve/sieve.y"
                                 { (yyval.cl) = build_addheader(sscript,
                                                        (yyvsp[-2].cl), (yyvsp[-1].sval), (yyvsp[0].sval)); }
#line 2583 "sieve/sieve.c"
    break;

  case 45: /* action: DELETEHEADER dhtags string optstringlist  */
#line 515 "sieve/sieve.y"
                                 { (yyval.cl) = build_deleteheader(sscript,
                                                           (yyvsp[-2].cl), (yyvsp[-1].sval), (yyvsp[0].sl)); }
#line 2590 "sieve/sieve.c"
    break;

  case 46: /* action: reject string  */
#line 518 "sieve/sieve.y"
                                 { (yyval.cl) = build_rej_err(sscript, (yyvsp[-1].nval), (yyvsp[0].sval)); }
#line 2596 "sieve/sieve.c"
    break;

  case 47: /* action: NOTIFY ntags string  */
#line 519 "sieve/sieve.y"
                                 { (yyval.cl) = build_notify(sscript, (yyvsp[-1].cl), (yyvsp[0].sval)); }
#line 2602 "sieve/sieve.c"
    break;

  case 48: /* action: LOG string  */
#line 520 "sieve/sieve.y"
                                 { (yyval.cl) = build_log(sscript, (yyvsp[0].sval)); }
#line 2608 "sieve/sieve.c"
    break;

  case 49: /* action: SNOOZE sntags timelist  */
#line 521 "sieve/sieve.y"
                                 { (yyval.cl) = build_snooze(sscript, (yyvsp[-1].cl), (yyvsp[0].nl)); }
#line 2614 "sieve/sieve.c"
    break;

  case 50: /* action: PROCESSIMIP imiptags  */
#line 522 "sieve/sieve.y"
                                 { (yyval.cl) = build_imip(sscript, (yyvsp[0].cl)); }
#line 2620 "sieve/sieve.c"
    break;

  case 51: /* action: IKEEP_TARGET ikttags string  */
#line 525 "sieve/sieve.y"
                                 { (yyval.cl) = build_ikeep_target(sscript, (yyvsp[-1].cl), (yyvsp[0].sval)); }
#line 2626 "sieve/sieve.c"
    break;

  case 52: /* ktags: %empty  */
#line 530 "sieve/sieve.y"
                                 {
                                     (yyval.cl) = new_command(B_KEEP, sscript);
                                     flags = &((yyval.cl)->u.k.flags);
                                 }
#line 2635 "sieve/sieve.c"
    break;

  case 54: /* flags: FLAGS stringlist  */
#line 539 "sieve/sieve.y"
                                 {
                                     /* **flags assigned by the calling rule */
                                     if (*flags != NULL) {
                                         sieveerror_c(sscript,
                                                      SIEVE_DUPLICATE_TAG,
                                                      ":flags");
                                         strarray_free(*flags);
                                     }
                                     else if (!supported(SIEVE_CAPA_IMAP4FLAGS)) {
                                         sieveerror_c(sscript,
                                                      SIEVE_MISSING_REQUIRE,
                                                      "imap4flags");
                                     }

                                     *flags = (yyvsp[0].sl);
                                 }
#line 2656 "sieve/sieve.c"
    break;

  case 55: /* ftags: %empty  */
#line 559 "sieve/sieve.y"
                                 {
                                     (yyval.cl) = new_command(B_FILEINTO, sscript);
                                     copy = &((yyval.cl)->u.f.copy);
                                     flags = &((yyval.cl)->u.f.flags);
                                     create = &((yyval.cl)->u.f.create);
                                     specialuse = &((yyval.cl)->u.f.t.specialuse);
                                     mailboxid = &((yyval.cl)->u.f.t.mailboxid);
                                 }
#line 2669 "sieve/sieve.c"
    break;

  case 61: /* copy: COPY  */
#line 576 "sieve/sieve.y"
                                 {
                                     /* *copy assigned by the calling rule */
                                     if ((*copy)++) {
                                         sieveerror_c(sscript,
                                                      SIEVE_DUPLICATE_TAG,
                                                      ":copy");
                                     }
                                     else if (!supported(SIEVE_CAPA_COPY)) {
                                         sieveerror_c(sscript,
                                                      SIEVE_MISSING_REQUIRE,
                                                      "copy");
                                     }
                                 }
#line 2687 "sieve/sieve.c"
    break;

  case 62: /* create: CREATE  */
#line 593 "sieve/sieve.y"
                                {
                                     /* *create assigned by the calling rule */
                                     if ((*create)++) {
                                         sieveerror_c(sscript,
                                                      SIEVE_DUPLICATE_TAG,
                                                      ":create");
                                     }
                                     else if (!supported(SIEVE_CAPA_MAILBOX)) {
                                         sieveerror_c(sscript,
                                                      SIEVE_MISSING_REQUIRE,
                                                      "mailbox");
                                     }
                                 }
#line 2705 "sieve/sieve.c"
    break;

  case 63: /* specialuse: SPECIALUSE string  */
#line 610 "sieve/sieve.y"
                                 {
                                     /* **specialuse assigned by calling rule */
                                     if (*specialuse != NULL) {
                                         sieveerror_c(sscript,
                                                      SIEVE_DUPLICATE_TAG,
                                                      ":specialuse");
                                         free(*specialuse);
                                     }
                                     else if (!supported(SIEVE_CAPA_SPECIAL_USE)) {
                                         sieveerror_c(sscript,
                                                      SIEVE_MISSING_REQUIRE,
                                                      "special-use");
                                     }
                                     else if (*mailboxid != NULL) {
                                         sieveerror_c(sscript,
                                                      SIEVE_CONFLICTING_TAGS,
                                                      ":specialuse",
                                                      ":mailboxid");
                                     }

                                     *specialuse = (yyvsp[0].sval);
                                 }
#line 2732 "sieve/sieve.c"
    break;

  case 64: /* mailboxid: MAILBOXID string  */
#line 635 "sieve/sieve.y"
                                 {
                                     /* **mailboxid assigned by calling rule */
                                     if (*mailboxid != NULL) {
                                         sieveerror_c(sscript,
                                                      SIEVE_DUPLICATE_TAG,
                                                      ":mailboxid");
                                         free(*mailboxid);
                                     }
                                     else if (!supported(SIEVE_CAPA_MAILBOXID)) {
                                         sieveerror_c(sscript,
                                                      SIEVE_MISSING_REQUIRE,
                                                      "mailboxid");
                                     }
                                     else if (*specialuse != NULL) {
                                         sieveerror_c(sscript,
                                                      SIEVE_CONFLICTING_TAGS,
                                                      ":mailboxid",
                                                      ":specialuse");
                                     }

                                     *mailboxid = (yyvsp[0].sval);
                                 }
#line 2759 "sieve/sieve.c"
    break;

  case 65: /* rtags: %empty  */
#line 661 "sieve/sieve.y"
                                 {
                                     (yyval.cl) = new_command(B_REDIRECT, sscript);
                                     copy = &((yyval.cl)->u.r.copy);
                                 }
#line 2768 "sieve/sieve.c"
    break;

  case 67: /* rtags: rtags LIST  */
#line 666 "sieve/sieve.y"
                                 {
                                     if ((yyval.cl)->u.r.list++) {
                                         sieveerror_c(sscript,
                                                      SIEVE_DUPLICATE_TAG,
                                                      ":list");
                                     }
                                     else if (!supported(SIEVE_CAPA_EXTLISTS)) {
                                         sieveerror_c(sscript,
                                                      SIEVE_MISSING_REQUIRE,
                                                      "extlists");
                                     }
                                 }
#line 2785 "sieve/sieve.c"
    break;

  case 68: /* rtags: rtags delbytags  */
#line 678 "sieve/sieve.y"
                                 {
                                     if (!supported(SIEVE_CAPA_REDIR_DELBY)) {
                                         sieveerror_c(sscript,
                                                      SIEVE_MISSING_REQUIRE,
                                                      "redirect-deliverby");
                                     }
                                 }
#line 2797 "sieve/sieve.c"
    break;

  case 69: /* rtags: rtags dsntags  */
#line 685 "sieve/sieve.y"
                                 {
                                     if (!supported(SIEVE_CAPA_REDIR_DSN)) {
                                         sieveerror_c(sscript,
                                                      SIEVE_MISSING_REQUIRE,
                                                      "redirect-dsn");
                                     }
                                 }
#line 2809 "sieve/sieve.c"
    break;

  case 70: /* delbytags: BYTIMEREL NUMBER  */
#line 695 "sieve/sieve.y"
                                 {
                                     /* $0 refers to rtags */
                                     commandlist_t *c = (yyvsp[-2].cl);

                                     if (c->u.r.bytime != NULL) {
                                         if (isdigit(c->u.r.bytime[0])) {
                                             sieveerror_c(sscript,
                                                          SIEVE_CONFLICTING_TAGS,
                                                          ":bytimerelative",
                                                          ":bytimeabsolute");
                                         }
                                         else {
                                             sieveerror_c(sscript,
                                                          SIEVE_DUPLICATE_TAG,
                                                          ":bytimerelative");
                                         }
                                     }                                         

                                     struct buf buf = BUF_INITIALIZER;
                                     buf_printf(&buf, "+%d", (yyvsp[0].nval));
                                     c->u.r.bytime = buf_release(&buf);
                                 }
#line 2836 "sieve/sieve.c"
    break;

  case 71: /* delbytags: BYTIMEABS string  */
#line 717 "sieve/sieve.y"
                                 {
                                     /* $0 refers to rtags */
                                     commandlist_t *c = (yyvsp[-2].cl);

                                     if (c->u.r.bytime != NULL) {
                                         if (isdigit(c->u.r.bytime[0])) {
                                             sieveerror_c(sscript,
                                                          SIEVE_DUPLICATE_TAG,
                                                          ":bytimeabsolute");
                                         }
                                         else {
                                             sieveerror_c(sscript,
                                                          SIEVE_CONFLICTING_TAGS,
                                                          ":bytimeabsolute",
                                                          ":bytimerelative");
                                         }
                                     }

                                     c->u.r.bytime = (yyvsp[0].sval);
                                 }
#line 2861 "sieve/sieve.c"
    break;

  case 72: /* delbytags: BYMODE string  */
#line 737 "sieve/sieve.y"
                                 {
                                     /* $0 refers to rtags */
                                     commandlist_t *c = (yyvsp[-2].cl);

                                     if (c->u.r.bymode != NULL) {
                                         sieveerror_c(sscript,
                                                      SIEVE_DUPLICATE_TAG,
                                                      ":bymode");
                                     }

                                     c->u.r.bymode = (yyvsp[0].sval);
                                 }
#line 2878 "sieve/sieve.c"
    break;

  case 73: /* delbytags: BYTRACE  */
#line 749 "sieve/sieve.y"
                                 {
                                     /* $0 refers to rtags */
                                     commandlist_t *c = (yyvsp[-1].cl);

                                     if (c->u.r.bytrace != 0) {
                                         sieveerror_c(sscript,
                                                      SIEVE_DUPLICATE_TAG,
                                                      ":bytrace");
                                     }

                                     c->u.r.bytrace = 1;
                                 }
#line 2895 "sieve/sieve.c"
    break;

  case 74: /* dsntags: DSNNOTIFY string  */
#line 765 "sieve/sieve.y"
                                 {
                                     /* $0 refers to rtags */
                                     commandlist_t *c = (yyvsp[-2].cl);

                                     if (c->u.r.dsn_notify != NULL) {
                                         sieveerror_c(sscript,
                                                      SIEVE_DUPLICATE_TAG,
                                                      ":notify");
                                     }                                         

                                     c->u.r.dsn_notify = (yyvsp[0].sval);
                                 }
#line 2912 "sieve/sieve.c"
    break;

  case 75: /* dsntags: DSNRET string  */
#line 777 "sieve/sieve.y"
                                 {
                                     /* $0 refers to rtags */
                                     commandlist_t *c = (yyvsp[-2].cl);

                                     if (c->u.r.dsn_ret != NULL) {
                                         sieveerror_c(sscript,
                                                      SIEVE_DUPLICATE_TAG,
                                                      ":ret");
                                     }

                                     c->u.r.dsn_ret = (yyvsp[0].sval);
                                 }
#line 2929 "sieve/sieve.c"
    break;

  case 76: /* stags: %empty  */
#line 793 "sieve/sieve.y"
                                 { (yyval.cl) = new_command(B_SET, sscript); }
#line 2935 "sieve/sieve.c"
    break;

  case 77: /* stags: stags mod40  */
#line 794 "sieve/sieve.y"
                                 {
                                     if ((yyval.cl)->u.s.modifiers & BFV_MOD40_MASK) {
                                         sieveerror_c(sscript,
                                                      SIEVE_MULTIPLE_TAGS,
                                                      "precedence 40 modifier");
                                     }

                                     (yyval.cl)->u.s.modifiers |= (yyvsp[0].nval);
                                 }
#line 2949 "sieve/sieve.c"
    break;

  case 78: /* stags: stags mod30  */
#line 803 "sieve/sieve.y"
                                 {
                                     if ((yyval.cl)->u.s.modifiers & BFV_MOD30_MASK) {
                                         sieveerror_c(sscript,
                                                      SIEVE_MULTIPLE_TAGS,
                                                      "precedence 30 modifier");
                                     }

                                     (yyval.cl)->u.s.modifiers |= (yyvsp[0].nval);
                                 }
#line 2963 "sieve/sieve.c"
    break;

  case 79: /* stags: stags mod20  */
#line 812 "sieve/sieve.y"
                                 {
                                     if ((yyval.cl)->u.s.modifiers & BFV_MOD20_MASK) {
                                         sieveerror_c(sscript,
                                                      SIEVE_MULTIPLE_TAGS,
                                                      "precedence 20 modifier");
                                     }

                                     (yyval.cl)->u.s.modifiers |= (yyvsp[0].nval);
                                 }
#line 2977 "sieve/sieve.c"
    break;

  case 80: /* stags: stags mod15  */
#line 821 "sieve/sieve.y"
                                 {
                                     if ((yyval.cl)->u.s.modifiers & BFV_MOD15_MASK) {
                                         sieveerror_c(sscript,
                                                      SIEVE_MULTIPLE_TAGS,
                                                      "precedence 15 modifier");
                                     }

                                     (yyval.cl)->u.s.modifiers |= (yyvsp[0].nval);
                                 }
#line 2991 "sieve/sieve.c"
    break;

  case 81: /* stags: stags mod10  */
#line 830 "sieve/sieve.y"
                                 {
                                     if ((yyval.cl)->u.s.modifiers & BFV_MOD10_MASK) {
                                         sieveerror_c(sscript,
                                                      SIEVE_MULTIPLE_TAGS,
                                                      "precedence 10 modifier");
                                     }

                                     (yyval.cl)->u.s.modifiers |= (yyvsp[0].nval);
                                 }
#line 3005 "sieve/sieve.c"
    break;

  case 82: /* mod40: LOWER  */
#line 843 "sieve/sieve.y"
                                { (yyval.nval) = BFV_LOWER; }
#line 3011 "sieve/sieve.c"
    break;

  case 83: /* mod40: UPPER  */
#line 844 "sieve/sieve.y"
                                { (yyval.nval) = BFV_UPPER; }
#line 3017 "sieve/sieve.c"
    break;

  case 84: /* mod30: LOWERFIRST  */
#line 847 "sieve/sieve.y"
                                { (yyval.nval) = BFV_LOWERFIRST; }
#line 3023 "sieve/sieve.c"
    break;

  case 85: /* mod30: UPPERFIRST  */
#line 848 "sieve/sieve.y"
                                { (yyval.nval) = BFV_UPPERFIRST; }
#line 3029 "sieve/sieve.c"
    break;

  case 86: /* mod20: QUOTEWILDCARD  */
#line 851 "sieve/sieve.y"
                                { (yyval.nval) = BFV_QUOTEWILDCARD; }
#line 3035 "sieve/sieve.c"
    break;

  case 87: /* mod20: QUOTEREGEX  */
#line 852 "sieve/sieve.y"
                                { 
                                     if (!supported(SIEVE_CAPA_REGEX)) {
                                         sieveerror_c(sscript,
                                                      SIEVE_MISSING_REQUIRE,
                                                      "regex");
                                     }

                                     (yyval.nval) = BFV_QUOTEREGEX;
                                 }
#line 3049 "sieve/sieve.c"
    break;

  case 88: /* mod15: ENCODEURL  */
#line 864 "sieve/sieve.y"
                                 { 
                                     if (!supported(SIEVE_CAPA_ENOTIFY)) {
                                         sieveerror_c(sscript,
                                                      SIEVE_MISSING_REQUIRE,
                                                      "enotify");
                                     }

                                     (yyval.nval) = BFV_ENCODEURL;
                                 }
#line 3063 "sieve/sieve.c"
    break;

  case 89: /* mod10: LENGTH  */
#line 875 "sieve/sieve.y"
                                 { (yyval.nval) = BFV_LENGTH; }
#line 3069 "sieve/sieve.c"
    break;

  case 90: /* vtags: %empty  */
#line 880 "sieve/sieve.y"
                                 {
                                     (yyval.cl) = new_command(B_VACATION, sscript);
                                     flags = &((yyval.cl)->u.v.fcc.flags);
                                     create = &((yyval.cl)->u.v.fcc.create);
                                     specialuse = &((yyval.cl)->u.v.fcc.t.specialuse);
                                     mailboxid = &((yyval.cl)->u.v.fcc.t.mailboxid);
                                     fccfolder = &((yyval.cl)->u.v.fcc.t.folder);
                                 }
#line 3082 "sieve/sieve.c"
    break;

  case 91: /* vtags: vtags DAYS NUMBER  */
#line 888 "sieve/sieve.y"
                                 {
                                     if ((yyval.cl)->u.v.seconds != -1) {
                                         sieveerror_c(sscript,
                                                      SIEVE_DUPLICATE_TAG,
                                                      ":days");
                                     }

                                     (yyval.cl)->u.v.seconds = (yyvsp[0].nval) * DAY2SEC;
                                 }
#line 3096 "sieve/sieve.c"
    break;

  case 92: /* vtags: vtags SECONDS NUMBER  */
#line 897 "sieve/sieve.y"
                                 {
                                     if (!supported(SIEVE_CAPA_VACATION_SEC)) {
                                         sieveerror_c(sscript,
                                                      SIEVE_MISSING_REQUIRE,
                                                      "vacation-seconds");
                                     }
                                     if ((yyval.cl)->u.v.seconds != -1) {
                                         sieveerror_c(sscript,
                                                      SIEVE_DUPLICATE_TAG,
                                                      ":seconds");
                                     }

                                     (yyval.cl)->u.v.seconds = (yyvsp[0].nval);
                                 }
#line 3115 "sieve/sieve.c"
    break;

  case 93: /* vtags: vtags SUBJECT string  */
#line 911 "sieve/sieve.y"
                                 {
                                     if ((yyval.cl)->u.v.subject != NULL) {
                                         sieveerror_c(sscript,
                                                      SIEVE_DUPLICATE_TAG,
                                                      ":subject");
                                         free((yyval.cl)->u.v.subject);
                                     }

                                     (yyval.cl)->u.v.subject = (yyvsp[0].sval);
                                 }
#line 3130 "sieve/sieve.c"
    break;

  case 94: /* vtags: vtags FROM string  */
#line 921 "sieve/sieve.y"
                                 {
                                     if ((yyval.cl)->u.v.from != NULL) {
                                         sieveerror_c(sscript,
                                                      SIEVE_DUPLICATE_TAG,
                                                      ":from");
                                         free((yyval.cl)->u.v.from);
                                     }

                                     (yyval.cl)->u.v.from = (yyvsp[0].sval);
                                 }
#line 3145 "sieve/sieve.c"
    break;

  case 95: /* vtags: vtags ADDRESSES stringlist  */
#line 933 "sieve/sieve.y"
                                 {
                                     if ((yyval.cl)->u.v.addresses != NULL) {
                                         sieveerror_c(sscript,
                                                      SIEVE_DUPLICATE_TAG,
                                                      ":addresses");
                                         strarray_free((yyval.cl)->u.v.addresses);
                                     }

                                     (yyval.cl)->u.v.addresses = (yyvsp[0].sl);
                                 }
#line 3160 "sieve/sieve.c"
    break;

  case 96: /* vtags: vtags MIME  */
#line 943 "sieve/sieve.y"
                                 {
                                     if ((yyval.cl)->u.v.mime != -1) {
                                         sieveerror_c(sscript,
                                                      SIEVE_DUPLICATE_TAG,
                                                      ":mime");
                                     }

                                     (yyval.cl)->u.v.mime = 1;
                                 }
#line 3174 "sieve/sieve.c"
    break;

  case 97: /* vtags: vtags HANDLE string  */
#line 952 "sieve/sieve.y"
                                 {
                                     if ((yyval.cl)->u.v.handle != NULL) {
                                         sieveerror_c(sscript,
                                                      SIEVE_DUPLICATE_TAG,
                                                      ":handle");
                                         free((yyval.cl)->u.v.handle);
                                     }

                                     (yyval.cl)->u.v.handle = (yyvsp[0].sval);
                                 }
#line 3189 "sieve/sieve.c"
    break;

  case 99: /* fcctags: FCC string  */
#line 966 "sieve/sieve.y"
                                 {
                                     /* **fccfolder assigned by the calling rule */
                                     if (*fccfolder != NULL) {
                                         sieveerror_c(sscript,
                                                      SIEVE_DUPLICATE_TAG,
                                                      ":fcc");
                                         free(*fccfolder);
                                     }
                                     else if (!supported(SIEVE_CAPA_FCC)) {
                                         sieveerror_c(sscript,
                                                      SIEVE_MISSING_REQUIRE,
                                                      "fcc");
                                     }

                                     *fccfolder = (yyvsp[0].sval);
                                 }
#line 3210 "sieve/sieve.c"
    break;

  case 104: /* flagaction: SETFLAG  */
#line 990 "sieve/sieve.y"
                                 { (yyval.nval) = B_SETFLAG;    }
#line 3216 "sieve/sieve.c"
    break;

  case 105: /* flagaction: ADDFLAG  */
#line 991 "sieve/sieve.y"
                                 { (yyval.nval) = B_ADDFLAG;    }
#line 3222 "sieve/sieve.c"
    break;

  case 106: /* flagaction: REMOVEFLAG  */
#line 992 "sieve/sieve.y"
                                 { (yyval.nval) = B_REMOVEFLAG; }
#line 3228 "sieve/sieve.c"
    break;

  case 107: /* flagtags: %empty  */
#line 997 "sieve/sieve.y"
                                 { (yyval.cl) = new_command((yyvsp[0].nval), sscript); }
#line 3234 "sieve/sieve.c"
    break;

  case 108: /* flagtags: flagtags string  */
#line 998 "sieve/sieve.y"
                                 {
                                     if ((yyval.cl)->u.fl.variable != NULL) {
                                         sieveerror_c(sscript,
                                                      SIEVE_DUPLICATE_ARG,
                                                      "variablename");
                                         free((yyval.cl)->u.fl.variable);
                                     }
                                     else if (!supported(SIEVE_CAPA_IMAP4FLAGS)) {
                                         sieveerror_c(sscript,
                                                      SIEVE_MISSING_REQUIRE,
                                                      "imap4flags");
                                     }

                                     (yyval.cl)->u.fl.variable = (yyvsp[0].sval);
                                 }
#line 3254 "sieve/sieve.c"
    break;

  case 109: /* ahtags: %empty  */
#line 1017 "sieve/sieve.y"
                                 { (yyval.cl) = new_command(B_ADDHEADER, sscript); }
#line 3260 "sieve/sieve.c"
    break;

  case 110: /* ahtags: ahtags LAST  */
#line 1018 "sieve/sieve.y"
                                 {
                                     if ((yyval.cl)->u.ah.index < 0) {
                                         sieveerror_c(sscript,
                                                      SIEVE_DUPLICATE_TAG,
                                                      ":last");
                                     }

                                     (yyval.cl)->u.ah.index = -1;
                                 }
#line 3274 "sieve/sieve.c"
    break;

  case 111: /* dhtags: %empty  */
#line 1031 "sieve/sieve.y"
                                 {
                                     (yyval.cl) = new_command(B_DELETEHEADER, sscript);
                                     ctags = &((yyval.cl)->u.dh.comp);
                                 }
#line 3283 "sieve/sieve.c"
    break;

  case 116: /* reject: REJCT  */
#line 1043 "sieve/sieve.y"
                                 { (yyval.nval) = B_REJECT;  }
#line 3289 "sieve/sieve.c"
    break;

  case 117: /* reject: EREJECT  */
#line 1044 "sieve/sieve.y"
                                 { (yyval.nval) = B_EREJECT; }
#line 3295 "sieve/sieve.c"
    break;

  case 118: /* ntags: %empty  */
#line 1049 "sieve/sieve.y"
                                 {
                                     (yyval.cl) = new_command(B_ENOTIFY, sscript);
                                     flags = &((yyval.cl)->u.n.fcc.flags);
                                     create = &((yyval.cl)->u.n.fcc.create);
                                     specialuse = &((yyval.cl)->u.n.fcc.t.specialuse);
                                     mailboxid = &((yyval.cl)->u.n.fcc.t.mailboxid);
                                     fccfolder = &((yyval.cl)->u.n.fcc.t.folder);
                                 }
#line 3308 "sieve/sieve.c"
    break;

  case 119: /* ntags: ntags FROM string  */
#line 1057 "sieve/sieve.y"
                                 {
                                     if ((yyval.cl)->u.n.from != NULL) {
                                         sieveerror_c(sscript,
                                                      SIEVE_DUPLICATE_TAG,
                                                      ":from");
                                         free((yyval.cl)->u.n.from);
                                     }

                                     (yyval.cl)->u.n.from = (yyvsp[0].sval);
                                 }
#line 3323 "sieve/sieve.c"
    break;

  case 120: /* ntags: ntags IMPORTANCE priority  */
#line 1069 "sieve/sieve.y"
                                 {
                                     if ((yyval.cl)->u.n.priority != -1) {
                                         sieveerror_c(sscript,
                                                      SIEVE_DUPLICATE_TAG,
                                                      ":importance");
                                     }

                                     (yyval.cl)->u.n.priority = (yyvsp[0].nval);
                                 }
#line 3337 "sieve/sieve.c"
    break;

  case 121: /* ntags: ntags MESSAGE string  */
#line 1078 "sieve/sieve.y"
                                 {
                                     if ((yyval.cl)->u.n.message != NULL) {
                                         sieveerror_c(sscript,
                                                      SIEVE_DUPLICATE_TAG,
                                                      ":message");
                                         free((yyval.cl)->u.n.message);
                                     }

                                     (yyval.cl)->u.n.message = (yyvsp[0].sval);
                                 }
#line 3352 "sieve/sieve.c"
    break;

  case 122: /* ntags: ntags OPTIONS stringlist  */
#line 1090 "sieve/sieve.y"
                                 {
                                     if ((yyval.cl)->u.n.options != NULL) {
                                         sieveerror_c(sscript,
                                                      SIEVE_DUPLICATE_TAG,
                                                      ":options");
                                         strarray_free((yyval.cl)->u.n.options);
                                     }

                                     (yyval.cl)->u.n.options = (yyvsp[0].sl);
                                 }
#line 3367 "sieve/sieve.c"
    break;

  case 124: /* priority: LOW  */
#line 1105 "sieve/sieve.y"
                                 { (yyval.nval) = B_LOW;    }
#line 3373 "sieve/sieve.c"
    break;

  case 125: /* priority: NORMAL  */
#line 1106 "sieve/sieve.y"
                                 { (yyval.nval) = B_NORMAL; }
#line 3379 "sieve/sieve.c"
    break;

  case 126: /* priority: HIGH  */
#line 1107 "sieve/sieve.y"
                                 { (yyval.nval) = B_HIGH;   }
#line 3385 "sieve/sieve.c"
    break;

  case 127: /* sntags: %empty  */
#line 1112 "sieve/sieve.y"
                                 {
                                     (yyval.cl) = new_command(B_SNOOZE, sscript);
                                     create = &((yyval.cl)->u.sn.f.create);
                                     specialuse = &((yyval.cl)->u.sn.f.t.specialuse);
                                     mailboxid = &((yyval.cl)->u.sn.f.t.mailboxid);
                                 }
#line 3396 "sieve/sieve.c"
    break;

  case 128: /* sntags: sntags MAILBOX string  */
#line 1118 "sieve/sieve.y"
                                 {
                                     if ((yyval.cl)->u.sn.f.t.folder != NULL) {
                                         sieveerror_c(sscript,
                                                      SIEVE_DUPLICATE_TAG,
                                                      ":mailbox");
                                         free((yyval.cl)->u.sn.f.t.folder);
                                     }

                                     (yyval.cl)->u.sn.f.t.folder = (yyvsp[0].sval);
                                 }
#line 3411 "sieve/sieve.c"
    break;

  case 132: /* sntags: sntags ADDFLAGS stringlist  */
#line 1133 "sieve/sieve.y"
                                 {
                                     if ((yyval.cl)->u.sn.addflags != NULL) {
                                         sieveerror_c(sscript,
                                                      SIEVE_DUPLICATE_TAG,
                                                      ":addflags");
                                         free((yyval.cl)->u.sn.addflags);
                                     }
                                     else if (!supported(SIEVE_CAPA_IMAP4FLAGS)) {
                                         sieveerror_c(sscript,
                                                      SIEVE_MISSING_REQUIRE,
                                                      "imap4flags");
                                     }

                                     (yyval.cl)->u.sn.addflags = (yyvsp[0].sl);
                                 }
#line 3431 "sieve/sieve.c"
    break;

  case 133: /* sntags: sntags REMOVEFLAGS stringlist  */
#line 1149 "sieve/sieve.y"
                                 {
                                     if ((yyval.cl)->u.sn.removeflags != NULL) {
                                         sieveerror_c(sscript,
                                                      SIEVE_DUPLICATE_TAG,
                                                      ":removeflags");
                                         free((yyval.cl)->u.sn.removeflags);
                                     }
                                     else if (!supported(SIEVE_CAPA_IMAP4FLAGS)) {
                                         sieveerror_c(sscript,
                                                      SIEVE_MISSING_REQUIRE,
                                                      "imap4flags");
                                     }

                                     (yyval.cl)->u.sn.removeflags = (yyvsp[0].sl);
                                 }
#line 3451 "sieve/sieve.c"
    break;

  case 134: /* sntags: sntags WEEKDAYS weekdaylist  */
#line 1165 "sieve/sieve.y"
                                 {
                                     if ((yyval.cl)->u.sn.days != 0) {
                                         sieveerror_c(sscript,
                                                      SIEVE_DUPLICATE_TAG,
                                                      ":weekdays");
                                     }

                                     (yyval.cl)->u.sn.days = (yyvsp[0].nval);
                                 }
#line 3465 "sieve/sieve.c"
    break;

  case 135: /* sntags: sntags TZID string  */
#line 1174 "sieve/sieve.y"
                                 {
                                     if ((yyval.cl)->u.sn.tzid != NULL) {
                                         sieveerror_c(sscript,
                                                      SIEVE_DUPLICATE_TAG,
                                                      ":tzid");
                                         free((yyval.cl)->u.sn.tzid);
                                     }

                                     (yyval.cl)->u.sn.tzid = (yyvsp[0].sval);
                                 }
#line 3480 "sieve/sieve.c"
    break;

  case 137: /* weekdaylist: '[' weekdays ']'  */
#line 1188 "sieve/sieve.y"
                                 { (yyval.nval) = (yyvsp[-1].nval); }
#line 3486 "sieve/sieve.c"
    break;

  case 139: /* weekdays: weekdays ',' weekday  */
#line 1193 "sieve/sieve.y"
                                 { (yyval.nval) = (yyvsp[-2].nval) | (yyvsp[0].nval); }
#line 3492 "sieve/sieve.c"
    break;

  case 140: /* weekday: STRING  */
#line 1197 "sieve/sieve.y"
                                 { (yyval.nval) = verify_weekday(sscript, (yyvsp[0].sval)); }
#line 3498 "sieve/sieve.c"
    break;

  case 142: /* timelist: '[' times ']'  */
#line 1202 "sieve/sieve.y"
                                 { (yyval.nl) = (yyvsp[-1].nl); }
#line 3504 "sieve/sieve.c"
    break;

  case 144: /* times: times ',' time  */
#line 1207 "sieve/sieve.y"
                                 { (yyval.nl) = (yyvsp[-2].nl); arrayu64_add((yyval.nl), (yyvsp[0].nval)); }
#line 3510 "sieve/sieve.c"
    break;

  case 145: /* time1: time  */
#line 1211 "sieve/sieve.y"
                                 {
                                     (yyval.nl) = arrayu64_new();
                                     arrayu64_add((yyval.nl), (yyvsp[0].nval));
                                 }
#line 3519 "sieve/sieve.c"
    break;

  case 146: /* time: STRING  */
#line 1218 "sieve/sieve.y"
                                 { (yyval.nval) = verify_time(sscript, (yyvsp[0].sval)); }
#line 3525 "sieve/sieve.c"
    break;

  case 147: /* imiptags: %empty  */
#line 1223 "sieve/sieve.y"
                                 { (yyval.cl) = new_command(B_PROCESSIMIP, sscript); }
#line 3531 "sieve/sieve.c"
    break;

  case 148: /* imiptags: imiptags INVITESONLY  */
#line 1224 "sieve/sieve.y"
                                 {
                                     if ((yyval.cl)->u.imip.invites_only) {
                                         sieveerror_c(sscript,
                                                      SIEVE_DUPLICATE_TAG,
                                                      ":invitesonly");
                                     }
                                     else if ((yyval.cl)->u.imip.updates_only) {
                                         sieveerror_c(sscript,
                                                      SIEVE_CONFLICTING_TAGS,
                                                      ":invitesonly",
                                                      ":updatesonly");
                                     }
                                     else if ((yyval.cl)->u.imip.delete_canceled) {
                                         sieveerror_c(sscript,
                                                      SIEVE_CONFLICTING_TAGS,
                                                      ":invitesonly",
                                                      ":deletecanceled");
                                     }

                                     (yyval.cl)->u.imip.invites_only = 1;
                                 }
#line 3557 "sieve/sieve.c"
    break;

  case 149: /* imiptags: imiptags UPDATESONLY  */
#line 1246 "sieve/sieve.y"
                                 {
                                     if ((yyval.cl)->u.imip.updates_only) {
                                         sieveerror_c(sscript,
                                                      SIEVE_DUPLICATE_TAG,
                                                      ":updatesonly");
                                     }
                                     else if ((yyval.cl)->u.imip.invites_only) {
                                         sieveerror_c(sscript,
                                                      SIEVE_CONFLICTING_TAGS,
                                                      ":invitesonly",
                                                      ":updatesonly");
                                     }
                                     else if ((yyval.cl)->u.imip.calendarid != NULL) {
                                         sieveerror_c(sscript,
                                                      SIEVE_CONFLICTING_TAGS,
                                                      ":updatesonly",
                                                      ":calendarid");
                                     }

                                     (yyval.cl)->u.imip.updates_only = 1;
                                 }
#line 3583 "sieve/sieve.c"
    break;

  case 150: /* imiptags: imiptags DELETECANCELED  */
#line 1269 "sieve/sieve.y"
                                 {
                                     if ((yyval.cl)->u.imip.delete_canceled) {
                                         sieveerror_c(sscript,
                                                      SIEVE_DUPLICATE_TAG,
                                                      ":deletecanceled");
                                     }
                                     else if ((yyval.cl)->u.imip.invites_only) {
                                         sieveerror_c(sscript,
                                                      SIEVE_CONFLICTING_TAGS,
                                                      ":invitesonly",
                                                      ":deletecanceled");
                                     }

                                     (yyval.cl)->u.imip.delete_canceled = 1;
                                 }
#line 3603 "sieve/sieve.c"
    break;

  case 151: /* imiptags: imiptags CALENDARID string  */
#line 1285 "sieve/sieve.y"
                                 {
                                     if ((yyval.cl)->u.imip.calendarid != NULL) {
                                         sieveerror_c(sscript,
                                                      SIEVE_DUPLICATE_TAG,
                                                      ":calendarid");
                                         free((yyval.cl)->u.imip.calendarid);
                                     }
                                     else if ((yyval.cl)->u.imip.updates_only) {
                                         sieveerror_c(sscript,
                                                      SIEVE_CONFLICTING_TAGS,
                                                      ":updatesonly",
                                                      ":calendarid");
                                     }

                                     (yyval.cl)->u.imip.calendarid = (yyvsp[0].sval);
                                 }
#line 3624 "sieve/sieve.c"
    break;

  case 152: /* imiptags: imiptags OUTCOME string  */
#line 1302 "sieve/sieve.y"
                                 {
                                     if ((yyval.cl)->u.imip.outcome_var != NULL) {
                                         sieveerror_c(sscript,
                                                      SIEVE_DUPLICATE_TAG,
                                                      ":outcome");
                                         free((yyval.cl)->u.imip.outcome_var);
                                     }
                                     else if (!supported(SIEVE_CAPA_VARIABLES)) {
                                         sieveerror_c(sscript,
                                                      SIEVE_MISSING_REQUIRE,
                                                      "variables");
                                     }

                                     (yyval.cl)->u.imip.outcome_var = (yyvsp[0].sval);
                                 }
#line 3644 "sieve/sieve.c"
    break;

  case 153: /* imiptags: imiptags ERRSTR string  */
#line 1317 "sieve/sieve.y"
                                 {
                                     if ((yyval.cl)->u.imip.errstr_var != NULL) {
                                         sieveerror_c(sscript,
                                                      SIEVE_DUPLICATE_TAG,
                                                      ":errstr");
                                         free((yyval.cl)->u.imip.errstr_var);
                                     }
                                     else if (!supported(SIEVE_CAPA_VARIABLES)) {
                                         sieveerror_c(sscript,
                                                      SIEVE_MISSING_REQUIRE,
                                                      "variables");
                                     }

                                     (yyval.cl)->u.imip.errstr_var = (yyvsp[0].sval);
                                 }
#line 3664 "sieve/sieve.c"
    break;

  case 154: /* ikttags: %empty  */
#line 1336 "sieve/sieve.y"
                                 {
                                     (yyval.cl) = new_command(B_IKEEP_TARGET, sscript);
                                     specialuse = &((yyval.cl)->u.ikt.specialuse);
                                     mailboxid = &((yyval.cl)->u.ikt.mailboxid);
                                 }
#line 3674 "sieve/sieve.c"
    break;

  case 157: /* testlist: '(' tests ')'  */
#line 1349 "sieve/sieve.y"
                                 { (yyval.testl) = (yyvsp[-1].testl); }
#line 3680 "sieve/sieve.c"
    break;

  case 158: /* tests: test  */
#line 1353 "sieve/sieve.y"
                                 { (yyval.testl) = new_testlist((yyvsp[0].test), NULL); }
#line 3686 "sieve/sieve.c"
    break;

  case 159: /* tests: test ',' tests  */
#line 1354 "sieve/sieve.y"
                                 { (yyval.testl) = new_testlist((yyvsp[-2].test), (yyvsp[0].testl)); }
#line 3692 "sieve/sieve.c"
    break;

  case 160: /* test: ANYOF testlist  */
#line 1358 "sieve/sieve.y"
                                 { (yyval.test) = build_anyof(sscript, (yyvsp[0].testl)); }
#line 3698 "sieve/sieve.c"
    break;

  case 161: /* test: ALLOF testlist  */
#line 1359 "sieve/sieve.y"
                                 { (yyval.test) = build_allof(sscript, (yyvsp[0].testl)); }
#line 3704 "sieve/sieve.c"
    break;

  case 162: /* test: NOT test  */
#line 1360 "sieve/sieve.y"
                                 { (yyval.test) = build_not(sscript, (yyvsp[0].test));   }
#line 3710 "sieve/sieve.c"
    break;

  case 163: /* test: SFALSE  */
#line 1361 "sieve/sieve.y"
                                 { (yyval.test) = new_test(BC_FALSE, sscript); }
#line 3716 "sieve/sieve.c"
    break;

  case 164: /* test: STRUE  */
#line 1362 "sieve/sieve.y"
                                 { (yyval.test) = new_test(BC_TRUE, sscript);  }
#line 3722 "sieve/sieve.c"
    break;

  case 165: /* test: EXISTS stringlist  */
#line 1363 "sieve/sieve.y"
                                 {
                                     (yyval.test) = new_test(BC_EXISTS, sscript);
                                     (yyval.test)->u.sl = (yyvsp[0].sl);
                                     (yyval.test)->nargs = bc_precompile((yyval.test)->args, "S",
                                                               (yyval.test)->u.sl);
                                 }
#line 3733 "sieve/sieve.c"
    break;

  case 166: /* test: SIZE sizetag NUMBER  */
#line 1369 "sieve/sieve.y"
                                 {
                                     (yyval.test) = new_test(BC_SIZE, sscript);
                                     (yyval.test)->u.sz.t = (yyvsp[-1].nval);
                                     (yyval.test)->u.sz.n = (yyvsp[0].nval);
                                     (yyval.test)->nargs = bc_precompile((yyval.test)->args, "ii",
                                                               (yyval.test)->u.sz.t,
                                                               (yyval.test)->u.sz.n);
                                 }
#line 3746 "sieve/sieve.c"
    break;

  case 167: /* test: HEADERT htags stringlist stringlist  */
#line 1379 "sieve/sieve.y"
                                 { (yyval.test) = build_header(sscript, (yyvsp[-2].test), (yyvsp[-1].sl), (yyvsp[0].sl)); }
#line 3752 "sieve/sieve.c"
    break;

  case 168: /* test: ADDRESS atags stringlist stringlist  */
#line 1382 "sieve/sieve.y"
                                 { (yyval.test) = build_address(sscript, (yyvsp[-2].test), (yyvsp[-1].sl), (yyvsp[0].sl)); }
#line 3758 "sieve/sieve.c"
    break;

  case 169: /* test: ENVELOPE etags stringlist stringlist  */
#line 1384 "sieve/sieve.y"
                                 { (yyval.test) = build_envelope(sscript, (yyvsp[-2].test), (yyvsp[-1].sl), (yyvsp[0].sl)); }
#line 3764 "sieve/sieve.c"
    break;

  case 170: /* test: BODY btags stringlist  */
#line 1386 "sieve/sieve.y"
                                 { (yyval.test) = build_body(sscript, (yyvsp[-1].test), (yyvsp[0].sl)); }
#line 3770 "sieve/sieve.c"
    break;

  case 171: /* test: ENVIRONMENT envtags string stringlist  */
#line 1389 "sieve/sieve.y"
                                 { (yyval.test) = build_mbox_meta(sscript,
                                                        (yyvsp[-2].test), NULL, (yyvsp[-1].sval), (yyvsp[0].sl)); }
#line 3777 "sieve/sieve.c"
    break;

  case 172: /* test: STRINGT strtags stringlist stringlist  */
#line 1393 "sieve/sieve.y"
                                 { (yyval.test) = build_stringt(sscript, (yyvsp[-2].test), (yyvsp[-1].sl), (yyvsp[0].sl)); }
#line 3783 "sieve/sieve.c"
    break;

  case 173: /* test: HASFLAG hftags stringlist stringlist  */
#line 1400 "sieve/sieve.y"
                                 { (yyval.test) = build_hasflag(sscript, (yyvsp[-2].test), (yyvsp[-1].sl), (yyvsp[0].sl)); }
#line 3789 "sieve/sieve.c"
    break;

  case 174: /* test: HASFLAG hftags stringlist  */
#line 1402 "sieve/sieve.y"
                                 { (yyval.test) = build_hasflag(sscript, (yyvsp[-1].test), NULL, (yyvsp[0].sl)); }
#line 3795 "sieve/sieve.c"
    break;

  case 175: /* test: DATE dttags string string stringlist  */
#line 1405 "sieve/sieve.y"
                                 { (yyval.test) = build_date(sscript, (yyvsp[-3].test), (yyvsp[-2].sval), (yyvsp[-1].sval), (yyvsp[0].sl)); }
#line 3801 "sieve/sieve.c"
    break;

  case 176: /* test: CURRENTDATE cdtags string stringlist  */
#line 1408 "sieve/sieve.y"
                                 { (yyval.test) = build_date(sscript, (yyvsp[-2].test), NULL, (yyvsp[-1].sval), (yyvsp[0].sl)); }
#line 3807 "sieve/sieve.c"
    break;

  case 177: /* test: VALIDNOTIFYMETHOD stringlist  */
#line 1411 "sieve/sieve.y"
                                 {
                                     (yyval.test) = new_test(BC_VALIDNOTIFYMETHOD, sscript);
                                     (yyval.test)->u.sl = (yyvsp[0].sl);
                                     (yyval.test)->nargs = bc_precompile((yyval.test)->args, "S",
                                                               (yyval.test)->u.sl);
                                 }
#line 3818 "sieve/sieve.c"
    break;

  case 178: /* test: NOTIFYMETHODCAPABILITY methtags string string stringlist  */
#line 1418 "sieve/sieve.y"
                                 { (yyval.test) = build_mbox_meta(sscript,
                                                        (yyvsp[-3].test), (yyvsp[-2].sval), (yyvsp[-1].sval), (yyvsp[0].sl)); }
#line 3825 "sieve/sieve.c"
    break;

  case 179: /* test: IHAVE stringlist  */
#line 1421 "sieve/sieve.y"
                                 { (yyval.test) = build_ihave(sscript, (yyvsp[0].sl)); }
#line 3831 "sieve/sieve.c"
    break;

  case 180: /* test: MAILBOXEXISTS stringlist  */
#line 1424 "sieve/sieve.y"
                                 {
                                     (yyval.test) = new_test(BC_MAILBOXEXISTS, sscript);
                                     (yyval.test) = build_mbox_meta(sscript,
                                                          (yyval.test), NULL, NULL, (yyvsp[0].sl));
                                 }
#line 3841 "sieve/sieve.c"
    break;

  case 181: /* $@1: %empty  */
#line 1430 "sieve/sieve.y"
                   { bctype = BC_METADATA; }
#line 3847 "sieve/sieve.c"
    break;

  case 182: /* test: METADATA $@1 mtags string string stringlist  */
#line 1431 "sieve/sieve.y"
                                 { (yyval.test) = build_mbox_meta(sscript,
                                                        (yyvsp[-3].test), (yyvsp[-2].sval), (yyvsp[-1].sval), (yyvsp[0].sl)); }
#line 3854 "sieve/sieve.c"
    break;

  case 183: /* test: METADATAEXISTS string stringlist  */
#line 1435 "sieve/sieve.y"
                                 {
                                     (yyval.test) = new_test(BC_METADATAEXISTS, sscript);
                                     (yyval.test) = build_mbox_meta(sscript,
                                                          (yyval.test), (yyvsp[-1].sval), NULL, (yyvsp[0].sl));
                                 }
#line 3864 "sieve/sieve.c"
    break;

  case 184: /* $@2: %empty  */
#line 1441 "sieve/sieve.y"
                         { bctype = BC_SERVERMETADATA; }
#line 3870 "sieve/sieve.c"
    break;

  case 185: /* test: SERVERMETADATA $@2 mtags string stringlist  */
#line 1442 "sieve/sieve.y"
                                 { (yyval.test) = build_mbox_meta(sscript,
                                                        (yyvsp[-2].test), NULL, (yyvsp[-1].sval), (yyvsp[0].sl)); }
#line 3877 "sieve/sieve.c"
    break;

  case 186: /* test: SERVERMETADATAEXISTS stringlist  */
#line 1446 "sieve/sieve.y"
                                 {
                                     (yyval.test) = new_test(BC_SERVERMETADATAEXISTS,
                                                   sscript);
                                     (yyval.test) = build_mbox_meta(sscript,
                                                          (yyval.test), NULL, NULL, (yyvsp[0].sl));
                                 }
#line 3888 "sieve/sieve.c"
    break;

  case 187: /* test: VALIDEXTLIST stringlist  */
#line 1454 "sieve/sieve.y"
                                 {
                                     (yyval.test) = new_test(BC_VALIDEXTLIST, sscript);
                                     (yyval.test)->u.sl = (yyvsp[0].sl);
                                     (yyval.test)->nargs = bc_precompile((yyval.test)->args, "S",
                                                               (yyval.test)->u.sl);
                                 }
#line 3899 "sieve/sieve.c"
    break;

  case 188: /* test: DUPLICATE duptags  */
#line 1460 "sieve/sieve.y"
                                 { (yyval.test) = build_duplicate(sscript, (yyvsp[0].test)); }
#line 3905 "sieve/sieve.c"
    break;

  case 189: /* test: SPECIALUSEEXISTS stringlist  */
#line 1463 "sieve/sieve.y"
                                 { 
                                     (yyval.test) = new_test(BC_SPECIALUSEEXISTS, sscript);
                                     (yyval.test) = build_mbox_meta(sscript,
                                                          (yyval.test), NULL, NULL, (yyvsp[0].sl));
                                 }
#line 3915 "sieve/sieve.c"
    break;

  case 190: /* test: SPECIALUSEEXISTS string stringlist  */
#line 1470 "sieve/sieve.y"
                                 {
                                     (yyval.test) = new_test(BC_SPECIALUSEEXISTS, sscript);
                                     (yyval.test) = build_mbox_meta(sscript,
                                                          (yyval.test), (yyvsp[-1].sval), NULL, (yyvsp[0].sl));
                                 }
#line 3925 "sieve/sieve.c"
    break;

  case 191: /* test: MAILBOXIDEXISTS stringlist  */
#line 1477 "sieve/sieve.y"
                                 {
                                     (yyval.test) = new_test(BC_MAILBOXIDEXISTS, sscript);
                                     (yyval.test) = build_mbox_meta(sscript,
                                                          (yyval.test), NULL, NULL, (yyvsp[0].sl));
                                 }
#line 3935 "sieve/sieve.c"
    break;

  case 192: /* test: JMAPQUERY string  */
#line 1483 "sieve/sieve.y"
                                 {
                                     (yyval.test) = new_test(BC_JMAPQUERY, sscript);
                                     (yyval.test) = build_jmapquery(sscript, (yyval.test), (yyvsp[0].sval));
                                 }
#line 3944 "sieve/sieve.c"
    break;

  case 193: /* test: error  */
#line 1488 "sieve/sieve.y"
                                 { (yyval.test) = new_test(BC_FALSE, sscript); }
#line 3950 "sieve/sieve.c"
    break;

  case 194: /* sizetag: OVER  */
#line 1493 "sieve/sieve.y"
                                 { (yyval.nval) = B_OVER;  }
#line 3956 "sieve/sieve.c"
    break;

  case 195: /* sizetag: UNDER  */
#line 1494 "sieve/sieve.y"
                                 { (yyval.nval) = B_UNDER; }
#line 3962 "sieve/sieve.c"
    break;

  case 196: /* htags: %empty  */
#line 1499 "sieve/sieve.y"
                                 {
                                     (yyval.test) = new_test(BC_HEADER, sscript);
                                     ctags = &((yyval.test)->u.hhs.comp);
                                 }
#line 3971 "sieve/sieve.c"
    break;

  case 201: /* matchtype: matchtag  */
#line 1511 "sieve/sieve.y"
                                 {
                                     /* *ctags assigned by the calling rule */
                                     if (ctags->match != -1) {
                                         sieveerror_c(sscript,
                                                      SIEVE_MULTIPLE_TAGS,
                                                      "match-type");
                                     }

                                     ctags->match = (yyvsp[0].nval);
                                 }
#line 3986 "sieve/sieve.c"
    break;

  case 202: /* matchtype: relmatch relation  */
#line 1522 "sieve/sieve.y"
                                 {
                                     if (ctags->match != B_COUNT &&
                                         ctags->match != B_VALUE &&
                                         !supported(SIEVE_CAPA_RELATIONAL)) {
                                         sieveerror_c(sscript,
                                                      SIEVE_MISSING_REQUIRE,
                                                      "relational");
                                     }
                                     if (ctags->match != -1) {
                                         sieveerror_c(sscript,
                                                      SIEVE_MULTIPLE_TAGS,
                                                      "match-type");
                                     }

                                     ctags->match = (yyvsp[-1].nval);
                                     ctags->relation = (yyvsp[0].nval);
                                 }
#line 4008 "sieve/sieve.c"
    break;

  case 203: /* matchtag: IS  */
#line 1543 "sieve/sieve.y"
                                 { (yyval.nval) = B_IS;       }
#line 4014 "sieve/sieve.c"
    break;

  case 204: /* matchtag: CONTAINS  */
#line 1544 "sieve/sieve.y"
                                 { (yyval.nval) = B_CONTAINS; }
#line 4020 "sieve/sieve.c"
    break;

  case 205: /* matchtag: MATCHES  */
#line 1545 "sieve/sieve.y"
                                 { (yyval.nval) = B_MATCHES;  }
#line 4026 "sieve/sieve.c"
    break;

  case 206: /* matchtag: REGEX  */
#line 1546 "sieve/sieve.y"
                                 {
                                     if (!supported(SIEVE_CAPA_REGEX)) {
                                         sieveerror_c(sscript,
                                                      SIEVE_MISSING_REQUIRE,
                                                      "regex");
                                     }

                                     (yyval.nval) = B_REGEX;
                                 }
#line 4040 "sieve/sieve.c"
    break;

  case 207: /* relmatch: COUNT  */
#line 1559 "sieve/sieve.y"
                                 { (yyval.nval) = B_COUNT; }
#line 4046 "sieve/sieve.c"
    break;

  case 208: /* relmatch: VALUE  */
#line 1560 "sieve/sieve.y"
                                 { (yyval.nval) = B_VALUE; }
#line 4052 "sieve/sieve.c"
    break;

  case 209: /* relation: EQ  */
#line 1565 "sieve/sieve.y"
                                 { (yyval.nval) = B_EQ; }
#line 4058 "sieve/sieve.c"
    break;

  case 210: /* relation: NE  */
#line 1566 "sieve/sieve.y"
                                 { (yyval.nval) = B_NE; }
#line 4064 "sieve/sieve.c"
    break;

  case 211: /* relation: GT  */
#line 1567 "sieve/sieve.y"
                                 { (yyval.nval) = B_GT; }
#line 4070 "sieve/sieve.c"
    break;

  case 212: /* relation: GE  */
#line 1568 "sieve/sieve.y"
                                 { (yyval.nval) = B_GE; }
#line 4076 "sieve/sieve.c"
    break;

  case 213: /* relation: LT  */
#line 1569 "sieve/sieve.y"
                                 { (yyval.nval) = B_LT; }
#line 4082 "sieve/sieve.c"
    break;

  case 214: /* relation: LE  */
#line 1570 "sieve/sieve.y"
                                 { (yyval.nval) = B_LE; }
#line 4088 "sieve/sieve.c"
    break;

  case 215: /* listmatch: LIST  */
#line 1575 "sieve/sieve.y"
                                 {
                                     /* *ctags assigned by the calling rule */
                                     if (ctags->match != B_LIST &&
                                         !supported(SIEVE_CAPA_EXTLISTS)) {
                                         sieveerror_c(sscript,
                                                      SIEVE_MISSING_REQUIRE,
                                                      "extlists");
                                     }
                                     if (ctags->match != -1) {
                                         sieveerror_c(sscript,
                                                      SIEVE_MULTIPLE_TAGS,
                                                      "match-type");
                                     }

                                     ctags->match = B_LIST;
                                 }
#line 4109 "sieve/sieve.c"
    break;

  case 216: /* comparator: COMPARATOR collation  */
#line 1596 "sieve/sieve.y"
                                 {
                                     /* *ctags assigned by the calling rule */
                                     if (ctags->collation != -1) {
                                         sieveerror_c(sscript,
                                                      SIEVE_DUPLICATE_TAG,
                                                      ":comparator");
                                     }

                                     ctags->collation = (yyvsp[0].nval);
                                 }
#line 4124 "sieve/sieve.c"
    break;

  case 217: /* collation: OCTET  */
#line 1610 "sieve/sieve.y"
                                 { (yyval.nval) = B_OCTET;        }
#line 4130 "sieve/sieve.c"
    break;

  case 218: /* collation: ASCIICASEMAP  */
#line 1611 "sieve/sieve.y"
                                 { (yyval.nval) = B_ASCIICASEMAP; }
#line 4136 "sieve/sieve.c"
    break;

  case 219: /* collation: ASCIINUMERIC  */
#line 1612 "sieve/sieve.y"
                                 {
                                     if (!supported(SIEVE_CAPA_COMP_NUMERIC)) {
                                         sieveerror_c(sscript,
                                                      SIEVE_MISSING_REQUIRE,
                                                      "comparator-"
                                                      "i;ascii-numeric");
                                     }

                                     (yyval.nval) = B_ASCIINUMERIC;
                                 }
#line 4151 "sieve/sieve.c"
    break;

  case 220: /* indexext: index  */
#line 1626 "sieve/sieve.y"
                                 {
                                     /* *ctags assigned by the calling rule */
                                     if (ctags->index != 0 &&
                                         !supported(SIEVE_CAPA_INDEX)) {
                                         sieveerror_c(sscript,
                                                      SIEVE_MISSING_REQUIRE,
                                                      "index");
                                     }
                                 }
#line 4165 "sieve/sieve.c"
    break;

  case 221: /* index: INDEX NUMBER  */
#line 1637 "sieve/sieve.y"
                                 {
                                     /* *ctags assigned by the calling rule */
                                     if (ctags->index == INT_MIN) {
                                         /* parsed :last before :index */
                                         ctags->index = -(yyvsp[0].nval);
                                     }
                                     else if (ctags->index != 0) {
                                         sieveerror_c(sscript,
                                                      SIEVE_DUPLICATE_TAG,
                                                      ":index");
                                     }
                                     else {
                                         ctags->index = (yyvsp[0].nval);
                                     }
                                 }
#line 4185 "sieve/sieve.c"
    break;

  case 222: /* index: LAST  */
#line 1652 "sieve/sieve.y"
                                 {
                                     if (ctags->index > 0) {
                                         ctags->index *= -1;
                                     }
                                     else if (ctags->index < 0) {
                                         sieveerror_c(sscript,
                                                      SIEVE_DUPLICATE_TAG,
                                                      ":last");
                                     }
                                     else {
                                         /* special value to indicate that
                                            we parsed :last before :index */
                                         ctags->index = INT_MIN;
                                     }
                                 }
#line 4205 "sieve/sieve.c"
    break;

  case 223: /* atags: %empty  */
#line 1671 "sieve/sieve.y"
                                 {
                                     (yyval.test) = new_test(BC_ADDRESS, sscript);
                                     ctags = &((yyval.test)->u.ae.comp);
                                 }
#line 4214 "sieve/sieve.c"
    break;

  case 229: /* addrpart: addrparttag  */
#line 1684 "sieve/sieve.y"
                                {
                                     /* $0 refers to a test_t* (ADDR/ENV)*/
                                     test_t *test = (yyvsp[-1].test);

                                     if (test->u.ae.addrpart != -1) {
                                         sieveerror_c(sscript,
                                                      SIEVE_MULTIPLE_TAGS,
                                                      "address-part");
                                     }

                                     test->u.ae.addrpart = (yyvsp[0].nval);
                                 }
#line 4231 "sieve/sieve.c"
    break;

  case 230: /* addrparttag: ALL  */
#line 1699 "sieve/sieve.y"
                                 { (yyval.nval) = B_ALL;       }
#line 4237 "sieve/sieve.c"
    break;

  case 231: /* addrparttag: LOCALPART  */
#line 1700 "sieve/sieve.y"
                                 { (yyval.nval) = B_LOCALPART; }
#line 4243 "sieve/sieve.c"
    break;

  case 232: /* addrparttag: DOMAIN  */
#line 1701 "sieve/sieve.y"
                                 { (yyval.nval) = B_DOMAIN;    }
#line 4249 "sieve/sieve.c"
    break;

  case 233: /* addrparttag: subaddress  */
#line 1702 "sieve/sieve.y"
                                 {
                                     if (!supported(SIEVE_CAPA_SUBADDRESS)) {
                                         sieveerror_c(sscript,
                                                      SIEVE_MISSING_REQUIRE,
                                                      "subaddress");
                                     }
                                 }
#line 4261 "sieve/sieve.c"
    break;

  case 234: /* subaddress: USER  */
#line 1713 "sieve/sieve.y"
                                 { (yyval.nval) = B_USER;   }
#line 4267 "sieve/sieve.c"
    break;

  case 235: /* subaddress: DETAIL  */
#line 1714 "sieve/sieve.y"
                                 { (yyval.nval) = B_DETAIL; }
#line 4273 "sieve/sieve.c"
    break;

  case 236: /* etags: %empty  */
#line 1719 "sieve/sieve.y"
                                 {
                                     (yyval.test) = new_test(BC_ENVELOPE, sscript);
                                     ctags = &((yyval.test)->u.ae.comp);
                                 }
#line 4282 "sieve/sieve.c"
    break;

  case 241: /* envtags: %empty  */
#line 1731 "sieve/sieve.y"
                                 {
                                     (yyval.test) = new_test(BC_ENVIRONMENT, sscript);
                                     ctags = &((yyval.test)->u.mm.comp);
                                 }
#line 4291 "sieve/sieve.c"
    break;

  case 244: /* btags: %empty  */
#line 1741 "sieve/sieve.y"
                                 {
                                     (yyval.test) = new_test(BC_BODY, sscript);
                                     ctags = &((yyval.test)->u.b.comp);
                                 }
#line 4300 "sieve/sieve.c"
    break;

  case 245: /* btags: btags transform  */
#line 1745 "sieve/sieve.y"
                                 {
                                     if ((yyval.test)->u.b.transform != -1) {
                                         sieveerror_c(sscript,
                                                      SIEVE_MULTIPLE_TAGS,
                                                      "transform");
                                     }

                                     (yyval.test)->u.b.transform = (yyvsp[0].nval);
                                 }
#line 4314 "sieve/sieve.c"
    break;

  case 246: /* btags: btags CONTENT stringlist  */
#line 1756 "sieve/sieve.y"
                                 {
                                     if ((yyval.test)->u.b.transform != -1) {
                                         sieveerror_c(sscript,
                                                      SIEVE_MULTIPLE_TAGS,
                                                      "transform");
                                         strarray_free((yyval.test)->u.b.content_types);
                                     }

                                     (yyval.test)->u.b.transform = B_CONTENT;
                                     (yyval.test)->u.b.content_types = (yyvsp[0].sl);
                                 }
#line 4330 "sieve/sieve.c"
    break;

  case 249: /* transform: RAW  */
#line 1774 "sieve/sieve.y"
                                 { (yyval.nval) = B_RAW;  }
#line 4336 "sieve/sieve.c"
    break;

  case 250: /* transform: TEXT  */
#line 1775 "sieve/sieve.y"
                                 { (yyval.nval) = B_TEXT; }
#line 4342 "sieve/sieve.c"
    break;

  case 251: /* strtags: %empty  */
#line 1780 "sieve/sieve.y"
                                 {
                                     (yyval.test) = new_test(BC_STRING, sscript);
                                     ctags = &((yyval.test)->u.hhs.comp);
                                 }
#line 4351 "sieve/sieve.c"
    break;

  case 255: /* hftags: %empty  */
#line 1791 "sieve/sieve.y"
                                 {
                                     (yyval.test) = new_test(BC_HASFLAG, sscript);
                                     ctags = &((yyval.test)->u.hhs.comp);
                                 }
#line 4360 "sieve/sieve.c"
    break;

  case 258: /* dttags: %empty  */
#line 1801 "sieve/sieve.y"
                                 {
                                     (yyval.test) = new_test(BC_DATE, sscript);
                                     ctags = &((yyval.test)->u.dt.comp);
                                 }
#line 4369 "sieve/sieve.c"
    break;

  case 260: /* dttags: dttags ORIGINALZONE  */
#line 1806 "sieve/sieve.y"
                                 {
                                     if ((yyval.test)->u.dt.zone.tag != -1) {
                                         sieveerror_c(sscript,
                                                      SIEVE_DUPLICATE_TAG,
                                                      ":originalzone");
                                     }

                                     (yyval.test)->u.dt.zone.tag = B_ORIGINALZONE;
                                 }
#line 4383 "sieve/sieve.c"
    break;

  case 264: /* zone: ZONE string  */
#line 1822 "sieve/sieve.y"
                                 {
                                     /* $0 refers to a test_t* ([CURRENT]DATE)*/
                                     test_t *test = (yyvsp[-2].test);

                                     if (test->u.dt.zone.tag != -1) {
                                         sieveerror_c(sscript,
                                                      SIEVE_DUPLICATE_TAG,
                                                      ":zone");
                                     }

                                     test->u.dt.zone.tag = B_TIMEZONE;
                                     test->u.dt.zone.offset = (yyvsp[0].sval);
                                 }
#line 4401 "sieve/sieve.c"
    break;

  case 265: /* cdtags: %empty  */
#line 1839 "sieve/sieve.y"
                                 {
                                     (yyval.test) = new_test(BC_CURRENTDATE, sscript);
                                     ctags = &((yyval.test)->u.dt.comp);
                                 }
#line 4410 "sieve/sieve.c"
    break;

  case 269: /* methtags: %empty  */
#line 1850 "sieve/sieve.y"
                                 {
                                     (yyval.test) = new_test(BC_NOTIFYMETHODCAPABILITY,
                                                   sscript);
                                     ctags = &((yyval.test)->u.mm.comp);
                                 }
#line 4420 "sieve/sieve.c"
    break;

  case 272: /* mtags: %empty  */
#line 1861 "sieve/sieve.y"
                                 {
                                     /* bctype assigned by the calling rule */
                                     (yyval.test) = new_test(bctype, sscript);
                                     ctags = &((yyval.test)->u.mm.comp);
                                 }
#line 4430 "sieve/sieve.c"
    break;

  case 275: /* duptags: %empty  */
#line 1872 "sieve/sieve.y"
                                 { (yyval.test) = new_test(BC_DUPLICATE, sscript); }
#line 4436 "sieve/sieve.c"
    break;

  case 276: /* duptags: duptags idtype string  */
#line 1873 "sieve/sieve.y"
                                 {
                                     if ((yyval.test)->u.dup.idtype != -1) {
                                         sieveerror_c(sscript,
                                                      SIEVE_CONFLICTING_TAGS,
                                                      ":header", ":uniqueid");
                                         free((yyval.test)->u.dup.idval);
                                     }

                                     (yyval.test)->u.dup.idtype = (yyvsp[-1].nval);
                                     (yyval.test)->u.dup.idval = (yyvsp[0].sval);
                                 }
#line 4452 "sieve/sieve.c"
    break;

  case 277: /* duptags: duptags HANDLE string  */
#line 1884 "sieve/sieve.y"
                                 {
                                     if ((yyval.test)->u.dup.handle != NULL) {
                                         sieveerror_c(sscript,
                                                      SIEVE_DUPLICATE_TAG,
                                                      ":handle");
                                         free((yyval.test)->u.dup.handle);
                                     }

                                     (yyval.test)->u.dup.handle = (yyvsp[0].sval);
                                 }
#line 4467 "sieve/sieve.c"
    break;

  case 278: /* duptags: duptags SECONDS NUMBER  */
#line 1894 "sieve/sieve.y"
                                 {
                                     if ((yyval.test)->u.dup.seconds != -1) {
                                         sieveerror_c(sscript,
                                                      SIEVE_DUPLICATE_TAG,
                                                      ":seconds");
                                     }

                                     (yyval.test)->u.dup.seconds = (yyvsp[0].nval);
                                 }
#line 4481 "sieve/sieve.c"
    break;

  case 279: /* duptags: duptags LAST  */
#line 1903 "sieve/sieve.y"
                                 {
                                     if ((yyval.test)->u.dup.last != 0) {
                                         sieveerror_c(sscript,
                                                      SIEVE_DUPLICATE_TAG,
                                                      ":last");
                                     }

                                     (yyval.test)->u.dup.last = 1;
                                 }
#line 4495 "sieve/sieve.c"
    break;

  case 280: /* idtype: HEADER  */
#line 1916 "sieve/sieve.y"
                                 { (yyval.nval) = B_HEADER;   }
#line 4501 "sieve/sieve.c"
    break;

  case 281: /* idtype: UNIQUEID  */
#line 1917 "sieve/sieve.y"
                                 { (yyval.nval) = B_UNIQUEID; }
#line 4507 "sieve/sieve.c"
    break;


#line 4511 "sieve/sieve.c"

      default: break;
    }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", YY_CAST (yysymbol_kind_t, yyr1[yyn]), &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;

  *++yyvsp = yyval;

  /* Now 'shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */
  {
    const int yylhs = yyr1[yyn] - YYNTOKENS;
    const int yyi = yypgoto[yylhs] + *yyssp;
    yystate = (0 <= yyi && yyi <= YYLAST && yycheck[yyi] == *yyssp
               ? yytable[yyi]
               : yydefgoto[yylhs]);
  }

  goto yynewstate;


/*--------------------------------------.
| yyerrlab -- here on detecting error.  |
`--------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == YYEMPTY ? YYSYMBOL_YYEMPTY : YYTRANSLATE (yychar);
  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
      yyerror (sscript, YY_("syntax error"));
    }

  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
         error, discard it.  */

      if (yychar <= YYEOF)
        {
          /* Return failure if at end of input.  */
          if (yychar == YYEOF)
            YYABORT;
        }
      else
        {
          yydestruct ("Error: discarding",
                      yytoken, &yylval, sscript);
          yychar = YYEMPTY;
        }
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:
  /* Pacify compilers when the user code never invokes YYERROR and the
     label yyerrorlab therefore never appears in user code.  */
  if (0)
    YYERROR;
  ++yynerrs;

  /* Do not reclaim the symbols of the rule whose action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;      /* Each real token shifted decrements this.  */

  /* Pop stack until we find a state that shifts the error token.  */
  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
        {
          yyn += YYSYMBOL_YYerror;
          if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYSYMBOL_YYerror)
            {
              yyn = yytable[yyn];
              if (0 < yyn)
                break;
            }
        }

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
        YYABORT;


      yydestruct ("Error: popping",
                  YY_ACCESSING_SYMBOL (yystate), yyvsp, sscript);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", YY_ACCESSING_SYMBOL (yyn), yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturnlab;


/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturnlab;


/*-----------------------------------------------------------.
| yyexhaustedlab -- YYNOMEM (memory exhaustion) comes here.  |
`-----------------------------------------------------------*/
yyexhaustedlab:
  yyerror (sscript, YY_("memory exhausted"));
  yyresult = 2;
  goto yyreturnlab;


/*----------------------------------------------------------.
| yyreturnlab -- parsing is finished, clean up and return.  |
`----------------------------------------------------------*/
yyreturnlab:
  if (yychar != YYEMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval, sscript);
    }
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
                  YY_ACCESSING_SYMBOL (+*yyssp), yyvsp, sscript);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif

  return yyresult;
}

#line 1921 "sieve/sieve.y"



/*
 * Yacc actions
 */

void yyerror(sieve_script_t *sscript, const char *msg)
{
    if (sscript->ignore_err) return;

    sscript->err++;
    if (sscript->interp.err) {
        sscript->interp.err(sievelineno, msg, sscript->interp.interp_context,
                            sscript->script_context);
    }
}


static void
__attribute__((format(printf, 2, 0)))
vsieveerror_f(sieve_script_t *sscript,
              const char *fmt, va_list args)
{
    buf_reset(&sscript->sieveerr);
    buf_vprintf(&sscript->sieveerr, fmt, args);
    yyerror(sscript, buf_cstring(&sscript->sieveerr));
}

void
__attribute__((format(printf, 2, 3)))
sieveerror_f(sieve_script_t *sscript, const char *fmt, ...)
{
    va_list args;

    va_start(args, fmt);
    vsieveerror_f(sscript, fmt, args);
    va_end(args);
}

void sieveerror_c(sieve_script_t *sscript, int code, ...)
{
    va_list args;

    va_start(args, code);
    vsieveerror_f(sscript, error_message(code), args);
    va_end(args);
}

/*
 * variable-ref        =  "${" [namespace] variable-name "}"
 * namespace           =  identifier "." *sub-namespace
 * sub-namespace       =  variable-name "."
 * variable-name       =  num-variable / identifier
 * num-variable        =  1*DIGIT
 * identifier          =  (ALPHA / "_") *(ALPHA / DIGIT / "_")
 */
static int find_variables(sieve_script_t *sscript, char *s, int find_one)
{
    char *p = s;

    if (!supported(SIEVE_CAPA_VARIABLES)) return !find_one;

    while ((p = strstr(p, "${"))) {
        long num = 0, is_id = 0;

        p += 2;  /* skip over beginning of variable-ref */

        do {
            if (isdigit(*p)) {
                /* possible num-variable - get value and skip over digits */
                num = strtol(p, &p, 10);
            }
            else if (!find_one) {
                /* validating match variables - ignoroe identifiers */
                break;
            }
            else if (isalpha(*p) || *p == '_') {
                /* possible identifier - skip over identifier chars */
                for (++p; isalnum(*p) || *p == '_'; p++);
                is_id = 1;
            }
            else {
                /* not a valid variable-name */
                break;
            }

            if (*p == '}') {
                /* end of variable-ref */
                if (find_one) return 1;

                /* validating match variables */
                if (!is_id && num > MAX_MATCH_VARS) {
                    sieveerror_f(sscript, "string '%s':"
                                 " match variable index > %u unsupported",
                                 s, MAX_MATCH_VARS);
                    return 0;
                }
            }

        } while (is_id && *p == '.' && *(++p));  /* (sub-)namespace */
    }

    return !find_one;
}

static int chk_match_vars(sieve_script_t *sscript, char *s)
{
    return find_variables(sscript, s, 0 /* find_one */);
}

static int contains_variable(sieve_script_t *sscript, char *s)
{
    return find_variables(sscript, s, 1 /* find_one */);
}

/*
 * Valid UTF-8 check (from RFC 2640 Annex B.1)
 *
 * The following routine checks if a byte sequence is valid UTF-8. This
 * is done by checking for the proper tagging of the first and following
 * bytes to make sure they conform to the UTF-8 format. It then checks
 * to assure that the data part of the UTF-8 sequence conforms to the
 * proper range allowed by the encoding. Note: This routine will not
 * detect characters that have not been assigned and therefore do not
 * exist.
 */
static int verify_utf8(sieve_script_t *sscript, char *s)
{
    const char *buf = s;
    const char *endbuf = s + strlen(s);
    unsigned char byte2mask = 0x00, c;
    int trailing = 0;  /* trailing (continuation) bytes to follow */

    while (buf != endbuf) {
        c = *buf++;
        if (trailing) {
            if ((c & 0xC0) == 0x80) {           /* Does trailing byte
                                                   follow UTF-8 format? */
                if (byte2mask) {                /* Need to check 2nd byte
                                                   for proper range? */
                    if (c & byte2mask)          /* Are appropriate bits set? */
                        byte2mask = 0x00;
                    else
                        break;
                }
                trailing--;
            }
            else
                break;
        }
        else {
            if ((c & 0x80) == 0x00)             /* valid 1 byte UTF-8 */
                continue;
            else if ((c & 0xE0) == 0xC0)        /* valid 2 byte UTF-8 */
                if (c & 0x1E) {                 /* Is UTF-8 byte
                                                   in proper range? */
                    trailing = 1;
                }
                else
                    break;
            else if ((c & 0xF0) == 0xE0) {      /* valid 3 byte UTF-8 */
                if (!(c & 0x0F)) {              /* Is UTF-8 byte
                                                   in proper range? */
                    byte2mask = 0x20;           /* If not, set mask
                                                   to check next byte */
                }
                trailing = 2;
            }
            else if ((c & 0xF8) == 0xF0) {      /* valid 4 byte UTF-8 */
                if (!(c & 0x07)) {              /* Is UTF-8 byte
                                                   in proper range? */
                    byte2mask = 0x30;           /* If not, set mask
                                                   to check next byte */
                }
                trailing = 3;
            }
            else if ((c & 0xFC) == 0xF8) {      /* valid 5 byte UTF-8 */
                if (!(c & 0x03)) {              /* Is UTF-8 byte
                                                   in proper range? */
                    byte2mask = 0x38;           /* If not, set mask
                                                   to check next byte */
                }
                trailing = 4;
            }
            else if ((c & 0xFE) == 0xFC) {      /* valid 6 byte UTF-8 */
                if (!(c & 0x01)) {              /* Is UTF-8 byte
                                                   in proper range? */
                    byte2mask = 0x3C;           /* If not, set mask
                                                   to check next byte */
                }
                trailing = 5;
            }
            else
                break;
        }
    }

    if ((buf != endbuf) || trailing) {
        sieveerror_f(sscript, "string '%s': not valid utf8", s);
        return 0;
    }

    return 1;
}

static int verify_stringlist(sieve_script_t *sscript, strarray_t *sa,
                             int (*verify)(sieve_script_t*, char *))
{
    int i;

    for (i = 0 ; i < strarray_size(sa) ; i++) {
        if (!verify(sscript, (char *) strarray_nth(sa, i))) return 0;
    }
    return 1;
}

#ifdef ENABLE_REGEX
static int verify_regexlist(sieve_script_t *sscript,
                            const strarray_t *sa, int collation)
{
    int i, ret = 0;
    int cflags = REG_EXTENDED | REG_NOSUB;

    /* support UTF8 comparisons */
#if defined HAVE_PCREPOSIX_H
    cflags |= REG_UTF8;
#elif defined HAVE_PCRE2POSIX_H
    cflags |= REG_UTF;
#endif

    if (collation == B_ASCIICASEMAP) {
        cflags |= REG_ICASE;
    }

    for (i = 0 ; !ret && i < strarray_size(sa) ; i++) {
        const char *s = strarray_nth(sa, i);
        regex_t reg = {0};

        /* Don't try to validate a regex that includes variables */
        if (supported(SIEVE_CAPA_VARIABLES) && strstr(s, "${")) continue;

        /* Check if the regex will compile */
        if ((ret = regcomp(&reg, s, cflags)) != 0) {
            size_t errbuf_size = regerror(ret, &reg, NULL, 0);

            buf_reset(&sscript->sieveerr);
            buf_ensure(&sscript->sieveerr, errbuf_size);
            (void) regerror(ret, &reg,
                            (char *) buf_base(&sscript->sieveerr),
                            errbuf_size);
            buf_truncate(&sscript->sieveerr, errbuf_size);
            yyerror(sscript, buf_cstring(&sscript->sieveerr));
        }

        regfree(&reg);
    }

    return (ret == 0);
}
#else

static int verify_regexlist(sieve_script_t *sscript __attribute__((unused)),
                            const strarray_t *sa __attribute__((unused)),
                            char *comp __attribute__((unused)))
{
    return 0;
}
#endif /* ENABLE_REGEX */

static int verify_patternlist(sieve_script_t *sscript,
                              strarray_t *sa, comp_t *c,
                              int (*verify)(sieve_script_t*, char *))
{
    if (verify && !verify_stringlist(sscript, sa, verify)) return 0;

    canon_comptags(c, sscript);

    return (c->match == B_REGEX) ?
        verify_regexlist(sscript, sa, c->collation) : 1;
}

static int verify_address(sieve_script_t *sscript, char *s)
{
    if (contains_variable(sscript, s)) return 1;

    YY_BUFFER_STATE buffer = addr_scan_string(s);
    int r = 1;

    sscript->addrerr[0] = '\0';    /* paranoia */
    if (addrparse(sscript)) {
        sieveerror_f(sscript, "address '%s': %s", s, sscript->addrerr);
        r = 0;
    }
    addr_delete_buffer(buffer);

    return r;
}

static int verify_mailbox(sieve_script_t *sscript, char *s)
{
    if (!verify_utf8(sscript, s)) {
        sieveerror_f(sscript, "mailbox '%s': not a valid mailbox", s);
        return 0;
    }

    return 1;
}

static int verify_header(sieve_script_t *sscript, char *hdr)
{
    char *h = hdr;

    while (*h) {
        /* field-name      =       1*ftext
           ftext           =       %d33-57 / %d59-126
           ; Any character except
           ;  controls, SP, and
           ;  ":". */
        if (!((*h >= 33 && *h <= 57) || (*h >= 59 && *h <= 126))) {
            sieveerror_f(sscript, "header '%s': not a valid header", hdr);
            return 0;
        }
        h++;
    }
    return 1;
}

static int verify_addrheader(sieve_script_t *sscript, char *hdr)
{
    const char **h, *hdrs[] = {
        "from", "sender", "reply-to",   /* RFC 5322 originator fields */
        "to", "cc", "bcc",              /* RFC 5322 destination fields */
        "message-id", "in-reply-to",    /* RFC 5322 identification fields */
        "references",
        "resent-from", "resent-sender", /* RFC 5322 resent fields */
        "resent-to", "resent-cc", "resent-bcc",
        "return-path",                  /* RFC 5322 trace fields */
        "disposition-notification-to",  /* RFC 8098 MDN request fields */
        "approved",                     /* RFC 5536 moderator/control fields */
        "delivered-to",                 /* non-standard (loop detection) */
        NULL
    };

    if (contains_variable(sscript, hdr)) return 1;

    if (!config_getswitch(IMAPOPT_RFC3028_STRICT))
        return verify_header(sscript, hdr);

    for (lcase(hdr), h = hdrs; *h; h++) {
        if (!strcmp(*h, hdr)) return 1;
    }

    sieveerror_f(sscript,
                 "header '%s': not a valid header for an address test", hdr);
    return 0;
}

static int verify_envelope(sieve_script_t *sscript, char *env)
{
    if (contains_variable(sscript, env)) return 1;

    lcase(env);
    if (!config_getswitch(IMAPOPT_RFC3028_STRICT) ||
        !strcmp(env, "from") || !strcmp(env, "to") || !strcmp(env, "auth")) {
        return 1;
    }

    sieveerror_f(sscript,
                 "env-part '%s': not a valid part for an envelope test", env);
    return 0;
}

static int verify_list(sieve_script_t *sscript, char *s)
{
    if (sscript->interp.isvalidlist &&
        sscript->interp.isvalidlist(sscript->interp.interp_context, s)
        != SIEVE_OK) {
        sieveerror_f(sscript, "list '%s': is not valid/supported", s);
        return 0;
    }

    return 1;
}

static int check_reqs(sieve_script_t *sscript, strarray_t *sa)
{
    char *s;
    struct buf *errs = &sscript->sieveerr;
    int ret = 1, sep = ':';

    buf_setcstr(errs, "Unsupported feature(s) in \"require\"");
    while ((s = strarray_shift(sa))) {
        if (!script_require(sscript, s)) {
            buf_printf(errs, "%c \"%s\"", sep, s);
            ret = 0;
            sep = ',';
        }
        free(s);
    }
    strarray_free(sa);

    if (ret == 0) yyerror(sscript, buf_cstring(&sscript->sieveerr));
    else if (supported(SIEVE_CAPA_IHAVE)) {
        /* mark all allowed extensions as supported */
        sscript->support |= (SIEVE_CAPA_ALL & ~SIEVE_CAPA_IHAVE_INCOMPAT);
    }

    encoded_char = supported(SIEVE_CAPA_ENCODED_CHAR);

    return ret;
}

/* take a format string and a variable list of arguments
   and populate the command arguments array */
static unsigned bc_precompile(cmdarg_t args[], const char *fmt, ...)
{
    va_list ap;
    unsigned n = 0;

    va_start(ap, fmt);
    for (n = 0; *fmt; fmt++, n++) {
        args[n].type = *fmt;

        switch (*fmt) {
        case 'i':
            args[n].u.i = va_arg(ap, int);
            break;
        case 's':
            args[n].u.s = va_arg(ap, const char *);
            break;
        case 'S':
            args[n].u.sa = va_arg(ap, const strarray_t *);
            break;
        case 'U':
            args[n].u.ua = va_arg(ap, const arrayu64_t *);
            break;
        case 't':
            args[n].u.t = va_arg(ap, const test_t *);
            break;
        case 'T':
            args[n].u.tl = va_arg(ap, const testlist_t *);
            break;
        case 'C': {
            /* Expand the comparator into its component parts */
            const comp_t *c = va_arg(ap, const comp_t *);

            args[n].type = AT_INT;
            args[n].u.i = c->match;
            args[++n].type = AT_INT;
            args[n].u.i = c->relation;
            args[++n].type = AT_INT;
            args[n].u.i = c->collation;
            break;
        }
        case 'Z': {
            /* Expand the zone into its component parts */
            const zone_t *z = va_arg(ap, const zone_t *);

            args[n].type = AT_INT;
            args[n].u.i = z->tag;

            if (z->tag == B_TIMEZONE) {
                args[++n].type = AT_STR;
                args[n].u.s = z->offset;
            }
            break;
        }
        default: assert(*fmt);
        }
    }
    va_end(ap);

    return n;
}

static commandlist_t *build_keep(sieve_script_t *sscript, commandlist_t *c)
{
    assert(c && c->type == B_KEEP);

    if (c->u.k.flags && !_verify_flaglist(c->u.k.flags)) {
        strarray_add(c->u.k.flags, "");
    }

    c->nargs = bc_precompile(c->args, "S", c->u.k.flags);

    return c;
}

static commandlist_t *build_fileinto(sieve_script_t *sscript,
                                     commandlist_t *c, char *folder)
{
    assert(c && c->type == B_FILEINTO);

    if (c->u.f.flags && !_verify_flaglist(c->u.f.flags)) {
        strarray_add(c->u.f.flags, "");
    }
    verify_mailbox(sscript, folder);
    c->u.f.t.folder = folder;

    c->nargs = bc_precompile(c->args, "ssiSis",
                             c->u.f.t.mailboxid,
                             c->u.f.t.specialuse,
                             c->u.f.create,
                             c->u.f.flags,
                             c->u.f.copy,
                             c->u.f.t.folder);

    return c;
}

static commandlist_t *build_redirect(sieve_script_t *sscript,
                                     commandlist_t *c, char *address)
{
    assert(c && c->type == B_REDIRECT);

    if (c->u.r.list) verify_list(sscript, address);
    else verify_address(sscript, address);

    /* Verify DELIVERBY values */
    if (c->u.r.bytime) {
        if (!supported(SIEVE_CAPA_VARIABLES)) {
            time_t t;

            if (c->u.r.bytime[0] != '+' &&
                time_from_iso8601(c->u.r.bytime, &t) == -1) {
                sieveerror_f(sscript,
                             "string '%s': not a valid DELIVERBY time value",
                             c->u.r.bytime);
            }
            if (c->u.r.bymode &&
                strcasecmp(c->u.r.bymode, "NOTIFY") &&
                strcasecmp(c->u.r.bymode, "RETURN")) {
                sieveerror_f(sscript,
                             "string '%s': not a valid DELIVERBY mode value",
                             c->u.r.bymode);
            }
        }
    }
    else if (c->u.r.bymode || c->u.r.bytrace) {
        sieveerror_c(sscript, SIEVE_MISSING_TAG,
                     ":bytimerelative OR :bytimeabsolute");
    }

    /* Verify DSN NOTIFY value(s) */
    if (c->u.r.dsn_notify && !supported(SIEVE_CAPA_VARIABLES)) {
        tok_t tok =
            TOK_INITIALIZER(c->u.r.dsn_notify, ",", TOK_TRIMLEFT|TOK_TRIMRIGHT);
        char *token;
        int never = 0;

        while ((token = tok_next(&tok))) {
            if (!strcasecmp(token, "NEVER")) never = 1;
            else if (never) {
                sieveerror_f(sscript,
                             "DSN NOTIFY value 'NEVER' MUST be used by itself");
                break;
            }
            else if (strcasecmp(token, "SUCCESS") &&
                     strcasecmp(token, "FAILURE") &&
                     strcasecmp(token, "DELAY")) {
                sieveerror_f(sscript,
                             "string '%s': not a valid DSN NOTIFY value",
                             token);
                break;
            }
        }
        tok_fini(&tok);
    }

    /* Verify DSN RET value */
    if (c->u.r.dsn_ret && !supported(SIEVE_CAPA_VARIABLES) &&
        strcasecmp(c->u.r.dsn_ret, "FULL") &&
        strcasecmp(c->u.r.dsn_ret, "HDRS")) {
        sieveerror_f(sscript, "string '%s': not a valid DSN RET value",
                     c->u.r.dsn_ret);
    }

    c->u.r.address = address;

    c->nargs = bc_precompile(c->args, "ssissiis",
                             c->u.r.bytime,
                             c->u.r.bymode,
                             c->u.r.bytrace,
                             c->u.r.dsn_notify,
                             c->u.r.dsn_ret,
                             c->u.r.list,
                             c->u.r.copy,
                             c->u.r.address);

    return c;
}

static int verify_identifier(sieve_script_t *sscript, char *s)
{
    /* identifier         = (ALPHA / "_") *(ALPHA / DIGIT / "_") */

    if (!is_identifier(s)) {
        sieveerror_f(sscript,
                     "string '%s': not a valid sieve identifier", s);
        return 0;
    }
    return 1;
}

static commandlist_t *build_set(sieve_script_t *sscript,
                                commandlist_t *c, char *variable, char *value)
{
    assert(c && c->type == B_SET);

    verify_identifier(sscript, variable);
    verify_utf8(sscript, value);

    c->u.s.variable = variable;
    c->u.s.value = value;

    c->nargs = bc_precompile(c->args, "iss",
                             c->u.s.modifiers,
                             c->u.s.variable,
                             c->u.s.value);

    return c;
}

static commandlist_t *build_vacation(sieve_script_t *sscript,
                                     commandlist_t *c, char *message)
{
    int min = sscript->interp.vacation->min_response;
    int max = sscript->interp.vacation->max_response;

    assert(c && c->type == B_VACATION);

    if (c->u.v.handle) verify_utf8(sscript, c->u.v.handle);
    if (c->u.v.subject) verify_utf8(sscript, c->u.v.subject);
    if (c->u.v.from) verify_address(sscript, c->u.v.from);
    if (c->u.v.addresses)
        verify_stringlist(sscript, c->u.v.addresses, verify_address);
    if (c->u.v.mime == -1) {
        verify_utf8(sscript, message);
        c->u.v.mime = 0;
    }
    if (c->u.v.fcc.t.folder) {
        verify_mailbox(sscript, c->u.v.fcc.t.folder);
        if (c->u.v.fcc.flags && !_verify_flaglist(c->u.v.fcc.flags)) {
            strarray_add(c->u.v.fcc.flags, "");
        }
    }
    else if (c->u.v.fcc.create || c->u.v.fcc.flags ||
             c->u.v.fcc.t.specialuse || c->u.v.fcc.t.mailboxid) {
        sieveerror_c(sscript, SIEVE_MISSING_TAG, ":fcc");
    }

    c->u.v.message = message;

    if (c->u.v.seconds == -1) c->u.v.seconds = 7 * DAY2SEC;
    if (c->u.v.seconds < min) c->u.v.seconds = min;
    if (c->u.v.seconds > max) c->u.v.seconds = max;

    c->nargs = bc_precompile(c->args,
                             c->u.v.fcc.t.folder ? "SssiisssiSss" : "Sssiisss",
                             c->u.v.addresses,
                             c->u.v.subject,
                             c->u.v.message,
                             c->u.v.seconds,
                             c->u.v.mime,
                             c->u.v.from,
                             c->u.v.handle,
                             c->u.v.fcc.t.folder,
                             c->u.v.fcc.create,
                             c->u.v.fcc.flags,
                             c->u.v.fcc.t.specialuse,
                             c->u.v.fcc.t.mailboxid);

    return c;
}

static commandlist_t *build_flag(sieve_script_t *sscript,
                                 commandlist_t *c, strarray_t *flags)
{
    assert(c && (c->type == B_SETFLAG ||
                 c->type == B_ADDFLAG ||
                 c->type == B_REMOVEFLAG));

    if (!_verify_flaglist(flags)) {
        strarray_add(flags, "");
    }
    c->u.fl.flags = flags;

    if (!c->u.fl.variable) c->u.fl.variable = xstrdup("");
    else if (!is_identifier(c->u.fl.variable)) {
        sieveerror_c(sscript, SIEVE_INVALID_VALUE, "variablename");
    }

    c->nargs = bc_precompile(c->args, "sS",
                             c->u.fl.variable,
                             c->u.fl.flags);

    return c;
}

static commandlist_t *build_addheader(sieve_script_t *sscript,
                                      commandlist_t *c, char *name, char *value)
{
    assert(c && c->type == B_ADDHEADER);

    verify_header(sscript, name);
    verify_utf8(sscript, value);

    if (c->u.ah.index == 0) c->u.ah.index = 1;
    c->u.ah.name = name;
    c->u.ah.value = value;

    c->nargs = bc_precompile(c->args, "iss",
                             c->u.ah.index,
                             c->u.ah.name,
                             c->u.ah.value);

    return c;
}

static commandlist_t *build_deleteheader(sieve_script_t *sscript,
                                         commandlist_t *c,
                                         char *name, strarray_t *values)
{
    assert(c && c->type == B_DELETEHEADER);

    if (!strcasecmp("Received", name) || !strcasecmp("Auto-Submitted", name)) {
        sieveerror_f(sscript,
                     "MUST NOT delete Received or Auto-Submitted headers");
    }
    else if (c->u.dh.comp.index == INT_MIN) {
        sieveerror_c(sscript, SIEVE_MISSING_TAG, ":index");
    }

    verify_header(sscript, name);
    verify_patternlist(sscript, values, &c->u.dh.comp, verify_utf8);

    c->u.dh.name = name;
    c->u.dh.values = values;

    c->nargs = bc_precompile(c->args, "iCsS",
                             c->u.dh.comp.index,
                             &c->u.dh.comp,
                             c->u.dh.name,
                             c->u.dh.values);

    return c;
}

static int verify_weekday(sieve_script_t *sscript, char *day)
{
    int n = day[0] - '0';

    if (n >= 0 && n <= 6 && day[1] == '\0') {
        return (1 << n);
    }

    sieveerror_f(sscript, "'%s': not a valid weekday for snooze", day);
    return 0;
}

static int verify_time(sieve_script_t *sscript, char *time)
{
    struct tm tm;
    int t = 0;
    char *r = strptime(time, "%T", &tm);

    if (r && *r == '\0') {
        t = 3600 * tm.tm_hour + 60 * tm.tm_min + tm.tm_sec;
    }
    else {
        sieveerror_f(sscript, "'%s': not a valid time for snooze", time);
    }

    free(time);  /* done with this string */

    return t;
}

static commandlist_t *build_snooze(sieve_script_t *sscript,
                                   commandlist_t *c, arrayu64_t *times)
{
    assert(c && c->type == B_SNOOZE);

    if (c->u.sn.f.t.folder) verify_mailbox(sscript, c->u.sn.f.t.folder);
    if (c->u.sn.addflags && !_verify_flaglist(c->u.sn.addflags)) {
        strarray_add(c->u.sn.addflags, "");
    }
    if (c->u.sn.removeflags && !_verify_flaglist(c->u.sn.removeflags)) {
        strarray_add(c->u.sn.removeflags, "");
    }
    if (!c->u.sn.days) c->u.sn.days = 0x7f; /* all days */

    /* Sort times earliest -> latest */
    arrayu64_sort(times, NULL/*ascending*/);
    c->u.sn.times = times;

    c->nargs = bc_precompile(c->args, "sssiSSisU",
                             c->u.sn.f.t.folder,
                             c->u.sn.f.t.mailboxid,
                             c->u.sn.f.t.specialuse,
                             c->u.sn.f.create,
                             c->u.sn.addflags,
                             c->u.sn.removeflags,
                             c->u.sn.days,
                             c->u.sn.tzid,
                             c->u.sn.times);

    return c;
}

static commandlist_t *build_rej_err(sieve_script_t *sscript,
                                    int t, char *message)
{
    commandlist_t *c;

    assert(t == B_REJECT || t == B_EREJECT || t == B_ERROR);

    verify_utf8(sscript, message);

    c = new_command(t, sscript);
    c->u.str = message;

    c->nargs = bc_precompile(c->args, "s", c->u.str);

    return c;
}

static commandlist_t *build_notify(sieve_script_t *sscript,
                                   commandlist_t *c, char *method)
{
    assert(c && c->type == B_ENOTIFY);

    if (c->u.n.fcc.t.folder) {
        verify_mailbox(sscript, c->u.n.fcc.t.folder);
        if (c->u.n.fcc.flags && !_verify_flaglist(c->u.n.fcc.flags)) {
            strarray_add(c->u.n.fcc.flags, "");
        }
    }
    else if (c->u.n.fcc.create || c->u.n.fcc.flags ||
             c->u.n.fcc.t.specialuse || c->u.n.fcc.t.mailboxid) {
        sieveerror_c(sscript, SIEVE_MISSING_TAG, ":fcc");
    }

    c->u.n.method = method;

    if (c->u.n.priority == -1) c->u.n.priority = B_NORMAL;
    if (!c->u.n.message) c->u.n.message = xstrdup("$from$: $subject$");

    c->nargs = bc_precompile(c->args, "ssSis",
                             c->u.n.method,
                             c->u.n.from,
                             c->u.n.options,
                             c->u.n.priority,
                             c->u.n.message);

    return c;
}

static commandlist_t *build_include(sieve_script_t *sscript,
                                    commandlist_t *c, char *script)
{
    assert(c && c->type == B_INCLUDE);

    if (strchr(script, '/')) {
        sieveerror_c(sscript, SIEVE_INVALID_VALUE, "script-name");
    }

    c->u.inc.script = script;
    if (c->u.inc.once == -1) c->u.inc.once = 0;
    if (c->u.inc.location == -1) c->u.inc.location = B_PERSONAL;
    if (c->u.inc.optional == -1) c->u.inc.optional = 0;

    c->nargs = bc_precompile(c->args, "is",
                             c->u.inc.location |
                                 (c->u.inc.once | c->u.inc.optional),
                             c->u.inc.script);

    return c;
}

static commandlist_t *build_log(sieve_script_t *sscript, char *text)
{
    commandlist_t *c;

    verify_utf8(sscript, text);

    c = new_command(B_LOG, sscript);
    c->u.l.text = text;

    c->nargs = bc_precompile(c->args, "s", c->u.l.text);

    return c;
}

static commandlist_t *build_imip(sieve_script_t *sscript, commandlist_t *c)
{
    unsigned flags = 0;

    assert(c && c->type == B_PROCESSIMIP);

    if (c->u.imip.invites_only) flags |= IMIP_INVITESONLY;
    if (c->u.imip.updates_only) flags |= IMIP_UPDATESONLY;
    if (c->u.imip.delete_canceled) flags |= IMIP_DELETECANCELED;

    if (c->u.imip.outcome_var) verify_identifier(sscript, c->u.imip.outcome_var);
    if (c->u.imip.errstr_var) verify_identifier(sscript, c->u.imip.errstr_var);
    
    c->nargs = bc_precompile(c->args, "isss",
                             flags,
                             c->u.imip.calendarid,
                             c->u.imip.outcome_var,
                             c->u.imip.errstr_var);

    return c;
}

static commandlist_t *build_ikeep_target(sieve_script_t *sscript,
                                         commandlist_t *c, char *folder)
{
    assert(c && c->type == B_IKEEP_TARGET);

    verify_mailbox(sscript, folder);
    c->u.ikt.folder = folder;

    c->nargs = bc_precompile(c->args, "sss",
                             c->u.ikt.mailboxid,
                             c->u.ikt.specialuse,
                             c->u.ikt.folder);

    return c;
}

static test_t *build_anyof(sieve_script_t *sscript, testlist_t *tl)
{
    test_t *t;

    assert(tl);

    if (tl->next == NULL) {
        /* collapse single item list into a simple test */
        t = tl->t;
        free(tl);
    }
    else {
        test_t *fail = NULL, *maybe = NULL;

        /* create ANYOF test */
        t = new_test(BC_ANYOF, sscript);
        t->u.tl = tl;

        /* find first test that did/didn't set ignore_err */
        for ( ; tl && !fail && !maybe; tl = tl->next) {
            if (tl->t->ignore_err) {
                if (!fail) fail = tl->t;
            }
            else if (!maybe) maybe = tl->t;
        }

        if (fail) {
            if (maybe) {
                /* test may succeed - backout ignore_err */
                sscript->ignore_err = --fail->ignore_err;
            }
            else {
                /* test will fail - revert ignore_err to first value */
                sscript->ignore_err = t->ignore_err = fail->ignore_err;
            }
        }

        t->nargs = bc_precompile(t->args, "T", t->u.tl);
    }

    return t;
}

static test_t *build_allof(sieve_script_t *sscript, testlist_t *tl)
{
    test_t *t;

    assert(tl);

    if (tl->next == NULL) {
        /* collapse single item list into a simple test */
        t = tl->t;
        free(tl);
    }
    else {
        /* create ALLOF test */
        t = new_test(BC_ALLOF, sscript);
        t->u.tl = tl;

        /* find first test that set ignore_err and revert to that value */
        for ( ; tl; tl = tl->next) {
            if (tl->t->ignore_err) {
                sscript->ignore_err = t->ignore_err = tl->t->ignore_err;
                break;
            }
        }

        t->nargs = bc_precompile(t->args, "T", t->u.tl);
    }

    return t;
}

static test_t *build_not(sieve_script_t *sscript, test_t *t)
{
    test_t *n;

    assert(t);

    if (t->ignore_err) {
        /* test will succeed - backout ignore_err */
        sscript->ignore_err = --t->ignore_err;
    }

    n = new_test(BC_NOT, sscript);
    n->u.t = t;

    n->nargs = bc_precompile(n->args, "t", n->u.t);

    return n;
}

static test_t *build_hhs(sieve_script_t *sscript, test_t *t,
                         strarray_t *sl, strarray_t *pl)
{
    assert(t);

    verify_patternlist(sscript, pl, &t->u.hhs.comp, verify_utf8);

    t->u.hhs.sl = sl;
    t->u.hhs.pl = pl;

    t->nargs += bc_precompile(t->args + t->nargs, "CSS",
                              &t->u.hhs.comp,
                              t->u.hhs.sl,
                              t->u.hhs.pl);

    return t;
}

static test_t *build_header(sieve_script_t *sscript, test_t *t,
                            strarray_t *sl, strarray_t *pl)
{
    assert(t && t->type == BC_HEADER);

    if (t->u.hhs.comp.index == INT_MIN) {
        sieveerror_c(sscript, SIEVE_MISSING_TAG, ":index");
    }

    verify_stringlist(sscript, sl, verify_header);

    t->nargs = bc_precompile(t->args, "i", t->u.hhs.comp.index);

    return build_hhs(sscript, t, sl, pl);
}

static test_t *build_stringt(sieve_script_t *sscript, test_t *t,
                             strarray_t *sl, strarray_t *pl)
{
    assert(t && t->type == BC_STRING);

    verify_stringlist(sscript, sl, verify_utf8);

    return build_hhs(sscript, t, sl, pl);
}

static test_t *build_hasflag(sieve_script_t *sscript, test_t *t,
                             strarray_t *sl, strarray_t *pl)
{
    assert(t && t->type == BC_HASFLAG);

    if (sl) {
        if (!supported(SIEVE_CAPA_VARIABLES)) {
            sieveerror_c(sscript, SIEVE_MISSING_REQUIRE, "variables");
        }

        verify_stringlist(sscript, sl, verify_identifier);
    }

    return build_hhs(sscript, t, sl, pl);
}

static test_t *build_ae(sieve_script_t *sscript, test_t *t,
                        strarray_t *sl, strarray_t *pl)
{
    assert(t);

    verify_patternlist(sscript, pl, &t->u.ae.comp, NULL);

    if (t->u.ae.addrpart == -1) t->u.ae.addrpart = B_ALL;
    t->u.ae.sl = sl;
    t->u.ae.pl = pl;

    t->nargs += bc_precompile(t->args + t->nargs, "CiSS",
                              &t->u.ae.comp,
                              t->u.ae.addrpart,
                              t->u.ae.sl,
                              t->u.ae.pl);

    return t;
}

static test_t *build_address(sieve_script_t *sscript, test_t *t,
                             strarray_t *sl, strarray_t *pl)
{
    assert(t && t->type == BC_ADDRESS);

    if (t->u.ae.comp.index == INT_MIN) {
        sieveerror_c(sscript, SIEVE_MISSING_TAG, ":index");
    }

    verify_stringlist(sscript, sl, verify_addrheader);

    t->nargs = bc_precompile(t->args, "i", t->u.ae.comp.index);

    return build_ae(sscript, t, sl, pl);
}

static test_t *build_envelope(sieve_script_t *sscript, test_t *t,
                              strarray_t *sl, strarray_t *pl)
{
    assert(t && t->type == BC_ENVELOPE);

    verify_stringlist(sscript, sl, verify_envelope);

    return build_ae(sscript, t, sl, pl);
}

static test_t *build_body(sieve_script_t *sscript,
                          test_t *t, strarray_t *pl)
{
    assert(t && (t->type == BC_BODY));

    verify_patternlist(sscript, pl, &t->u.b.comp, verify_utf8);

    if (t->u.b.offset == -1) t->u.b.offset = 0;
    if (t->u.b.transform == -1) t->u.b.transform = B_TEXT;
    if (t->u.b.content_types == NULL) {
        t->u.b.content_types = strarray_new();
        strarray_append(t->u.b.content_types,
                        (t->u.b.transform == B_RAW) ? "" : "text");
    }
    t->u.b.pl = pl;

    t->nargs = bc_precompile(t->args, "CiiSS",
                             &t->u.b.comp,
                             t->u.b.transform,
                             t->u.b.offset,
                             t->u.b.content_types,
                             t->u.b.pl);

    return t;
}

static const struct {
    const char *name;
    unsigned bytecode;
} date_parts[] = {
    { "year",    B_YEAR    },
    { "month",   B_MONTH   },
    { "day",     B_DAY     },
    { "date",    B_DATE    },
    { "julian",  B_JULIAN  },
    { "hour",    B_HOUR    },
    { "minute",  B_MINUTE  },
    { "second",  B_SECOND  },
    { "time",    B_TIME    },
    { "iso8601", B_ISO8601 },
    { "std11",   B_STD11   },
    { "zone",    B_ZONE    },
    { "weekday", B_WEEKDAY },
    { NULL,      0         }
};

static int verify_date_part(sieve_script_t *sscript, char *part)
{
    int i;

    for (i = 0; date_parts[i].name && strcasecmp(date_parts[i].name, part); i++);

    if (!date_parts[i].name) {
        sieveerror_f(sscript, "invalid date-part '%s'", part);
    }

    return date_parts[i].bytecode;
}

static test_t *build_date(sieve_script_t *sscript,
                          test_t *t, char *hn, char *part, strarray_t *kl)
{
    assert(t && (t->type == BC_DATE || t->type == BC_CURRENTDATE));

    if (hn) verify_header(sscript, hn);
    verify_patternlist(sscript, kl, &t->u.dt.comp, NULL);

    if (t->u.dt.comp.index == 0) {
        t->u.dt.comp.index = 1;
    }
    else if (t->u.dt.comp.index == INT_MIN) {
        sieveerror_c(sscript, SIEVE_MISSING_TAG, ":index");
    }

    if (t->u.dt.zone.tag == -1) {
        t->u.dt.zone.tag = B_TIMEZONE;
    }

    t->u.dt.date_part = verify_date_part(sscript, part);
    free(part); /* done with this string */
    t->u.dt.header_name = hn;
    t->u.dt.kl = kl;

    if (t->type == BC_DATE) {
        t->nargs = bc_precompile(t->args, "iZCisS",
                                 t->u.dt.comp.index,
                                 &t->u.dt.zone,
                                 &t->u.dt.comp,
                                 t->u.dt.date_part,
                                 t->u.dt.header_name,
                                 t->u.dt.kl);
    }
    else {
        t->nargs = bc_precompile(t->args, "ZCiS",
                                 &t->u.dt.zone,
                                 &t->u.dt.comp,
                                 t->u.dt.date_part,
                                 t->u.dt.kl);
    }

    return t;
}

static test_t *build_ihave(sieve_script_t *sscript, strarray_t *sa)
{
    test_t *t;
    int i;

    t = new_test(BC_IHAVE, sscript);
    t->u.sl = sa;

    /* check if we support all listed extensions */
    for (i = 0; i < strarray_size(sa); i++) {
        unsigned long long capa = lookup_capability(strarray_nth(sa, i));

        if (!capa) {
            /* need to start ignoring errors immediately in case this ihave
               is part of a testlist with an unknown test later in the list */
            if (!t->ignore_err) t->ignore_err = ++sscript->ignore_err;
        }
        else if (capa & SIEVE_CAPA_IHAVE_INCOMPAT) {
            /* incompatible extension used in ihave - parse error */
            sscript->ignore_err = 0;
            sieveerror_c(sscript, SIEVE_IHAVE_INCOMPAT, strarray_nth(sa, i));
            break;
        }
    }

    t->nargs = bc_precompile(t->args, "S", t->u.sl);

    return t;
}

static test_t *build_mbox_meta(sieve_script_t *sscript,
                               test_t *t, char *extname,
                               char *keyname, strarray_t *keylist)
{
    assert(t);

    canon_comptags(&t->u.mm.comp, sscript);
    t->u.mm.extname = extname;
    t->u.mm.keyname = keyname;
    t->u.mm.keylist = keylist;

    switch (t->type) {
    case BC_MAILBOXEXISTS:
    case BC_MAILBOXIDEXISTS:
    case BC_SERVERMETADATAEXISTS:
        t->nargs = bc_precompile(t->args, "S",
                                 t->u.mm.keylist);
        break;

    case BC_METADATAEXISTS:
    case BC_SPECIALUSEEXISTS:
        t->nargs = bc_precompile(t->args, "sS",
                                 t->u.mm.extname,
                                 t->u.mm.keylist);
        break;

    case BC_SERVERMETADATA:
    case BC_ENVIRONMENT:
        t->nargs = bc_precompile(t->args, "CsS",
                                 &t->u.mm.comp,
                                 t->u.mm.keyname,
                                 t->u.mm.keylist);
        break;

    case BC_METADATA:
    case BC_NOTIFYMETHODCAPABILITY:
        t->nargs = bc_precompile(t->args, "CssS",
                                 &t->u.mm.comp,
                                 t->u.mm.extname,
                                 t->u.mm.keyname,
                                 t->u.mm.keylist);
        break;

    default:
        assert(0);
        break;
    }

    return t;
}

static test_t *build_duplicate(sieve_script_t *sscript, test_t *t)
{
    assert(t && t->type == BC_DUPLICATE);

    switch (t->u.dup.idtype) {
    case B_HEADER:
        verify_header(sscript, t->u.dup.idval);
        break;

    case B_UNIQUEID:
        verify_utf8(sscript, t->u.dup.idval);
        break;

    default:
        t->u.dup.idtype = B_HEADER;
        t->u.dup.idval = xstrdup("Message-ID");
        break;
    }

    if (!t->u.dup.handle) t->u.dup.handle = xstrdup("");
    else verify_utf8(sscript, t->u.dup.handle);

    if (t->u.dup.seconds == -1) t->u.dup.seconds = 7 * 86400; /* 7 days */

    t->nargs = bc_precompile(t->args, "issii",
                             t->u.dup.idtype,
                             t->u.dup.idval,
                             t->u.dup.handle,
                             t->u.dup.seconds,
                             t->u.dup.last);

    return t;
}

#ifdef WITH_JMAP
#include "imap/jmap_api.h"
#include "imap/jmap_mail_query_parse.h"

struct filter_rock {
    strarray_t *path;
    unsigned is_invalid;
};

static void filter_parser_invalid(const char *field, void *rock)
{
    struct filter_rock *frock = (struct filter_rock *) rock;

    if (!frock->is_invalid++) strarray_push(frock->path, field);
}

static void filter_parser_push_index(const char *field, size_t index,
                                     const char *name, void *rock)
{
    struct filter_rock *frock = (struct filter_rock *) rock;
    struct buf buf = BUF_INITIALIZER;

    if (!frock->is_invalid) {
        if (name) buf_printf(&buf, "%s[%zu:%s]", field, index, name);
        else buf_printf(&buf, "%s[%zu]", field, index);
        strarray_pushm(frock->path, buf_release(&buf));
    }
}

static void filter_parser_pop(void *rock)
{
    struct filter_rock *frock = (struct filter_rock *) rock;

    if (!frock->is_invalid) free(strarray_pop(frock->path));
}

static test_t *build_jmapquery(sieve_script_t *sscript, test_t *t, char *json)
{
    json_t *jquery;
    json_error_t jerr;

    assert(t && t->type == BC_JMAPQUERY);

    /* validate query */
    jquery = json_loads(json, 0, &jerr);
    if (jquery) {
        strarray_t path = STRARRAY_INITIALIZER;
        strarray_t capabilities = STRARRAY_INITIALIZER;
        struct filter_rock frock = { &path, 0 };
        jmap_email_filter_parse_ctx_t parse_ctx = {
            NULL,
            &filter_parser_invalid,
            &filter_parser_push_index,
            &filter_parser_pop,
            &capabilities,
            &frock
        };

        strarray_append(&capabilities, JMAP_URN_MAIL);
        strarray_append(&capabilities, JMAP_MAIL_EXTENSION);

        jmap_email_filter_parse(jquery, &parse_ctx);

        if (frock.is_invalid) {
            char *s = strarray_join(&path, "/");

            sieveerror_f(sscript, "invalid jmapquery argument: '%s'", s);
            free(s);
        }

        strarray_fini(&path);
        strarray_fini(&capabilities);
    }
    else {
        sieveerror_f(sscript, "invalid jmapquery format");
    }

    t->u.jquery = json_dumps(jquery, JSON_COMPACT);
    json_decref(jquery);

    free(json);  /* done with this string */

    t->nargs = bc_precompile(t->args, "s", t->u.jquery);

    return t;
}
#else
static test_t *build_jmapquery(sieve_script_t *sscript, test_t *t, char *json)
{
    sieveerror_c(sscript, SIEVE_UNSUPP_EXT, "vnd.cyrus.jmapquery");

    free(json);  /* done with this string */

    return t;
}
#endif /* WITH_JMAP */
