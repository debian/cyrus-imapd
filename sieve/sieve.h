/* A Bison parser, made by GNU Bison 3.8.2.  */

/* Bison interface for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015, 2018-2021 Free Software Foundation,
   Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* DO NOT RELY ON FEATURES THAT ARE NOT DOCUMENTED in the manual,
   especially those whose name start with YY_ or yy_.  They are
   private implementation details that can be changed or removed.  */

#ifndef YY_SIEVE_SIEVE_SIEVE_H_INCLUDED
# define YY_SIEVE_SIEVE_SIEVE_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int sievedebug;
#endif

/* Token kinds.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    YYEMPTY = -2,
    YYEOF = 0,                     /* "end of file"  */
    YYerror = 256,                 /* error  */
    YYUNDEF = 257,                 /* "invalid token"  */
    NUMBER = 258,                  /* NUMBER  */
    STRING = 259,                  /* STRING  */
    IF = 260,                      /* IF  */
    ELSIF = 261,                   /* ELSIF  */
    ELSE = 262,                    /* ELSE  */
    REQUIRE = 263,                 /* REQUIRE  */
    STOP = 264,                    /* STOP  */
    DISCARD = 265,                 /* DISCARD  */
    KEEP = 266,                    /* KEEP  */
    FILEINTO = 267,                /* FILEINTO  */
    REDIRECT = 268,                /* REDIRECT  */
    ANYOF = 269,                   /* ANYOF  */
    ALLOF = 270,                   /* ALLOF  */
    EXISTS = 271,                  /* EXISTS  */
    NOT = 272,                     /* NOT  */
    SFALSE = 273,                  /* SFALSE  */
    STRUE = 274,                   /* STRUE  */
    SIZE = 275,                    /* SIZE  */
    HEADERT = 276,                 /* HEADERT  */
    ADDRESS = 277,                 /* ADDRESS  */
    ENVELOPE = 278,                /* ENVELOPE  */
    COMPARATOR = 279,              /* COMPARATOR  */
    OVER = 280,                    /* OVER  */
    UNDER = 281,                   /* UNDER  */
    ALL = 282,                     /* ALL  */
    LOCALPART = 283,               /* LOCALPART  */
    DOMAIN = 284,                  /* DOMAIN  */
    IS = 285,                      /* IS  */
    CONTAINS = 286,                /* CONTAINS  */
    MATCHES = 287,                 /* MATCHES  */
    OCTET = 288,                   /* OCTET  */
    ASCIICASEMAP = 289,            /* ASCIICASEMAP  */
    ASCIINUMERIC = 290,            /* ASCIINUMERIC  */
    REGEX = 291,                   /* REGEX  */
    QUOTEREGEX = 292,              /* QUOTEREGEX  */
    COPY = 293,                    /* COPY  */
    BODY = 294,                    /* BODY  */
    RAW = 295,                     /* RAW  */
    TEXT = 296,                    /* TEXT  */
    CONTENT = 297,                 /* CONTENT  */
    ENVIRONMENT = 298,             /* ENVIRONMENT  */
    STRINGT = 299,                 /* STRINGT  */
    SET = 300,                     /* SET  */
    LOWER = 301,                   /* LOWER  */
    UPPER = 302,                   /* UPPER  */
    LOWERFIRST = 303,              /* LOWERFIRST  */
    UPPERFIRST = 304,              /* UPPERFIRST  */
    QUOTEWILDCARD = 305,           /* QUOTEWILDCARD  */
    LENGTH = 306,                  /* LENGTH  */
    VACATION = 307,                /* VACATION  */
    DAYS = 308,                    /* DAYS  */
    SUBJECT = 309,                 /* SUBJECT  */
    FROM = 310,                    /* FROM  */
    ADDRESSES = 311,               /* ADDRESSES  */
    MIME = 312,                    /* MIME  */
    HANDLE = 313,                  /* HANDLE  */
    SECONDS = 314,                 /* SECONDS  */
    COUNT = 315,                   /* COUNT  */
    VALUE = 316,                   /* VALUE  */
    GT = 317,                      /* GT  */
    GE = 318,                      /* GE  */
    LT = 319,                      /* LT  */
    LE = 320,                      /* LE  */
    EQ = 321,                      /* EQ  */
    NE = 322,                      /* NE  */
    FLAGS = 323,                   /* FLAGS  */
    HASFLAG = 324,                 /* HASFLAG  */
    SETFLAG = 325,                 /* SETFLAG  */
    ADDFLAG = 326,                 /* ADDFLAG  */
    REMOVEFLAG = 327,              /* REMOVEFLAG  */
    USER = 328,                    /* USER  */
    DETAIL = 329,                  /* DETAIL  */
    DATE = 330,                    /* DATE  */
    CURRENTDATE = 331,             /* CURRENTDATE  */
    ORIGINALZONE = 332,            /* ORIGINALZONE  */
    ZONE = 333,                    /* ZONE  */
    INDEX = 334,                   /* INDEX  */
    LAST = 335,                    /* LAST  */
    ADDHEADER = 336,               /* ADDHEADER  */
    DELETEHEADER = 337,            /* DELETEHEADER  */
    REJCT = 338,                   /* REJCT  */
    EREJECT = 339,                 /* EREJECT  */
    OPTIONS = 340,                 /* OPTIONS  */
    MESSAGE = 341,                 /* MESSAGE  */
    IMPORTANCE = 342,              /* IMPORTANCE  */
    VALIDNOTIFYMETHOD = 343,       /* VALIDNOTIFYMETHOD  */
    NOTIFYMETHODCAPABILITY = 344,  /* NOTIFYMETHODCAPABILITY  */
    NOTIFY = 345,                  /* NOTIFY  */
    ENCODEURL = 346,               /* ENCODEURL  */
    LOW = 347,                     /* LOW  */
    NORMAL = 348,                  /* NORMAL  */
    HIGH = 349,                    /* HIGH  */
    IHAVE = 350,                   /* IHAVE  */
    ERROR = 351,                   /* ERROR  */
    MAILBOXEXISTS = 352,           /* MAILBOXEXISTS  */
    CREATE = 353,                  /* CREATE  */
    METADATA = 354,                /* METADATA  */
    METADATAEXISTS = 355,          /* METADATAEXISTS  */
    SERVERMETADATA = 356,          /* SERVERMETADATA  */
    SERVERMETADATAEXISTS = 357,    /* SERVERMETADATAEXISTS  */
    BYTIMEREL = 358,               /* BYTIMEREL  */
    BYTIMEABS = 359,               /* BYTIMEABS  */
    BYMODE = 360,                  /* BYMODE  */
    BYTRACE = 361,                 /* BYTRACE  */
    DSNNOTIFY = 362,               /* DSNNOTIFY  */
    DSNRET = 363,                  /* DSNRET  */
    VALIDEXTLIST = 364,            /* VALIDEXTLIST  */
    LIST = 365,                    /* LIST  */
    INCLUDE = 366,                 /* INCLUDE  */
    OPTIONAL = 367,                /* OPTIONAL  */
    ONCE = 368,                    /* ONCE  */
    RETURN = 369,                  /* RETURN  */
    PERSONAL = 370,                /* PERSONAL  */
    GLOBAL = 371,                  /* GLOBAL  */
    DUPLICATE = 372,               /* DUPLICATE  */
    HEADER = 373,                  /* HEADER  */
    UNIQUEID = 374,                /* UNIQUEID  */
    SPECIALUSEEXISTS = 375,        /* SPECIALUSEEXISTS  */
    SPECIALUSE = 376,              /* SPECIALUSE  */
    FCC = 377,                     /* FCC  */
    MAILBOXID = 378,               /* MAILBOXID  */
    MAILBOXIDEXISTS = 379,         /* MAILBOXIDEXISTS  */
    SNOOZE = 380,                  /* SNOOZE  */
    MAILBOX = 381,                 /* MAILBOX  */
    ADDFLAGS = 382,                /* ADDFLAGS  */
    REMOVEFLAGS = 383,             /* REMOVEFLAGS  */
    WEEKDAYS = 384,                /* WEEKDAYS  */
    TZID = 385,                    /* TZID  */
    LOG = 386,                     /* LOG  */
    JMAPQUERY = 387,               /* JMAPQUERY  */
    PROCESSIMIP = 388,             /* PROCESSIMIP  */
    INVITESONLY = 389,             /* INVITESONLY  */
    UPDATESONLY = 390,             /* UPDATESONLY  */
    DELETECANCELED = 391,          /* DELETECANCELED  */
    CALENDARID = 392,              /* CALENDARID  */
    OUTCOME = 393,                 /* OUTCOME  */
    ERRSTR = 394,                  /* ERRSTR  */
    IKEEP_TARGET = 395             /* IKEEP_TARGET  */
  };
  typedef enum yytokentype yytoken_kind_t;
#endif
/* Token kinds.  */
#define YYEMPTY -2
#define YYEOF 0
#define YYerror 256
#define YYUNDEF 257
#define NUMBER 258
#define STRING 259
#define IF 260
#define ELSIF 261
#define ELSE 262
#define REQUIRE 263
#define STOP 264
#define DISCARD 265
#define KEEP 266
#define FILEINTO 267
#define REDIRECT 268
#define ANYOF 269
#define ALLOF 270
#define EXISTS 271
#define NOT 272
#define SFALSE 273
#define STRUE 274
#define SIZE 275
#define HEADERT 276
#define ADDRESS 277
#define ENVELOPE 278
#define COMPARATOR 279
#define OVER 280
#define UNDER 281
#define ALL 282
#define LOCALPART 283
#define DOMAIN 284
#define IS 285
#define CONTAINS 286
#define MATCHES 287
#define OCTET 288
#define ASCIICASEMAP 289
#define ASCIINUMERIC 290
#define REGEX 291
#define QUOTEREGEX 292
#define COPY 293
#define BODY 294
#define RAW 295
#define TEXT 296
#define CONTENT 297
#define ENVIRONMENT 298
#define STRINGT 299
#define SET 300
#define LOWER 301
#define UPPER 302
#define LOWERFIRST 303
#define UPPERFIRST 304
#define QUOTEWILDCARD 305
#define LENGTH 306
#define VACATION 307
#define DAYS 308
#define SUBJECT 309
#define FROM 310
#define ADDRESSES 311
#define MIME 312
#define HANDLE 313
#define SECONDS 314
#define COUNT 315
#define VALUE 316
#define GT 317
#define GE 318
#define LT 319
#define LE 320
#define EQ 321
#define NE 322
#define FLAGS 323
#define HASFLAG 324
#define SETFLAG 325
#define ADDFLAG 326
#define REMOVEFLAG 327
#define USER 328
#define DETAIL 329
#define DATE 330
#define CURRENTDATE 331
#define ORIGINALZONE 332
#define ZONE 333
#define INDEX 334
#define LAST 335
#define ADDHEADER 336
#define DELETEHEADER 337
#define REJCT 338
#define EREJECT 339
#define OPTIONS 340
#define MESSAGE 341
#define IMPORTANCE 342
#define VALIDNOTIFYMETHOD 343
#define NOTIFYMETHODCAPABILITY 344
#define NOTIFY 345
#define ENCODEURL 346
#define LOW 347
#define NORMAL 348
#define HIGH 349
#define IHAVE 350
#define ERROR 351
#define MAILBOXEXISTS 352
#define CREATE 353
#define METADATA 354
#define METADATAEXISTS 355
#define SERVERMETADATA 356
#define SERVERMETADATAEXISTS 357
#define BYTIMEREL 358
#define BYTIMEABS 359
#define BYMODE 360
#define BYTRACE 361
#define DSNNOTIFY 362
#define DSNRET 363
#define VALIDEXTLIST 364
#define LIST 365
#define INCLUDE 366
#define OPTIONAL 367
#define ONCE 368
#define RETURN 369
#define PERSONAL 370
#define GLOBAL 371
#define DUPLICATE 372
#define HEADER 373
#define UNIQUEID 374
#define SPECIALUSEEXISTS 375
#define SPECIALUSE 376
#define FCC 377
#define MAILBOXID 378
#define MAILBOXIDEXISTS 379
#define SNOOZE 380
#define MAILBOX 381
#define ADDFLAGS 382
#define REMOVEFLAGS 383
#define WEEKDAYS 384
#define TZID 385
#define LOG 386
#define JMAPQUERY 387
#define PROCESSIMIP 388
#define INVITESONLY 389
#define UPDATESONLY 390
#define DELETECANCELED 391
#define CALENDARID 392
#define OUTCOME 393
#define ERRSTR 394
#define IKEEP_TARGET 395

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
union YYSTYPE
{
#line 180 "sieve/sieve.y"

    int nval;
    char *sval;
    arrayu64_t *nl;
    strarray_t *sl;
    comp_t *ctag;
    test_t *test;
    testlist_t *testl;
    commandlist_t *cl;

#line 358 "sieve/sieve.h"

};
typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif




int sieveparse (sieve_script_t *sscript);


#endif /* !YY_SIEVE_SIEVE_SIEVE_H_INCLUDED  */
