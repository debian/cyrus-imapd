# cyrus-imapd po-debconf translation to Spanish
# Copyright (C) 2006, 2009 Software in the Public Interest
# This file is distributed under the same license as the cyrus-imapd package.
#
# Changes:
#   - Initial translation
#       César Gómez Martín <cesar.gomez@gmail.com>, 2006
#
#   - Updates
#       Francisco Javier Cuadrado <fcocuadrado@gmail.com>, 2009
#
# Traductores, si no conocen el formato PO, merece la pena leer la
# documentación de gettext, especialmente las secciones dedicadas a este
# formato, por ejemplo ejecutando:
#       info -n '(gettext)PO Files'
#       info -n '(gettext)Header Entry'
#
# Equipo de traducción al español, por favor, lean antes de traducir
# los siguientes documentos:
#
#   - El proyecto de traducción de Debian al español
#     http://www.debian.org/intl/spanish/
#     especialmente las notas de traducción en
#     http://www.debian.org/intl/spanish/notas
#
#   - La guía de traducción de po's de debconf:
#     /usr/share/doc/po-debconf/README-trans
#     o http://www.debian.org/intl/l10n/po-debconf/README-trans
#
msgid ""
msgstr ""
"Project-Id-Version: cyrus-imapd 2.4.14-1\n"
"Report-Msgid-Bugs-To: cyrus-imapd@packages.debian.org\n"
"POT-Creation-Date: 2011-04-11 15:24+0200\n"
"PO-Revision-Date: 2009-04-27 10:51+0100\n"
"Last-Translator: Francisco Javier Cuadrado <fcocuadrado@gmail.com>\n"
"Language-Team: Debian l10n spanish <debian-l10n-spanish@lists.debian.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: boolean
#. Description
#: ../cyrus-common.templates:1001
msgid "Remove the mail and news spools?"
msgstr "¿Desea eliminar las colas de correo y noticias?"

#. Type: boolean
#. Description
#: ../cyrus-common.templates:1001
msgid ""
"The Cyrus mail and news spools, as well as users' sieve scripts, can be "
"removed when the package is purged."
msgstr ""
"Al purgar el paquete se pueden eliminar tanto el correo de Cyrus y las colas "
"de noticias, como los scripts de filtrado de los usuarios."

#. Type: boolean
#. Description
#: ../cyrus-common.templates:1001
msgid ""
"This question only applies to the default spools and sieve script "
"directories in /var.  If you modified their location in imapd.conf, the new "
"locations will not be removed; just the old ones in /var."
msgstr ""
"Esta pregunta sólo se aplica a los directorios predeterminados de las colas "
"y de los scripts de filtrado de «/var». Si modificó sus ubicaciones en el "
"archivo «imapd.conf», las nuevas ubicaciones no se eliminarán; sólo lo harán "
"las viejas de «/var»."
