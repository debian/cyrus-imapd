Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: cyrus-imapd
Upstream-Contact: cyrus-devel@lists.andrew.cmu.edu
Source: https://www.cyrusimap.org/
Files-Excluded:
 docsrc/exts/sphinxlocal/__pycache__
 docsrc/exts/sphinxlocal/*/__pycache__

Files: *
Copyright: 1994-2020, Carnegie Mellon University
License: CMU

Files: cassandane/*
Copyright: 2011 Opera Software Australia Pty. Ltd.
License: OSAP

Files: cassandane/Cassandane/*
 cassandane/utils/crash.c
Copyright: 2011-2022 Fastmail Pty Ltd
License: FPL

Files: cmulocal/*.m4
 compile
 config.*
 ylwrap
Copyright: 1992-2022 Free Software Foundation, Inc.
License: GPL-2+

Files: cmulocal/ax_cxx_compile_stdcxx_11.m4
Copyright: 2008 Benjamin Kosnik <bkoz@redhat.com>
 2012 Zack Weinberg <zackw@panix.com>
 2013 Roy Stogner <roystgnr@ices.utexas.edu>
 2014, 2015 Google Inc., contributed by Alexey Sokolov <sokolov@google.com>
 2015 Paul Norman <penorman@mac.com>
License: FSFAPL

Files: cmulocal/ax_prog_perl_modules.m4
Copyright: 2009 Dean Povey <povey@wedgetail.com>
License: FSFAPL

Files: cmulocal/ax_python_module.m4
Copyright: 2008 Andrew Collier
License: FSFAPL

Files: com_err/et/*
Copyright: 1987, 1988 by the Student Information Processing Board of the
 Massachusetts Institute of Technology
License: MIT

Files: debian/*
Copyright: 1997, Joey Hess
 2001-2010, Henrique de Moraes Holschuh
 2002-2010, Sven Mueller
 2002-2010, Benjamin Seidenberg
 2002-2010, Farzad FARID
 2002-2010, Christoph Berg
 2005-2015, Ondřej Surý
 2019-2024, Xavier Guimard <yadd@debian.org>
 2022-2025, Linagora <https://linagora.com>
License: GPL-2+

Files: doc/*
Copyright: 1993–2023 The Cyrus Team
License: CMU
Comment: Built with Sphinx, license BSD-2-Clause

Files: imap/objectstore_caringo.c
Copyright: 2015 OpenIO, as a part of Cyrus
License: CMU

Files: imap/setproctitle.c
Copyright: 1994-2008, Carnegie Mellon University
 1983-1995 Eric P. Allman
 1988-1993 The Regents of the University of California
License: CMU

Files: install-sh
Copyright: 1994 X Consortium
License: Expat
Comment:
 Except as contained in this notice, the name of the X Consortium shall not
 be used in advertising or otherwise to promote the sale, use or other deal-
 ings in this Software without prior written authorization from the X Consor-
 tium.
 .
 FSF changes to this file are in the public domain

Files: lib/bloom.*
Copyright: 012-2016, Jyri J. Virkki
License: BSD-2-Clause

Files: lib/crc32.c
Copyright: 2011-2015 Stephan Brumme
License: CRC-License

Files: perl/imap/Cyrus/*
Copyright: 2008-2017 FastMail
License: CMU

License: BSD-2-Clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are
 met:
 .
 1. Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 .
 2. Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: CRC-License
 This software is provided 'as-is', without any express or implied warranty.
 In no event will the author be held liable for any damages arising from the
 of this software.
 .
 Permission is granted to anyone to use this software for any purpose,
 including commercial applications, and to alter it and redistribute it
 freely, subject to the following restrictions:
 .
 1. The origin of this software must not be misrepresented; you must not
    claim that you wrote the original software.
 2. If you use this software in a product, an acknowledgment in the product
    documentation would be appreciated but is not required.
 3. Altered source versions must be plainly marked as such, and must not be
    misrepresented as being the original software.

License: CMU
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 .
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution.
 .
 3. The name "Carnegie Mellon University" must not be used to
    endorse or promote products derived from this software without
    prior written permission. For permission or any legal
    details, please contact
      Office of Technology Transfer
      Carnegie Mellon University
      5000 Forbes Avenue
      Pittsburgh, PA  15213-3890
      (412) 268-4387, fax: (412) 268-7395
      tech-transfer@andrew.cmu.edu
 .
 4. Redistributions of any form whatsoever must retain the following
    acknowledgment:
    "This product includes software developed by Computing Services
     at Carnegie Mellon University (https://www.cmu.edu/computing/)."
 .
 CARNEGIE MELLON UNIVERSITY DISCLAIMS ALL WARRANTIES WITH REGARD TO
 THIS SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
 AND FITNESS, IN NO EVENT SHALL CARNEGIE MELLON UNIVERSITY BE LIABLE
 FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN
 AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING
 OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

License: FPL
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 .
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution.
 .
 3. The name "Fastmail Pty Ltd" must not be used to
    endorse or promote products derived from this software without
    prior written permission. For permission or any legal
    details, please contact
     FastMail Pty Ltd
     PO Box 234
     Collins St West 8007
     Victoria
     Australia
 .
 4. Redistributions of any form whatsoever must retain the following
    acknowledgment:
    "This product includes software developed by Fastmail Pty. Ltd."
 .
 FASTMAIL PTY LTD DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE,
 INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY  AND FITNESS, IN NO
 EVENT SHALL OPERA SOFTWARE AUSTRALIA BE LIABLE FOR ANY SPECIAL, INDIRECT
 OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF
 USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE
 OF THIS SOFTWARE.

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to
 deal in the Software without restriction, including without limitation the
 rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
 X CONSORTIUM BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNEC-
 TION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

License: FSFAPL
 Copying and distribution of this file, with or without modification,
 are permitted in any medium without royalty provided the copyright
 notice and this notice are preserved. This file is offered as-is,
 without any warranty

License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of version 2 of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-2'

License: MIT
 Permission to use, copy, modify, and distribute this software
 and its documentation for any purpose and without fee is
 hereby granted, provided that the above copyright notice
 appear in all copies and that both that copyright notice and
 this permission notice appear in supporting documentation,
 and that the names of M.I.T. and the M.I.T. S.I.P.B. not be
 used in advertising or publicity pertaining to distribution
 of the software without specific, written prior permission.
 M.I.T. and the M.I.T. S.I.P.B. make no representations about
 the suitability of this software for any purpose.  It is
 provided "as is" without express or implied warranty.

License: OSAP
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 .
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution.
 .
 3. The name "Opera Software Australia" must not be used to
    endorse or promote products derived from this software without
    prior written permission. For permission or any legal
    details, please contact
        Opera Software Australia Pty. Ltd.
        Level 50, 120 Collins St
        Melbourne 3000
        Victoria
        Australia
 .
 4. Redistributions of any form whatsoever must retain the following
    acknowledgment:
    "This product includes software developed by Opera Software
    Australia Pty. Ltd."
 .
 OPERA SOFTWARE AUSTRALIA DISCLAIMS ALL WARRANTIES WITH REGARD TO
 THIS SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
 AND FITNESS, IN NO EVENT SHALL OPERA SOFTWARE AUSTRALIA BE LIABLE
 FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN
 AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING
 OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
